start:
	docker-compose up -d

stop:
	docker-compose down

link:
	docker-compose exec nginx ln -s /var/www/storage/app/public /var/www/public/storage

build:
	docker-compose up --build -d

npm-i:
	docker-compose exec node npm i

gulp:
	docker-compose exec node gulp

test:
	docker-compose exec php-cli vendor/bin/phpunit

gulp-rebuild:
	docker-compose exec node npm rebuild node-sass

docker-start:
	sudo sudo service docker start

memory:
	sudo sysctl -w vm.max_map_count=262144

perm:
	sudo chgrp -R www-data storage bootstrap/cache
	sudo chmod -R ug+rwx storage bootstrap/cache
