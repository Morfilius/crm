<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'namespace' => 'Admin',
    'middleware' => ['auth'],
], function () {
    Route::resource('users', 'UsersController');
    Route::resource('employee', 'EmployeeController')->except(['create']);
    Route::resource('salon', 'SalonController');
    Route::resource('staff', 'StaffController');
    Route::resource('client', 'ClientController');

    Route::group([
        'namespace' => 'Order'
    ], function () {
        Route::get('/order/calendar', 'OrderListController@calendar')->name('order.list.calendar');
        Route::get('/order/list', 'OrderListController@list')->name('order.list.list');
        Route::get('/order/employees', 'OrderListController@employees')->name('order.list.employees');
        Route::resource('order', 'OrderController');
    });

    Route::group([
        'namespace' => 'Manager'
    ], function () {
        Route::get('/manager/salon', 'SalonController@index')->name('salon.manager.index');
        Route::get('/manager/salon/{salon}/edit', 'SalonController@edit')->name('salon.manager.edit');
        Route::put('/manager/salon/{salon}/update', 'SalonController@update')->name('salon.manager.update');
    });

    Route::group([
        'namespace' => 'Service'
    ], function () {
        Route::delete('/service/user/{service}/{employee}', 'ServiceController@detach')->name('service.detach.user');

        Route::get('/service/{active_category_id?}', 'ServiceController@index')->name('service.index');
        Route::get('/service/create/{category_id}', 'ServiceController@create')->name('service.create');
        Route::post('/service/store', 'ServiceController@store')->name('service.store');
        Route::get('/service/edit/{service}', 'ServiceController@edit')->name('service.edit');
        Route::post('/service/update/{service}', 'ServiceController@update')->name('service.update');
        Route::delete('/service/destroy/{service}', 'ServiceController@destroy')->name('service.destroy');

        Route::post('/service/scope', 'ScopeController@store')->name('service.scope.store');

        Route::post('/service/category/{id}', 'CategoryController@store')->name('service.category.store');
    });

});

Auth::routes();
