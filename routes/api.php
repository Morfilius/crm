<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group([
    'as' => 'api',
    'namespace' => 'Api',
], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');
    Route::resource('user', 'UserController')->only( 'update');
    Route::resource('salon', 'SalonController')->only( 'index');

    Route::resource('employee/employee', 'Employee\EmployeeController')->only( 'index', 'show');
    Route::get('employee/example', 'Employee\ExampleController@index');

    Route::get('service/sphere', 'Service\SphereController@index');
    Route::get('service/category', 'Service\CategoryController@index');

    Route::middleware('auth:sanctum')->group(function () {
        Route::post('employee/example/like/{id}', 'Employee\ExampleController@like')
            ->where(['id' => '[0-9]+']);
        Route::delete('employee/example/like/{id}', 'Employee\ExampleController@unLike')
            ->where(['id' => '[0-9]+']);

        Route::get('user/self', 'UserController@self');

        Route::resource('order', 'OrderController')->only( 'index', 'store');
        Route::resource('review', 'ReviewController')->only( 'store');
        Route::put('order/cancel/{id}', 'OrderController@cancel')
            ->where(['id' => '[0-9]+']);;
    });
});
