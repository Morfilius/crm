<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salons', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('photo')->nullable();
            $table->string('rating')->default(0);
            $table->string('type');
            $table->string('address');
            $table->text('description');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('work_time');


            $table->string('company_type');
            $table->string('inn');
            $table->string('bank_name');
            $table->string('company_name');
            $table->string('kpp');
            $table->string('correspondent_account');
            $table->string('company_address');
            $table->string('bik');
            $table->string('checking_account');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salons');
    }
}
