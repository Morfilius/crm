<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('type');
            $table->bigInteger('parent_id')->nullable();
            $table->timestamps();
        });

        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('price');
            $table->text('description')->nullable();
            $table->boolean('online');
            $table->string('session_start');
            $table->string('session_end');
            $table->string('session_step');
            $table->foreignId('service_categories_id')->nullable()->constrained()->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('service_record_excluded', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date');
            $table->foreignId('service_id')->constrained()->onDelete('cascade');
        });

        Schema::create('employee_service', function (Blueprint $table) {
            $table->index(['service_id', 'employee_id']);
            $table->foreignId('service_id')->constrained()->onDelete('cascade');
            $table->foreignId('employee_id')->constrained()->onDelete('cascade');
        });

        Schema::create('salon_service', function (Blueprint $table) {
            $table->index(['salon_id', 'service_id']);
            $table->foreignId('service_id')->constrained()->onDelete('cascade');
            $table->foreignId('salon_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_record_excluded');
        Schema::dropIfExists('employee_service');
        Schema::dropIfExists('salon_service');
        Schema::dropIfExists('services');
        Schema::dropIfExists('service_categories');
    }
}
