<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->mediumText('description')->nullable();
            $table->string('address')->nullable();
            $table->string('work_time')->nullable();
            $table->string('price')->nullable();
            $table->string('status')->default('moderation');
            $table->foreignId('salon_id')->nullable()->constrained()->onDelete('cascade');
            $table->text('block_reason')->nullable();
            $table->smallInteger('rating')->default(0)->index();
            $table->timestamps();
        });

        Schema::create('employee_images', function (Blueprint $table) {
            $table->id();
            $table->foreignId('employee_id')->constrained()->onDelete('cascade');
            $table->string('photo');
            $table->timestamps();
        });

        Schema::create('employee_reviews', function (Blueprint $table) {
            $table->id();
            $table->foreignId('employee_id')->constrained()->onDelete('cascade');
            $table->string('rating');
            $table->string('text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_images');
        Schema::dropIfExists('employee_reviews');
        Schema::dropIfExists('employees');
    }
}
