<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Entity\Employee{
/**
 * App\Entity\Employee\Employee
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string|null $description
 * @property string|null $photo
 * @property string $staff
 * @property string $address
 * @property string $work_time
 * @property string $price
 * @property string $status
 * @property int|null $salon_id
 * @property string|null $block_reason
 * @property int $rating
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Employee\EmployeeImages[] $images
 * @property-read int|null $images_count
 * @property-read \App\Entity\Salon|null $salon
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Service\Service[] $services
 * @property-read int|null $services_count
 * @property-read \App\Entity\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereBlockReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereSalonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereStaff($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereWorkTime($value)
 * @mixin \Eloquent
 */
	class Employee extends \Eloquent {}
}

namespace App\Entity\Employee{
/**
 * App\Entity\Employee\EmployeeImages
 *
 * @property-read \App\Entity\Employee\Employee $employee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\EmployeeImages newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\EmployeeImages newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\EmployeeImages query()
 * @mixin \Eloquent
 */
	class EmployeeImages extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Post
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Post query()
 * @mixin \Eloquent
 */
	class Post extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Salon
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string|null $photo
 * @property string $rating
 * @property string $type
 * @property string $address
 * @property string $description
 * @property string $latitude
 * @property string $longitude
 * @property string $work_time
 * @property string $company_type
 * @property string $inn
 * @property string $bank_name
 * @property string $company_name
 * @property string $kpp
 * @property string $correspondent_account
 * @property string $company_address
 * @property string $bik
 * @property string $checking_account
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Employee\Employee[] $employees
 * @property-read int|null $employees_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Service\Service[] $services
 * @property-read int|null $services_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereBik($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereCheckingAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereCompanyAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereCompanyType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereCorrespondentAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereKpp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereWorkTime($value)
 * @mixin \Eloquent
 */
	class Salon extends \Eloquent {}
}

namespace App\Entity\Service{
/**
 * App\Entity\Service\Category
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property int $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Service\Service[] $services
 * @property-read int|null $services_count
 */
	class Category extends \Eloquent {}
}

namespace App\Entity\Service{
/**
 * App\Entity\Service\RecordExcluded
 *
 * @property int $id
 * @property string $date
 * @property int $service_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\RecordExcluded newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\RecordExcluded newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\RecordExcluded query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\RecordExcluded whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\RecordExcluded whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\RecordExcluded whereServiceId($value)
 * @mixin \Eloquent
 * @property-read \App\Entity\Service\Service|null $service
 */
	class RecordExcluded extends \Eloquent {}
}

namespace App\Entity\Service{
/**
 * App\Entity\Service\Service
 *
 * @property int $id
 * @property string $name
 * @property int $price
 * @property string|null $description
 * @property int $online
 * @property string $session_start
 * @property string $session_end
 * @property string $session_step
 * @property int|null $service_categories_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entity\Service\Category|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Employee\Employee[] $employees
 * @property-read int|null $employees_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Service\RecordExcluded[] $records
 * @property-read int|null $records_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Salon[] $salons
 * @property-read int|null $salons_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereOnline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereServiceCategoriesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereSessionEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereSessionStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereSessionStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Service extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Staff
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Staff newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Staff newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Staff query()
 * @mixin \Eloquent
 */
	class Staff extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $role
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $phone
 * @property string|null $photo
 * @property-read \App\Entity\Employee\Employee|null $employee
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class User extends \Eloquent {}
}

