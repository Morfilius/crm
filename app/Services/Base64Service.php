<?php


namespace App\Services;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\File\File;

class Base64Service
{
    public static function loadFromBase64($string) : ?UploadedFile
    {
        if (!preg_match('#data:image/(.*?);base64,(.*)#s', $string, $matches)) {
            return null;
        }
        $ext = $matches[1];
        self::validateExt($ext);

        $fileData = base64_decode($matches[2]);

        $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
        file_put_contents($tmpFilePath, $fileData);
        $tmpFile = new File($tmpFilePath);

        return new UploadedFile(
            $tmpFile->getPathname(),
            $tmpFile->getFilename(),
            $tmpFile->getMimeType(),
            0
        );
    }

    private static function validateExt($ext)
    {
        if (! in_array($ext, ['png', 'jpg', 'jpeg'])) {
            throw new BadRequestHttpException('Invalid or empty file extension ' . $ext);
        }
    }
}
