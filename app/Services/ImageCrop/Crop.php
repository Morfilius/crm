<?php


namespace App\Services\ImageCrop;


trait Crop
{
    /**
     * @var ImageCrop
     */
    private $imageCrop;

    public function setImageCropConfig()
    {
        //переопределить с контентом $this->imageCrop->setConfig(new Config(...));
    }

    protected static function booted()
    {
        static::saving(function ($model) {
            $model->beforeSave();
        });

        static::saved(function ($model) {
            $model->afterSave($model);
        });

        static::updated(function ($model) {
            $model->afterSave($model);
        });

        static::retrieved(function ($model) {
            $model->afterFind($model);
        });

        static::deleted(function ($model) {
            $model->afterDelete($model);
        });
    }

    public function beforeSave()
    {
        $this->photo = $this->imageCrop->beforeSave();
    }

    public function afterSave($model)
    {
        $this->imageCrop->afterSave();
    }

    public function afterFind($model)
    {
        $this->imageCrop->afterFind($model);
    }

    public function afterDelete($model)
    {
        $this->imageCrop->afterDelete($model);
    }

    public function getOriginalImage()
    {
        return $this->imageCrop->getOriginal();
    }

    public function getThumb($name)
    {
        return $this->imageCrop->getThumb($name);
    }

    public function hasImage()
    {
        return $this->imageCrop->hasImage();
    }

    protected function initializeCrop()
    {
        $this->imageCrop = new ImageCrop($this);
        //default config
        $this->imageCrop->setConfig(new Config('image', 'images', '', []));
        $this->setImageCropConfig();
    }
}
