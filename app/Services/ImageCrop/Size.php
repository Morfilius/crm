<?php


namespace App\Services\ImageCrop;


class Size
{
    public $name;
    public $width;
    public $height;

    public function __construct($name, $width, $height)
    {
        $this->name = $name;
        $this->width = $width;
        $this->height = $height;
    }
}
