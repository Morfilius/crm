<?php


namespace App\Services\ImageCrop;


use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageCrop
{
    private $model;
    /**
     * @var Config
     */
    private $config;
    private $fileName;


    public function __construct(Model $model)
    {
        $this->setModel($model);
    }

    public function setConfig(Config $config)
    {
        $this->config = $config;
    }

    public function beforeSave()
    {
        return $this->saveImage();
    }

    public function afterSave()
    {
        $this->setFilename();
        $this->moveFile();
        if ($this->isImageChanged()) {
            $this->removeFiles($this->model->getOriginal($this->config->imageAttr));
        }
    }

    public function afterFind($model)
    {
        $this->setModel($model);
        $this->setFilename();
    }

    public function afterDelete($model)
    {
        $this->removeFiles($this->fileName);
    }

    public function hasImage($name = null)
    {
        return $name ? is_file($this->thumbFilePath($name)) : is_file($this->originalFilePath());
    }

    public function getOriginal()
    {
        if ($this->fileName) {
            return asset('storage/' . $this->targetRelatedFileDir() . '/' . $this->fileName);
        }
        return '';
    }

    public function getThumb($name)
    {
        if ($this->fileName) {
            if ($this->isNeedCreateThumb($name)) {
                $this->createThumb($name);
            }
            return asset('storage/' . $this->cacheRelatedDir() . '/' . $this->getThumbFilename($name));
        }
        return '';
    }

    private function isImageChanged()
    {
        $oldFilename = $this->model->getOriginal($this->config->imageAttr);
        if (empty($oldFilename) || empty($this->fileName)) {
            return false;
        }
        return ($oldFilename !== $this->fileName);
    }

    private function removeFiles($filename)
    {
        $sizes = $this->config->getSizes();
        if (is_file($this->targetFileDir() . '/' . $filename)) {
            unlink($this->targetFileDir() . '/' . $filename);
        }
        if (is_array($sizes) && ! empty($sizes)) {
            foreach ($sizes as $size) {
                if (is_file($this->cacheDir() . '/' . $this->getThumbFilename($size->name, $filename))) {
                    unlink($this->cacheDir() . '/' . $this->getThumbFilename($size->name, $filename));
                }
            }
        }
    }

    private function createThumb($name)
    {
        $size = $this->config->getSizes()[$name];
        $image = $this->resizeImage($size);
        $this->saveThumb($image, $name);
    }

    private function isNeedCreateThumb($name)
    {
        return (isset($this->config->getSizes()[$name]) && $this->hasImage() && ! $this->hasImage($name));
    }

    private function resizeImage(Size $size)
    {
        $img = Image::make($this->originalFilePath());
        return $img->fit($size->width, $size->height);
    }

    private function saveThumb(\Intervention\Image\Image $image, $name)
    {
        $this->createDir($this->cacheDir());
        $image->save($this->thumbFilePath($name));
    }

    private function saveImage()
    {
        $imageFile = $this->model[$this->config->imageAttr];
        if ($imageFile && $imageFile instanceof UploadedFile) {
            return basename($imageFile->store($this->config->getRelatedUploadDir()));
        }
        return $this->fileName;
    }

    private function moveFile()
    {
        if ($this->mustMove()) {
            $this->move($this->targetFileDir());
        }
    }

    private function mustMove()
    {
        return ($this->fileName && $this->config->subFolderAttr && ! $this->hasImage());
    }

    private function move($toDir)
    {
        $from = $this->config->getUploadDir() . '/' . $this->fileName;
        $to   = $toDir . '/' . $this->fileName;

        $this->createDir($toDir);
        if (is_file($from) && !is_file($to)) {
            rename($from, $to);
        }
    }

    private function createDir($dir)
    {
        if (! is_dir($dir)) {
            mkdir($dir, 0755, true);
        }
    }

    private function targetFileDir()
    {
        return $this->config->subFolderAttr ? $this->config->getUploadDir() . '/' . $this->getSubFolder() : $this->config->getUploadDir();
    }

    private function cacheDir()
    {
        return $this->config->subFolderAttr ? $this->config->getCacheUploadDir() . '/' . $this->getSubFolder() : $this->config->getCacheUploadDir();
    }

    private function cacheRelatedDir()
    {
        return $this->config->subFolderAttr ? $this->config->getCacheRelatedUploadDir() . '/' . $this->getSubFolder() : $this->config->getCacheRelatedUploadDir();
    }

    private function targetRelatedFileDir()
    {
        return $this->config->subFolderAttr ? $this->config->getRelatedUploadDir() . '/' . $this->getSubFolder() : $this->config->getRelatedUploadDir();
    }

    private function originalFilePath()
    {
        return $this->targetFileDir() . '/' . $this->fileName;
    }

    private function thumbFilePath($name)
    {
        return $this->cacheDir() . '/' . $this->getThumbFilename($name);
    }

    private function getThumbFilename($name, $fileName = null)
    {
        $fileName = $fileName ? $fileName : $this->fileName;
        if ($fileName) {
            list($basename, $ext) = explode('.', $fileName);
            return $basename . '-' . $name . '.' . $ext;
        }
        return '';
    }

    private function getSubFolder()
    {
        return $this->model[$this->config->subFolderAttr];
    }

    private function setModel($model)
    {
        $this->model = $model;
    }

    private function setFilename()
    {
        $this->fileName = $this->model[$this->config->imageAttr];
    }
}
