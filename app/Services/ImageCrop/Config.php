<?php


namespace App\Services\ImageCrop;


class Config
{
    public $imageAttr;
    public $folder;
    public $subFolderAttr;
    /**
     * @var $sizes Size[]
     */
    private $sizes;
    public $storagePath;

    public function __construct($imageAttr, $folder, $subFolderAttr, array $sizes, $storagePath = null)
    {
        $this->imageAttr = $imageAttr;
        $this->folder = $folder;
        $this->subFolderAttr = $subFolderAttr;
        $this->setSizes($sizes);
        $this->setStoragePath($storagePath);
    }

    private function setSizes(array $sizes)
    {
        $this->sizes = array_reduce($sizes, function ($result, Size $size) {
            $result[$size->name] = $size;
            return $result;
        }, []);
    }

    private function setStoragePath($path)
    {
        $this->storagePath = $path ? $path : storage_path('app/public');
    }

    public function getRelatedUploadDir()
    {
        return 'origin/' . $this->folder;
    }

    public function getUploadDir()
    {
        return $this->storagePath . '/' . $this->getRelatedUploadDir();
    }

    public function getCacheRelatedUploadDir()
    {
        return 'cache/' . $this->folder;
    }

    public function getCacheUploadDir()
    {
        return $this->storagePath . '/' . $this->getCacheRelatedUploadDir();
    }

    /**
     * @return Size[]
     */
    public function getSizes(): ?array
    {
        return $this->sizes;
    }
}
