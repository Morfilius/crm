<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Post
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Post query()
 * @mixin \Eloquent
 */
class Post extends Model
{
    //
}
