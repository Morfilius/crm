<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Client
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $city
 * @property int $discount
 * @property string $status
 * @property string $source
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Client query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Client whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Client whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Client whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Client wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Client whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Client whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Client whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Client whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Entity\Salon $salons
 * @property-read \App\Entity\Client $user
 * @property-read int|null $salons_count
 */
class Client extends Model
{
    public const STATUS_LOW = 'low';
    public const STATUS_HIGH = 'high';

    public const SOURCE_MOBILE = 'mobile';
    public const SOURCE_ADMIN = 'admin';

    protected $fillable = ['user_id', 'discount', 'status', 'source'];

    public static function statusesList(): array
    {
        return [
            self::STATUS_HIGH => 'Высокий',
            self::STATUS_LOW => 'Низкий',
        ];
    }

    public static function sourcesList(): array
    {
        return [
            self::SOURCE_ADMIN => 'Админ. панель',
            self::SOURCE_MOBILE => 'Мобильное приложение',
        ];
    }

    public static function getStatus($status)
    {
        return self::statusesList()[$status];
    }


    public static function getSource($source)
    {
        return self::sourcesList()[$source];
    }

    public function isClientInSalon($salon_id)
    {
        return $this->whereHas('salons', function ($query) use ($salon_id) {
            $query->where('salon_id', $salon_id)->where('client_id', $this->id);
        })->exists();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function salons()
    {
        return $this->belongsToMany(Salon::class);
    }
}
