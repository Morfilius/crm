<?php

namespace App\Entity\Service;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Service\RecordExcluded
 *
 * @property int $id
 * @property string $date
 * @property int $service_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\RecordExcluded newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\RecordExcluded newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\RecordExcluded query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\RecordExcluded whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\RecordExcluded whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\RecordExcluded whereServiceId($value)
 * @mixin \Eloquent
 * @property-read \App\Entity\Service\Service|null $service
 */
class RecordExcluded extends Model
{
    protected $table = 'service_record_excluded';
    public $timestamps = false;
    protected $fillable = ['date'];

    protected $casts = [
        'date' => 'date',
    ];

    public function service()
    {
        return $this->hasOne(Service::class);
    }
}
