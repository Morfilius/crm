<?php

namespace App\Entity\Service;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Service\Category
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property int $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Service\Service[] $services
 * @property-read int|null $services_count
 */
class Category extends Model
{
    public const TYPE_SCOPE = 'scope';
    public const TYPE_CATEGORY = 'category';

    protected $table = 'service_categories';

    protected $fillable = ['name', 'type'];

    public function assignToScope($scope_id)
    {
        $this->parent_id = $scope_id;
    }

    public function isScope()
    {
        return $this->type === self::TYPE_SCOPE;
    }

    public function isCategory()
    {
        return $this->type === self::TYPE_CATEGORY;
    }

    public function categories()
    {
        return self::whereType(self::TYPE_CATEGORY)->whereParentId($this->id)->get();
    }

    public function hasChildCategory($category_id)
    {
        return self::whereType(self::TYPE_CATEGORY)->whereParentId($this->id)->whereId($category_id)->exists();
    }

    public function services()
    {
        return $this->hasMany(Service::class, 'service_categories_id');
    }
}
