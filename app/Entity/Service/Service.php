<?php

namespace App\Entity\Service;

use App\Entity\Employee\Employee;
use App\Entity\Salon;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Entity\Service\Service
 *
 * @property int $id
 * @property string $name
 * @property int $price
 * @property string|null $description
 * @property int $online
 * @property string $session_start
 * @property string $session_end
 * @property string $session_step
 * @property int|null $service_categories_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entity\Service\Category|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Employee\Employee[] $employees
 * @property-read int|null $employees_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Service\RecordExcluded[] $records
 * @property-read int|null $records_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Salon[] $salons
 * @property-read int|null $salons_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereOnline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereServiceCategoriesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereSessionEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereSessionStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereSessionStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Service\Service whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Service extends Model
{
    protected $fillable = ['name', 'price', 'description', 'online', 'session_start',
        'session_end', 'session_step'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'service_categories_id');
    }

    public static function sessionStartList()
    {
        return ['7:00', '8:00', '9:00', '10:00', '11:00', '12:00', '13:00',
            '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00',
            '21:00', '22:00', '23:00'];
    }

    public static function sessionEndList()
    {
        return ['7:00', '8:00', '9:00', '10:00', '11:00', '12:00', '13:00',
            '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00',
            '21:00', '22:00', '23:00'];
    }

    public static function sessionStepList()
    {
        return ['5', '10', '15', '20', '25', '30'];
    }

    public function getEmployee()
    {
        return $this->employees()->first();
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class);
    }

    public function salons()
    {
        return $this->belongsToMany(Salon::class);
    }

    public function records()
    {
        return $this->hasMany(RecordExcluded::class);
    }
}
