<?php

namespace App\Entity\Employee;

use App\Entity\Salon;
use App\Entity\Service\Service;
use App\Entity\User;
use App\Services\ImageCrop\Config;
use App\Services\ImageCrop\Crop;
use App\Services\ImageCrop\Size;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Entity\Employee\Employee
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string|null $description
 * @property string|null $photo
 * @property string $staff
 * @property string $address
 * @property string $work_time
 * @property string $price
 * @property string $status
 * @property int|null $salon_id
 * @property string|null $block_reason
 * @property int $rating
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Employee\EmployeeImages[] $images
 * @property-read int|null $images_count
 * @property-read \App\Entity\Salon|null $salon
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Service\Service[] $services
 * @property-read int|null $services_count
 * @property-read \App\Entity\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereBlockReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereSalonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereStaff($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee whereWorkTime($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\Employee current()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\User[] $subscribers
 * @property-read int|null $subscribers_count
 */
class Employee extends Model
{
    public const MODERATE = 'moderation';
    public const ACTIVE = 'active';
    public const BLOCKED = 'blocked';

    protected $fillable = ['work_time',
        'address', 'price', 'description'];

    public static function getStatusList()
    {
        return [
            self::MODERATE => 'На модерации',
            self::ACTIVE => 'Активный',
            self::BLOCKED => 'Заблокированный',
        ];
    }

    public static function getStatus($status)
    {
        return self::getStatusList()[$status];
    }

    public function syncImages($imagesIds)
    {
        if (is_array($imagesIds) && !empty($imagesIds)) {
            $mustRemoveIds = EmployeeImages::whereNotIn('id', $imagesIds)->where('employee_id', $this->id)->pluck('id')->toArray();
            if (!empty($mustRemoveIds) && is_array($mustRemoveIds)) {
                foreach ($mustRemoveIds as $id) {
                    EmployeeImages::find($id)->delete();
                }
            }
        }
    }

    public function block($reason)
    {

    }

    public function isSubscribeExists($client_id, Carbon $recordDateTime)
    {
        return $this->subscribers()->wherePivot('user_id', $client_id)
            ->wherePivot('date', $recordDateTime->format('Y-m-d H:i:s'))->exists();
    }

    public static function getList()
    {
        return self::all();
    }

    public function images()
    {
        return $this->hasMany(EmployeeImages::class);
    }

    public function salon()
    {
        return $this->belongsTo(Salon::class);
    }

    public function services()
    {
        return $this->belongsToMany(Service::class);
    }

    public function subscribers()
    {
        return $this->belongsToMany(User::class, 'employee_subscribers')->withPivot('date');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeCurrent($query)
    {
        return $query->whereHas('user', function ($query){
            $query->where('salon_id', Auth::user()->salon->id);
        });
    }
}
