<?php

namespace App\Entity\Employee;

use App\Entity\User;
use App\Services\ImageCrop\Config;
use App\Services\ImageCrop\Crop;
use App\Services\ImageCrop\Size;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Entity\Employee\EmployeeImages
 *
 * @property-read \App\Entity\Employee\Employee $employee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\EmployeeImages newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\EmployeeImages newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\EmployeeImages query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $employee_id
 * @property string $photo
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\EmployeeImages whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\EmployeeImages whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\EmployeeImages whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\EmployeeImages wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Employee\EmployeeImages whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\User[] $like
 * @property-read int|null $like_count
 */
class EmployeeImages extends Model
{
    use Crop;

    protected $fillable = ['photo'];

    public function setImageCropConfig()
    {
        $this->imageCrop->setConfig(new Config('photo', 'master_images', 'id', [
            new Size('list', '184', '112'),
        ]));
    }

    public function isLiked($client_id)
    {
        return $this->like()->wherePivot('user_id', $client_id)->exists();
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function like()
    {
        return $this->belongsToMany(User::class, 'employee_images_like');
    }
}
