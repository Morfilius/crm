<?php

namespace App\Entity;

use App\Entity\Employee\Employee;
use App\Services\ImageCrop\Config;
use App\Services\ImageCrop\Crop;
use App\Services\ImageCrop\Size;
use Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


/**
 * App\Entity\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $role
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $phone
 * @property string|null $photo
 * @property string|null $city
 * @property-read \App\Entity\Employee\Employee|null $employee
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Entity\Salon|null $salon
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User user()
 * @property int|null $staff_id
 * @property int|null $salon_id
 * @property-read \App\Entity\Staff|null $staff
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User current()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereSalonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereStaffId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereCity($value)
 */
class User extends Authenticatable
{
    public const ROLE_CLIENT = 'client';
    public const ROLE_USER = 'user';
    public const ROLE_MODERATOR = 'moder';
    public const ROLE_ADMIN = 'admin';

    use Notifiable, HasApiTokens, Crop;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'first_name', 'last_name', 'phone', 'photo', 'city'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function rolesList(): array
    {
        return [
            self::ROLE_CLIENT => 'Клиент',
            self::ROLE_USER => 'Пользователь',
            self::ROLE_MODERATOR => 'Модератор',
            self::ROLE_ADMIN => 'Администратор',
        ];
    }

    public static function new($email, $password, $role = null): self
    {
        return static::create([
            'name' => $email,
            'email' => $email,
            'password' => Hash::make($password),
            'role' => $role
        ]);
    }

    public static function getByPhone($phone) : ?self
    {
        if (strlen($phone) < 9) {
            throw new BadRequestHttpException('Минимальное количество символов - 9');
        }

        return User::query()->where('phone', 'LIKE', "%$phone%")->firstOrFail();
    }

    public function isAdmin()
    {
        return $this->role = User::ROLE_ADMIN;
    }

    public function isModer()
    {
        return $this->role = User::ROLE_MODERATOR;
    }

    public function isUser()
    {
        return $this->role = User::ROLE_USER;
    }

    public function isClient()
    {
        return $this->role = User::ROLE_CLIENT;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    public function clearTokens($device_name)
    {
        $this->tokens()->where('name', $device_name)->delete();
        return $this;
    }

    public function getRoleAttribute($value)
    {
        return self::rolesList()[$value];
    }

    public function setImageCropConfig()
    {
        $this->imageCrop->setConfig(new Config('photo', 'user', 'id', [
            new Size('preview', '104', '104'),
            new Size('grid', '104', '140')
        ]));
    }

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function isNewStaffCreated()
    {
        return ($this->getOriginal('staff_id') == null && $this->staff_id != null);
    }

    public function isStaffFilled()
    {
        return ($this->staff_id != null);
    }

    public function scopeUser($query)
    {
        return $query->whereRole(User::ROLE_USER);
    }

    public function scopeCurrent($query)
    {
        return $query->whereSalonId(Auth::user()->salon->id ?? 0)->orWhere('id', '=', Auth::user()->id);
    }

    public function salon()
    {
        return $this->belongsTo(Salon::class);
    }

    public function employee()
    {
        return $this->hasOne(Employee::class);
    }

    public function staff()
    {
        return $this->belongsTo(Staff::class);
    }

    public function assignSalon($salon_id)
    {
        $this->salon_id = $salon_id;
    }
}
