<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Review
 *
 * @property int $id
 * @property string|null $text
 * @property string|null $rating
 * @property int $user_id
 * @property int $order_id
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Review newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Review newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Review query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Review whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Review whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Review whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Review whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Review whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Review whereUserId($value)
 * @mixin \Eloquent
 */
class Review extends Model
{
    const STATUS_DRAFT = 'draft'; //default
    const STATUS_PUBLISH = 'publish';

    protected $fillable = ['user_id', 'order_id', 'rating', 'text'];

    public static function isExists($user_id, $order_id)
    {
        return self::whereUserId($user_id)->whereOrderId($order_id)->exists();
    }
}
