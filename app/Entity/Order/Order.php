<?php

namespace App\Entity\Order;

use App\Entity\Client;
use App\Entity\Employee\Employee;
use Illuminate\Database\Eloquent\Model;
use LaravelFillableRelations\Eloquent\Concerns\HasFillableRelations;

/**
 * App\Entity\Order\Order
 *
 * @property int $id
 * @property int $client_id
 * @property int $employee_id
 * @property string $date
 * @property string $session_start
 * @property string $session_end
 * @property string $status
 * @property string $priority
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order whereSessionEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order whereSessionStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $source
 * @property string $client_status
 * @property-read \App\Entity\Client $client
 * @property-read \App\Entity\Employee\Employee $employee
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Order\OrderService[] $services
 * @property-read int|null $services_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order whereClientStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\Order whereSource($value)
 */
class Order extends Model
{
    use HasFillableRelations;

    public const STATUS_WAITING = 'waiting';
    public const STATUS_ACCEPT = 'accept';
    public const STATUS_COME = 'come';
    public const STATUS_CANCEL = 'cancel';

    public const PRIORITY_LOW = 'low';
    public const PRIORITY_HIGH = 'high';

    public const SOURCE_MOBILE = 'mobile';
    public const SOURCE_ADMIN = 'admin';

    protected $fillable = ['client_id', 'employee_id', 'priority', 'date', 'session_start', 'session_end', 'status', 'source'];
    protected $casts = ['date' => 'date'];


    protected $fillable_relations = ['services'];

    public static function statusesList(): array
    {
        return [
            self::STATUS_WAITING => 'Ожидаем клиента',
            self::STATUS_ACCEPT => 'Клиент подтвердил',
            self::STATUS_COME => 'Клиент пришел',
            self::STATUS_CANCEL => 'Клиент не пришел',
        ];
    }

    public static function priorityList(): array
    {
        return [
            self::PRIORITY_HIGH => 'Высокий',
            self::PRIORITY_LOW => 'Низкий',
        ];
    }

    public static function sourcesList(): array
    {
        return [
            self::SOURCE_ADMIN => 'Админ. панель',
            self::SOURCE_MOBILE => 'Мобильное приложение',
        ];
    }

    public static function getStatus($status)
    {
        return self::statusesList()[$status];
    }

    public static function getPriority($priority)
    {
        return self::priorityList()[$priority];
    }

    public static function getSource($source)
    {
        return self::sourcesList()[$source];
    }

    public function isWaiting()
    {
        return $this->status === self::STATUS_WAITING;
    }

    public function isAccept()
    {
        return $this->status === self::STATUS_ACCEPT;
    }

    public function changeToCancel()
    {
        $this->status = self::STATUS_CANCEL;
    }

    public function services()
    {
        return $this->hasMany(OrderService::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
