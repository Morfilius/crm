<?php

namespace App\Entity\Order;

use App\Entity\Service\Service;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Order\OrderService
 *
 * @property int $id
 * @property int $order_id
 * @property int $service_id
 * @property int $qty
 * @property int $price
 * @property int $discount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\OrderService newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\OrderService newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\OrderService query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\OrderService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\OrderService whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\OrderService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\OrderService whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\OrderService wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\OrderService whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\OrderService whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Order\OrderService whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Entity\Order\Order $order
 * @property-read \App\Entity\Service\Service|null $service
 */
class OrderService extends Model
{
    protected $fillable = ['service_id', 'qty', 'price', 'discount'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
    public function getCost()
    {
        return $this->price  - ($this->price * ($this->discount/100));
    }

}
