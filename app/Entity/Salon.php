<?php

namespace App\Entity;

use App\Entity\Employee\Employee;
use App\Entity\Service\Service;
use App\Services\ImageCrop\Config;
use App\Services\ImageCrop\Crop;
use App\Services\ImageCrop\Size;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Salon
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string|null $photo
 * @property string $rating
 * @property string $type
 * @property string $address
 * @property string $description
 * @property string $latitude
 * @property string $longitude
 * @property string $work_time
 * @property string $company_type
 * @property string $inn
 * @property string $bank_name
 * @property string $company_name
 * @property string $kpp
 * @property string $correspondent_account
 * @property string $company_address
 * @property string $bik
 * @property string $checking_account
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Employee\Employee[] $employees
 * @property-read int|null $employees_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Service\Service[] $services
 * @property-read int|null $services_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereBik($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereCheckingAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereCompanyAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereCompanyType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereCorrespondentAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereKpp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Salon whereWorkTime($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Service\Service[] $clients
 * @property-read int|null $clients_count
 */
class Salon extends Model
{
    use Crop;

    protected $fillable = ['title', 'photo', 'type', 'address', 'description', 'latitude', 'longitude', 'work_time',
        'company_type', 'inn', 'bank_name', 'company_name', 'kpp', 'correspondent_account', 'company_address', 'bik', 'checking_account'];

    public function setImageCropConfig()
    {
        $this->imageCrop->setConfig(new Config('photo', 'salon', 'id', [
            new Size('preview', '104', '104'),
            new Size('grid', '104', '124')
        ]));
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    public static function getList()
    {
        return self::all();
    }

    public function services()
    {
        return $this->belongsToMany(Service::class);
    }

    public function clients()
    {
        return $this->belongsToMany(Client::class);
    }
}
