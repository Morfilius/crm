<?php

namespace App\Entity;

use Auth;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Staff
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Staff newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Staff newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Staff query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int $salon_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entity\Salon $salon
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Staff current()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Staff whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Staff whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Staff whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Staff whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Staff whereSalonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Staff whereUpdatedAt($value)
 * @property-read \App\Entity\User|null $user
 */
class Staff extends Model
{
    protected $fillable = ['name', 'description'];

    public function scopeCurrent($query)
    {
        return $query->whereSalonId(Auth::user()->salon->id ?? null);
    }

    public static function getList()
    {
        return self::current()->get();
    }

    public function salon()
    {
        return $this->belongsTo(Salon::class);
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
