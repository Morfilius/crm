<?php


namespace App\Filter;


use App\Entity\Client;

class ClientFilter
{
    public function filter($request, $salon_id)
    {
        $query = Client::with(['user'])->whereHas('salons', function ($query) use($salon_id) {
            $query->where('salon_id', '=', $salon_id);
        });

//        if (!empty($value = $request->get('search'))) {
//            if ($role = array_search(mb_strtolower($value), array_map('mb_strtolower',User::rolesList()))) {
//                $query->whereHas('user', function ($query) use($role) {
//                    $query->where('role', $role);
//                });
//            } else {
//                $query->where('id', 0);
//            }
//
//        }
        return $query->paginate(20)->onEachSide(5);
    }
}
