<?php


namespace App\Filter;


use App\Entity\Employee;
use App\Entity\Order\Order;
use App\Entity\User;

class OrderFilter
{
    public function filter($request)
    {
        $query = Order::with(['employee', 'client', 'services.service'])->has('services');

        if ( ! empty($value = $request->get('date'))) {
            $query->where('date', $value);
        }

        if ( ! empty($value = $request->get('employee'))) {
            $query->where('employee_id', $value);
        }

        if ( ! empty($value = $request->get('client'))) {
            $query->where('client_id', $value);
        }

        if ( ! empty($value = $request->get('service'))) {
            $query->whereHas('services', function ($query) use($value) {
                $query->where('service_id', $value);
            });
        }

        if ( ! empty($value = $request->get('time'))) {
            $query->where('session_start', $value);
        }

        if ( ! empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }

        if ( ! empty($value = $request->get('source'))) {
            $query->where('source', $value);
        }

        if ( ! empty($from = $request->get('from')) && ! empty($to = $request->get('to'))) {
            $query->where('date', '>=', $from)->where('date', '<=', $to);
        }

        return $query->paginate();

    }
}
