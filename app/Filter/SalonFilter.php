<?php


namespace App\Filter;


use App\Entity\Salon;

class SalonFilter
{
    public function filter($request)
    {
        $query = Salon::select();

//        if (!empty($value = $request->get('filter'))) {
//            $query->where('status', $value);
//        }
        return $query->paginate(20)->onEachSide(5);
    }
}
