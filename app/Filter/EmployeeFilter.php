<?php


namespace App\Filter;


use App\Entity\Employee\Employee;

class EmployeeFilter
{
    public function filter($request)
    {
        $query = Employee::current();

        if (!empty($value = $request->get('filter'))) {
            $query->where('status', $value);
        }
        return $query->paginate(20)->onEachSide(5);
    }
}
