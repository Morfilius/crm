<?php


namespace App\Filter\Api;



use App\Entity\Order\Order;
use App\Http\Requests\Api\Order\IndexRequest;

class OrderFilter
{
    public function filter(IndexRequest $request)
    {
        $query = Order::select();
        if (isset($request->filter) && ! empty($request->filter)) {
            $query->whereStatus($request->filter);
        }
        return $query->paginate(20);
    }
}
