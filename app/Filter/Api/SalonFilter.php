<?php


namespace App\Filter\Api;


use App\Entity\Salon;
use App\Http\Requests\Api\Salon\IndexRequest;

class SalonFilter
{
    public function filter(IndexRequest $request)
    {
        $query = Salon::with('employees');

        if (isset($request->service_ids) && ! empty($request->service_ids)) {
            $query->whereHas('services', function($q) use($request) {
                    $q->whereIn('id', $request->service_ids);
                });
        }
        return $query->paginate(20);
    }
}
