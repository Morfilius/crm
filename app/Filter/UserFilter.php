<?php


namespace App\Filter;


use App\Entity\Employee;
use App\Entity\User;

class UserFilter
{
    public function filter($request)
    {
        $query = User::select()->user()->current();

        if (!empty($value = $request->get('search'))) {
            if ($role = array_search(mb_strtolower($value), array_map('mb_strtolower',User::rolesList()))) {
                $query->whereHas('user', function ($query) use($role) {
                    $query->where('role', $role);
                });
            } else {
                $query->where('id', 0);
            }

        }
        return $query->paginate(20)->onEachSide(5);
    }
}
