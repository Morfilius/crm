<?php

use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

if (! function_exists('filter_active')) {
    function filter_active($key, $value) {
        return (request($key) === $value);
    }
}

if (! function_exists('time_to_integer')) {
    function time_to_integer($time) {
        list($hour, $minute) = explode(':', $time);
        return $hour*60 + (int)$minute;
    }
}

if (! function_exists('minutes_to_string')) {
    function minutes_to_string($minutes) {
        if ($minutes < 1) {
            return;
        }
        $hours = floor($minutes / 60);
        $minutes = ($minutes % 60);
        return sprintf('%02d:%02d', $hours, $minutes);
    }
}

if (! function_exists('get_elem_by_time')) {
    function get_elem_by_time($collection, $time) {
        $key = array_filter(array_keys($collection), function ($key) use($time){
            $time = time_to_integer($time);
            return ($key >=  $time  && $key <= ($time+59));
        });
        return !empty($key) ? $collection[reset($key)] : false;
    }
}

if (! function_exists('time_to_part')) {
    function time_to_part($time) {
        return [
            'hour' => $time / 60,
            'min' => '00'
        ];
    }
}

if (! function_exists('url_to')) {
    function url_to(array $params = [], $url = null)
    {
        $url = $url ? $url : url()->current();
        $query = request()->all();
        $query = array_filter(array_replace_recursive($query, $params));
        return $query ? $url . '?' . http_build_query($query) : $url;
    }
}

if (! function_exists('responseApi')) {
    function responseApi(array $output = [])
    {
        return response()->json($output, 200, [], JSON_UNESCAPED_SLASHES);
    }
}

if (! function_exists('formattedResponseApi')) {
    function formattedResponseApi(callable $function)
    {
        $defaultData = [
            'success' => true,
            'code' => 200,
            'message' => '',
            'data' => []
        ];

        try{
            $data = $function();
            if($data instanceof AuthenticationException) {
                return array_merge($defaultData, [
                    'success' => false,
                    'code' => 401,
                    'message' => $data->getMessage()
                ]);
            }
        } catch (ValidationException $e) {
            return array_merge($defaultData, [
                'success' => false,
                'code' => $e->status,
                'message' => $e->validator->getMessageBag()->first()
            ]);
        } catch (Exception $e) {
            return array_merge($defaultData, [
                'success' => false,
                'code' => method_exists($e, 'getStatusCode') ? $e->getStatusCode() : $e->getCode(),
                'message' => $e->getMessage()
            ]);
        }

        $links = [];
        if ( isset($data->collection) ) {
            $resourceArr = $data->resource->toArray();
            $links['links'] = [
                'first' => $resourceArr['first_page_url'],
                'last' => $resourceArr['last_page_url'],
                'prev' => $resourceArr['prev_page_url'],
                'next' => $resourceArr['next_page_url'],
            ];
        }

        return responseApi(array_merge($defaultData, [
            'data' => $data
        ], $links));
    }
}
