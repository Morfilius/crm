<?php


namespace App\Repositories;


use App\Entity\Employee\Employee;
use App\Entity\Service\Service;

class ServiceReadRepository
{
    //список всех услуг для салонов
    public function showServicesWithEmployeeList($salon_id)
    {
        return Service::whereHas('employees', function ($query) use($salon_id) {
            $query->where('employees.salon_id', '=', $salon_id);
        })->get()->flatMap(function (Service $service) {
            return $service->employees->map(function (Employee $employee) use ($service) {
                return [
                    'employee' => $employee,
                    'service' => $service,
                ];
            })->toArray();
        });
    }
}
