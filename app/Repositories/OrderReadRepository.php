<?php


namespace App\Repositories;


use App\Entity\Employee\Employee;
use App\Entity\Order\Order;
use Illuminate\Support\Collection;

class OrderReadRepository
{
    public function getBySalon($salon_id): Collection
    {
        $employeeIds = Employee::whereHas('user', function ($query) use($salon_id) {
            $query->whereSalonId($salon_id);
        })->pluck('id')->toArray();
        return Order::whereIn('employee_id', $employeeIds)->has('services')->get();
    }
}
