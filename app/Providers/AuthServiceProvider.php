<?php

namespace App\Providers;

use App\Entity\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerPermissions();
    }

    private function registerPermissions(): void
    {
        Gate::define('user-panel', function (User $user) {
            return $user->isUser();
        });

        Gate::define('moder-panel', function (User $user) {
            return $user->isModer();
        });

        Gate::define('admin-panel', function (User $user) {
            return $user->isAdmin();
        });
    }
}
