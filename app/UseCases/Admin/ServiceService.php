<?php


namespace App\UseCases\Admin;


use App\Entity\Employee\Employee;
use App\Entity\Salon;
use App\Entity\Service\Category;
use App\Entity\Service\RecordExcluded;
use App\Entity\Service\Service;
use App\Http\Requests\Admin\Service\CreateRequest;
use App\Http\Requests\Admin\Service\UpdateRequest;
use DB;

class ServiceService
{
    public function categoryCreate($name, $scope_id)
    {
        $scope = Category::findOrFail($scope_id);

        $category = Category::make([
            'name' => $name,
            'type' => Category::TYPE_CATEGORY
        ]);

        if (! $scope->isScope()) {
            throw new \DomainException('Parent is not scope!');
        }

        $category->assignToScope($scope->id);
        $category->saveOrFail();
    }

    public function create(CreateRequest $request)
    {
        $category = Category::findOrFail($request->category_id);

        $service = Service::make([
            'name' => $request['name'],
            'price' => $request['price'],
            'description' => $request['description'],
            'online' => $request['online'] ?? false,
            'session_start' => $request['session_start'],
            'session_end' => $request['session_end'],
            'session_step' => $request['session_step'],
        ]);
        $service->category()->associate($category);

        $service->saveOrFail();

        if ($request->employee_ids) {
            foreach ($request->employee_ids as $employee_id) {
                $employee = Employee::findOrFail($employee_id);
                $service->employees()->attach($employee->id);
            }
        }

        if ($request->salon_ids) {
            foreach ($request->salon_ids as $salon_id) {
                $salon = Salon::findOrFail($salon_id);
                $service->salons()->attach($salon->id);
            }
        }

        $records = array_map(function ($date) {
            return RecordExcluded::make([
                'date' => $date
            ]);
        }, $request->record ?? []);

        if ($records) {
            $service->records()->saveMany($records);
        }
    }

    public function update(UpdateRequest $request, Service $service)
    {
        $service->fill([
            'name' => $request['name'],
            'price' => $request['price'],
            'description' => $request['description'],
            'online' => $request['online'] ?? false,
            'session_start' => $request['session_start'],
            'session_end' => $request['session_end'],
            'session_step' => $request['session_step'],
        ]);

        if ($request->employee_ids) {
            foreach ($request->employee_ids as $employee_id) {
                $employee = Employee::findOrFail($employee_id);
                $service->employees()->attach($employee->id);
            }
        }

        if ($request->salon_ids) {
            foreach ($request->salon_ids as $salon_id) {
                $salon = Salon::findOrFail($salon_id);
                $service->salons()->attach($salon->id);
            }
        }

        $records = array_map(function ($date) {
            return RecordExcluded::make([
                'date' => $date
            ]);
        }, $request->record ?? []);

        DB::transaction(function () use ($service, $records) {
            $service->saveOrFail();
            if ($records) {
                $service->records()->saveMany($records);
            }
        });
    }

    public function detach(Service $service, Employee $employee)
    {
        $service->employees()->detach($employee->id);
    }
}
