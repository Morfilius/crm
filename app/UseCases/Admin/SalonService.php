<?php


namespace App\UseCases\Admin;


use App\Entity\Employee\Employee;
use App\Entity\Salon;
use App\Http\Requests\Admin\Salon\CreateRequest;
use App\Http\Requests\Admin\Salon\UpdateRequest;

class SalonService
{
    public function create(CreateRequest $request)
    {
        $salon = Salon::make([
            'photo' => $request->file('photo'),
            'title' => $request->title,
            'type' => $request->type,
            'address' => $request->address,
            'description' => $request->description,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'work_time' => $request->work_time,
            'company_type' => $request->company_type,
            'inn' => $request->inn,
            'bank_name' => $request->bank_name,
            'company_name' => $request->company_name,
            'kpp' => $request->kpp,
            'correspondent_account' => $request->correspondent_account,
            'company_address' => $request->company_address,
            'bik' => $request->bik,
            'checking_account' => $request->checking_account
        ]);

        $salon->saveOrFail();

        if ($request->employee_ids) {
            foreach ($request->employee_ids as $employee_id) {
                $employee = Employee::find($employee_id);
                $employee->salon()->associate($salon);
                $employee->saveOrFail();
            }
        }
    }

    public function update(UpdateRequest $request, Salon $salon)
    {
        $salon->fill([
            'photo' => $request->file('photo'),
            'title' => $request->title,
            'type' => $request->type,
            'address' => $request->address,
            'description' => $request->description,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'work_time' => $request->work_time,
            'company_type' => $request->company_type,
            'inn' => $request->inn,
            'bank_name' => $request->bank_name,
            'company_name' => $request->company_name,
            'kpp' => $request->kpp,
            'correspondent_account' => $request->correspondent_account,
            'company_address' => $request->company_address,
            'bik' => $request->bik,
            'checking_account' => $request->checking_account
        ]);
        $salon->save();
        if ($request->employee_ids) {
            foreach ($request->employee_ids as $employee_id) {
                $employee = Employee::find($employee_id);
                $employee->salon()->associate($salon);
                $employee->saveOrFail();
            }
        }
    }
}
