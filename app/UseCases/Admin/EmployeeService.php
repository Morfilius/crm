<?php


namespace App\UseCases\Admin;


use App\Entity\Employee\Employee;
use App\Entity\Employee\EmployeeImages;
use App\Entity\User;
use App\Http\Requests\Admin\Employee\UpdateRequest;
use Carbon\Carbon;
use DB;
use Illuminate\Http\UploadedFile;

class EmployeeService
{
    public function update(UpdateRequest $request, Employee $employee)
    {
        $employee = $employee->fill(['work_time' => $request->work_time, 'address' => $request->address,
            'price' => $request->price, 'images' => [], 'description' => $request->description]);

        $images = [];
        $imageIds = $request->image_ids;

        if ($files = $request->file('image')) {
            $images = array_map(function (UploadedFile $file) {
                return EmployeeImages::make(['photo' => $file]);
            }, $files);
        }

        DB::transaction(function () use($images, $imageIds, $employee) {
            $employee->syncImages($imageIds);
            if ($images) {
                $employee->images()->saveMany($images);
            }
            $employee->saveOrFail();
        });
    }

    public function subscribeToMaster($employee_id, $client_id, Carbon $recordDateTime)
    {
        $employee = Employee::findOrFail($employee_id);
        User::findOrFail($client_id);
        if ( ! $employee->isSubscribeExists($client_id, $recordDateTime)) {
            $employee->subscribers()->attach([$client_id => ['date' => $recordDateTime]]);
        }
    }

    public function likeExample($employee_images_id, $client_id)
    {
        $image = EmployeeImages::findOrFail($employee_images_id);
        User::findOrFail($client_id);
        if ( ! $image->isLiked($client_id)) {
            $image->like()->attach($client_id);
        }
    }

    public function unLikeExample($employee_images_id, $client_id)
    {
        $image = EmployeeImages::findOrFail($employee_images_id);
        User::findOrFail($client_id);
        if ($image->isLiked($client_id)) {
            $image->like()->detach($client_id);
        }
    }
}
