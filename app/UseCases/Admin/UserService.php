<?php


namespace App\UseCases\Admin;


use App\Entity\Employee\Employee;
use App\Entity\Staff;
use App\Entity\User;
use App\Http\Requests\Admin\Users\CreateRequest;
use App\Http\Requests\Admin\Users\UpdateRequest;
use Auth;
use DB;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public $user;

    public function create(CreateRequest $request, $salon_id = null)
    {
        $this->user = User::make([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'name' => $request['email'],
            'password' => Hash::make($request['password']),
            'phone' => $request['phone'],
            'photo' => $request->file('photo')
        ]);
        $this->assignStaff($request['staff']);
        $this->user->assignSalon($salon_id ?? Auth::user()->salon->id);
        $this->user->isStaffFilled() ? $this->save() : $this->user->saveOrFail();
    }

    public function update(UpdateRequest $request, User $user)
    {
        $this->user = $user;
        $this->user->fill(array_filter([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'name' => $request['email'],
            'password' => $request['password'] ? Hash::make($request['password']) : false,
            'phone' => $request['phone'],
            'photo' => $request->file('photo')
        ]));
        $this->assignStaff($request['staff']);
        $this->user->isNewStaffCreated() ? $this->save() : $this->user->saveOrFail();
    }

    public function assignStaff($staff)
    {
        if ($staff) {
            $staff = Staff::whereName($staff)->first();
            $this->user->staff()->associate($staff);
        }
    }

    public function createEmployee()
    {
        $employee = Employee::make();
        $employee->user()->associate($this->user);
        $employee->saveOrFail();
    }

    public function delete(User $user)
    {
        $user->delete();
    }

    private function save()
    {
        DB::transaction(function () {
            $this->user->saveOrFail();
            $this->createEmployee();
        });
    }
}
