<?php


namespace App\UseCases\Admin;


use App\Entity\Order\Order;
use App\Http\Requests\Admin\Order\CreateRequest;
use App\Http\Requests\Admin\Order\UpdateRequest;
use DB;
use Illuminate\Support\Collection;

class OrderService
{
    public function create(CreateRequest $request)
    {
        $order = Order::make([
            'client_id' => $request->client,
            'employee_id' => $request->employee,
            'priority' => $request->priority,
            'date' => $request->date,
            'session_start' => $request->session_start,
            'session_end' => $request->session_end,
            'status' => $request->status,
            'source' => $request->source,
        ]);
        $service = $this->servicesCollect($request->service)->map(function ($item) {
            return [
                'service_id' => $item['name'],
                'qty' => $item['qty'],
                'price' => $item['price'],
                'discount' => $item['discount'],
            ];
        })->toArray();
        DB::transaction(function () use($order, $service) {
            $order->saveOrFail();
            $order->services()->createMany($service);
        });
    }

    public function update(UpdateRequest $request, Order $order)
    {
        $service = $this->servicesCollect($request->service)->map(function ($item) {
            return [
                'service_id' => $item['name'],
                'qty' => $item['qty'],
                'price' => $item['price'],
                'discount' => $item['discount'],
            ];
        })->toArray();

        $order->fill([
            'client_id' => $request->client,
            'employee_id' => $request->employee,
            'priority' => $request->priority,
            'date' => $request->date,
            'session_start' => $request->session_start,
            'session_end' => $request->session_end,
            'status' => $request->status,
            'source' => $request->source,
            'services' => $service
        ]);


        $order->saveOrFail();
//        DB::transaction(function () use($order, $service) {
//            $order->saveOrFail();
//            $order->services()->createMany($service);
//        });
    }



    private function servicesCollect($services) : Collection
    {
        $i = 0; $result = [];
        while (is_array($services) && !empty($services)) {
            foreach ($services as $key => &$value) {
                $result[$i][$key] = array_shift($value);
                if (empty($value)) unset($services[$key]);
            }
            ++$i;
        }
        return collect($result);
    }
}
