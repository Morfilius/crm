<?php


namespace App\UseCases\Admin;


use App\Entity\Client;
use App\Entity\Salon;
use App\Entity\User;
use App\Http\Requests\Admin\Client\CreateRequest;
use App\Http\Requests\Admin\Client\UpdateRequest;
use DB;
use Illuminate\Support\Facades\Hash;

class ClientService
{
    public function create(CreateRequest $request, Salon $salon)
    {
        $user = User::make([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'name' => $request['email'],
            'password' => Hash::make($request['password']),
            'phone' => $request['phone'],
            'photo' => $request->file('photo')
        ]);
        $user->setRole(User::ROLE_CLIENT);

        $client = Client::make([
            'discount' => $request['discount'],
            'status' => $request['status'],
            'source' => $request['source']
        ]);

        DB::transaction(function () use($user, $client, $salon) {
            $user->saveOrFail();
            $client->user()->associate($user);
            $client->saveOrFail();
            $client->salons()->attach($salon);
        });

    }

    public function update(UpdateRequest $request, Client $client)
    {
        $client->fill([
            'discount' => $request['discount'],
            'status' => $request['status'],
            'source' => $request['source']
        ]);
        $client->user->fill([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'name' => $request['email'],
            'password' => $request['password'] ? Hash::make($request['password']) : false,
            'phone' => $request['phone'],
            'photo' => $request->file('photo')
        ]);
        $client->push();
    }

    public function destroy(Client $client)
    {
        $client->salons()->detach();
    }
}
