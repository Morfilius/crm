<?php


namespace App\UseCases\Api;


use App\Entity\User;
use App\Services\Base64Service;

class UserService
{
    public function update(User $user, $first_name, $last_name, $email, $photo, $city)
    {
        $uploadedFile = Base64Service::loadFromBase64($photo);

        $user->fill([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'photo' => $uploadedFile,
            'city' => $city
        ]);

        $user->saveOrFail();
    }
}
