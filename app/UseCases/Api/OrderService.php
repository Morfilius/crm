<?php


namespace App\UseCases\Api;

use App\Entity\Client;
use App\Entity\Employee\Employee;
use App\Entity\Order\Order;
use App\Entity\Service\Service;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class OrderService
{
    public function create($employee_id, $user_id, array $services_ids, Carbon $dateTimeStart)
    {
        $services = Service::findMany($services_ids);

        $client = $this->createClient($employee_id, $user_id);
        $order = $this->makeOrder($employee_id, $client->id, $services, $dateTimeStart);
        $order_services = $this->makeServices($services);

        DB::transaction(function () use($order, $order_services) {
            $order->saveOrFail();
            $order->services()->createMany($order_services);
        });
    }

    private function makeServices($services)
    {
        return $services->map(function (Service $item) {
            return [
                'service_id' => $item->id,
                'qty' => 1,
                'price' => $item->price,
                'discount' => 0,
            ];
        })->toArray();
    }

    private function createClient($employee_id, $user_id)
    {
        $employee = Employee::findOrFail($employee_id);

        $client = Client::whereUserId($user_id)->first();

        if ( ! $client) {
            $client = Client::create([
                'user_id' => $user_id,
                'status' => Client::STATUS_LOW,
                'source' => Client::SOURCE_MOBILE,
            ]);
        }

        if ( ! $client->isClientInSalon($employee->salon_id)) {
            $client->salons()->attach($employee->salon_id);
        }

        return $client;
    }

    private function makeOrder($employee_id, $client_id, $services, Carbon $dateTimeStart)
    {
        $session_start = time_to_integer($dateTimeStart->format('H:i'));
        $session_end = $services->reduce(function ($prev, Service $service) {
            $prev += (int)$service->session_step;
            return $prev;
        }, 0);
        $session_end = $session_start + $session_end;

        return Order::make([
            'client_id' => $client_id,
            'employee_id' => $employee_id,
            'priority' => Order::PRIORITY_LOW,
            'date' => $dateTimeStart->format('Y-m-d'),
            'session_start' => $session_start,
            'session_end' => $session_end,
            'status' => Order::STATUS_WAITING,
            'source' => Order::SOURCE_MOBILE
        ]);
    }
}
