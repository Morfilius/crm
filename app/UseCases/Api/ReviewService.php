<?php


namespace App\UseCases\Api;


use App\Entity\Order\Order;
use App\Entity\Review;
use App\Http\Requests\Api\Review\CreateRequest;

class ReviewService
{
    public function create(CreateRequest $request)
    {
        $order = Order::findOrFail($request->order_id);

        if ( ! Review::isExists($request->user_id, $request->order_id) && $order->isAccept()) {
            Review::create([
                'user_id' => $request->user_id,
                'order_id' => $request->order_id,
                'rating' => $request->rating,
                'text' => $request->text
            ]);
        }
    }
}
