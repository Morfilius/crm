<?php

namespace App\Http\Requests\Admin\Service;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'price' => 'required|integer',
            'description' => 'required|string|max:30000',
            'online' => 'nullable|integer|max:1',
            'session_start' => 'required|string|max:255',
            'session_end' => 'required|string|max:255',
            'session_step' => 'required|string|max:255',
            'master_ids' => 'nullable|array',
            'master_ids.*' => 'nullable|integer',
            'record' => 'nullable|array',
            'record.*' => 'nullable|string',
        ];
    }
}
