<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'nullable|string|max:30000',
            'address' => 'required|string|max:255',
            'work_time' => 'required|string|max:255',
            'price' => 'required|string|max:255',
            'image_ids' => 'nullable|array',
            'image_ids.*' => 'nullable|integer',
        ];
    }
}
