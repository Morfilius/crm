<?php

namespace App\Http\Requests\Admin\Order;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client' => 'required|integer|exists:clients,id',
            'employee' => 'required|integer|exists:employees,id',
            'status' => 'required|string|max:255',
            'priority' => 'required|string|max:255',
            'date' => 'required|string|max:255',
            'source' => 'required|string|max:255',
            'session_start' => 'required|integer',
            'session_end' => 'required|integer',
            'service' => 'required|array',
            'service.name.*' => 'required|integer|exists:services,id',
            'service.qty.*' => 'required|integer',
            'service.price.*' => 'required|integer',
            'service.discount.*' => 'required|integer|max:100',
        ];
    }
}
