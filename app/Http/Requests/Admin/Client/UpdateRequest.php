<?php

namespace App\Http\Requests\Admin\Client;

use App\Entity\Client;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $client =  $this->route('client');
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'discount' => 'nullable|string|max:255',
            'password' => 'nullable|string|max:255',
            'status' => 'required|string|max:255',
            'source' => 'required|string|max:255',
            'email' => ['required','email', 'max:255', Rule::unique('users', 'email')->ignore($client->user->id)],
        ];
    }
}
