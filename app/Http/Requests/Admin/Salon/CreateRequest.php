<?php

namespace App\Http\Requests\Admin\Salon;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'type' => 'string|max:255',
            'address' => 'string|max:255',
            'description' => 'string|max:30000',
            'latitude' => 'string|max:255',
            'longitude' => 'string|max:255',
            'work_time' => 'string|max:255',
            'company_type' => 'string|max:255',
            'inn' => 'string|max:255',
            'bank_name' => 'string|max:255',
            'company_name' => 'string|max:255',
            'kpp' => 'string|max:255',
            'correspondent_account' => 'string|max:255',
            'company_address' => 'string|max:255',
            'bik' => 'string|max:255',
            'checking_account' => 'string|max:255',
            'master_ids' => 'nullable|array',
            'master_ids.*' => 'nullable|integer',
        ];
    }
}
