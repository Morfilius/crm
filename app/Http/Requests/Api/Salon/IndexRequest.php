<?php

namespace App\Http\Requests\Api\Salon;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class IndexRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_ids' => 'nullable|array',
            'service_ids.*' => 'nullable|integer',
        ];
    }
}
