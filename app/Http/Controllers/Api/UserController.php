<?php

namespace App\Http\Controllers\Api;

use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use App\UseCases\Api\UserService;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use function formattedResponseApi;

class UserController extends Controller
{

    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function self()
    {
        return formattedResponseApi(function () {
            $user = Auth::user();
            return new UserResource($user);
        });
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $phone
     */
    public function update(Request $request, $phone)
    {
        return formattedResponseApi(function () use ($phone, $request) {
            $user = User::getByPhone($phone);

            $this->validate($request, [
                'first_name' => 'required|string|min:3|max:255',
                'last_name' => 'required|string|min:3|max:255',
                'email' => ['required','email', 'min:3', 'max:255', Rule::unique('users', 'email')->ignore($user->id)],
                'photo' => 'required|string|min:3',
                'city' => 'required|string|min:3|max:50',
            ]);

            $this->userService->update($user, $request['first_name'], $request['last_name'],
                $request['email'], $request['photo'], $request['city']);
            return [];
        });
    }
}
