<?php

namespace App\Http\Controllers\Api\Employee;

use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Http\Resources\Employee\EmployeeResource;
use App\Http\Resources\Employee\EmployeeResourceCollection;

class EmployeeController extends Controller
{
    public function index()
    {
        return formattedResponseApi(function () {
            $salons = User::where(['role' => User::ROLE_USER])->has('employee')->has('staff')
                ->has('salon')->paginate();
            return new EmployeeResourceCollection($salons);
        });
    }

    public function show($id)
    {
        return formattedResponseApi(function () use($id) {
            $user = User::has('employee')->has('staff')->has('salon')
                ->with('employee.images')->with('employee.services')->findOrFail($id);
            return new EmployeeResource($user);
        });
    }
}
