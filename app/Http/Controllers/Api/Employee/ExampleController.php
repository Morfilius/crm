<?php

namespace App\Http\Controllers\Api\Employee;

use App\Entity\Employee\EmployeeImages;
use App\Http\Controllers\Controller;
use App\Http\Resources\Employee\ExampleResourceCollection;
use App\UseCases\Admin\EmployeeService;
use Auth;

class ExampleController extends Controller
{
    /**
     * @var EmployeeService
     */
    private $service;

    public function __construct(EmployeeService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return formattedResponseApi(function () {
            return new ExampleResourceCollection(EmployeeImages::with('employee')->with('employee.user')
                ->inRandomOrder()->paginate());
        });
    }

    public function like($id)
    {
        return formattedResponseApi(function () use ($id) {
            $this->service->likeExample($id, Auth::id());
            return [];
        });

    }

    public function unLike($id)
    {
        return formattedResponseApi(function () use ($id) {
            $this->service->unLikeExample($id, Auth::id());
            return [];
        });

    }
}
