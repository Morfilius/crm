<?php

namespace App\Http\Controllers\Api;

use App\Filter\Api\SalonFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Salon\IndexRequest;
use App\Http\Resources\Salon\SalonResourceCollection;

class SalonController extends Controller
{
    public function index(IndexRequest $request)
    {
        return formattedResponseApi(function () use($request) {
            $filter = new SalonFilter();
            $salons = $filter->filter($request);
            return new SalonResourceCollection($salons);
        });
    }
}
