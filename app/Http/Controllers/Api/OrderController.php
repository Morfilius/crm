<?php

namespace App\Http\Controllers\Api;

use App\Entity\Order\Order;
use App\Filter\Api\OrderFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Order\IndexRequest;
use App\Http\Resources\Order\OrderResourceCollection;
use App\UseCases\Api\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * @var OrderService
     */
    private $service;

    public function __construct(OrderService $service)
    {
        $this->service = $service;
    }

    public function index(IndexRequest $request)
    {
        return formattedResponseApi(function () use ($request) {
            $filter = new OrderFilter();
            return new OrderResourceCollection($filter->filter($request));
        });
    }

    public function store(Request $request)
    {
        return formattedResponseApi(function () use ($request) {
            $this->validate($request, [
                'employee_id' => 'required|integer',
                'date_start' => 'required|integer',
                'service_ids' => 'required|array',
                'service_ids.*' => 'required|integer',
            ]);
            $dateTime = Carbon::createFromTimestamp($request->date_start);
            $this->service->create($request->employee_id, Auth::id(), $request->service_ids, $dateTime);
            return [];
        });
    }

    public function cancel($id)
    {
        return formattedResponseApi(function () use ($id) {
            $order = Order::findOrFail($id);
            if ($order->isWaiting()) {
                $order->changeToCancel();
                $order->saveOrFail();
            }
            return [];
        });

    }
}
