<?php

namespace App\Http\Controllers\Api\Service;

use App\Entity\Service\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\Service\SphereResourceCollection;

class SphereController extends Controller
{
    public function index()
    {
        return formattedResponseApi(function () {
               $parentCategories = Category::query()->where(['parent_id' => null])->paginate();
               return new SphereResourceCollection($parentCategories);
            });
    }
}
