<?php

namespace App\Http\Controllers\Api\Service;

use App\Entity\Service\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\Service\CategoryResourceCollection;

class CategoryController extends Controller
{
    public function index()
    {
        return formattedResponseApi(function () {
            $parentCategories = Category::query()->whereNotNull('parent_id')
                ->with('services')->paginate();
            return new CategoryResourceCollection($parentCategories);
        });
    }
}
