<?php


namespace App\Http\Controllers\Api\Auth;


use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    public function login(Request $request)
    {
        return formattedResponseApi(function () use ($request) {
            $this->validate($request, [
                'phone' => 'required|string|max:255|min:9',
                'device_name' => 'required',
            ]);

            $user = User::getByPhone($request['phone']);

            return ['token' => $user->clearTokens($request->device_name)->createToken($request->device_name)->plainTextToken];
        });
    }
}
