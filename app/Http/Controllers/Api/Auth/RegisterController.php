<?php


namespace App\Http\Controllers\Api\Auth;


use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        return formattedResponseApi(function () use ($request) {
            $this->validate($request, [
                'phone' => 'required|string|max:255|min:9|unique:users',
                'device_name' => 'required|string'
            ]);

            $user = User::create([
                'phone' => $request['phone'],
                'role' => User::ROLE_CLIENT,
                'password' => Hash::make($request['password']),
            ]);

            $token = $user->createToken($request->device_name)->plainTextToken;

            return ['token' => $token];
        });
    }
}
