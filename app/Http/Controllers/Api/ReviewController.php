<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Review\CreateRequest;
use App\UseCases\Api\ReviewService;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * @var ReviewService
     */
    private $service;

    public function __construct(ReviewService $service)
    {
        $this->service = $service;
    }

    public function store(CreateRequest $request)
    {
        return formattedResponseApi(function () use ($request) {
            $this->service->create($request);
            return [];
        });
    }
}
