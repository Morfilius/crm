<?php

namespace App\Http\Controllers\Admin\Order;

use App\Entity\Order\Order;
use App\Filter\OrderFilter;
use App\Http\Controllers\Controller;
use App\Repositories\OrderReadRepository;
use Auth;
use Illuminate\Http\Request;

class OrderListController extends Controller
{
    /**
     * @var OrderReadRepository
     */
    private $orderReadRepository;

    public function __construct(OrderReadRepository $orderReadRepository)
    {
        $this->orderReadRepository = $orderReadRepository;
    }

    public function calendar(Request $request)
    {
        $orders = (new OrderFilter())->filter($request);
        $ordersByTimeAndEmployee = $orders->reduce(function ($result, Order $item) {
            $result[$item->session_start][$item->employee_id]= $item;
            return $result;
        }, []);
        $salon = Auth::user()->salon;
        $calendars = [];
        $this->orderReadRepository->getBySalon($salon->id)->unique()->each(function (Order $order) use (&$calendars) {
            $calendars[$order->date->format('Ym')]['year'] = $order->date->year;
            $calendars[$order->date->format('Ym')]['month'] = $order->date->month;
            $calendars[$order->date->format('Ym')]['orders'][] = $order;
        });
        return view('admin.order.calendar', compact('orders', 'ordersByTimeAndEmployee', 'salon', 'calendars'));
    }

    public function list(Request $request)
    {
        $orders = (new OrderFilter())->filter($request);
        $salon = Auth::user()->salon;
        return view('admin.order.list', compact('orders', 'salon'));
    }

    public function employees(Request $request)
    {
        $orders = (new OrderFilter())->filter($request);
        $salon = Auth::user()->salon;
        return view('admin.order.employees', compact('salon', 'orders'));
    }

}
