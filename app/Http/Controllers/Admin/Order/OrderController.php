<?php

namespace App\Http\Controllers\Admin\Order;

use App\Entity\Order\Order;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Order\CreateRequest;
use App\Http\Requests\Admin\Order\UpdateRequest;
use App\Repositories\ServiceReadRepository;
use App\UseCases\Admin\OrderService;
use Auth;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * @var ServiceReadRepository
     */
    private $serviceReadRepository;
    /**
     * @var OrderService
     */
    private $orderService;

    public function __construct(ServiceReadRepository $serviceReadRepository, OrderService $orderService)
    {
        $this->serviceReadRepository = $serviceReadRepository;
        $this->orderService = $orderService;
    }

    public function create()
    {
        $salon = Auth::user()->salon;
        return view('admin.order.create', compact('salon'));
    }

    public function store(CreateRequest $request)
    {
        $this->orderService->create($request);
        return redirect()->route('admin.order.list.calendar');
    }

    public function edit(Order $order)
    {
        $salon = Auth::user()->salon;
        return view('admin.order.edit', compact('order', 'salon'));
    }

    public function update(UpdateRequest $request, Order $order)
    {
        $this->orderService->update($request, $order);
        return redirect()->route('admin.order.list.list');
    }

    public function destroy(Order $order)
    {
        $order->delete();
        return back();
    }
}
