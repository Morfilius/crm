<?php

namespace App\Http\Controllers\Admin\Service;

use App\Http\Controllers\Controller;
use App\UseCases\Admin\ServiceService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var ServiceService
     */
    private $serviceService;

    public function __construct(ServiceService $serviceService)
    {
        $this->serviceService = $serviceService;
    }

    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:service_categories,name'
        ]);

        $this->serviceService->categoryCreate($request->name, $id);

        return redirect()->route('admin.service.index');
    }
}
