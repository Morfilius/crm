<?php

namespace App\Http\Controllers\Admin\Service;

use App\Entity\Service\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ScopeController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:service_categories,name'
        ]);

        Category::create([
            'name' => $request['name'],
            'type' => Category::TYPE_SCOPE,
        ]);

        return redirect()->route('admin.service.index');
    }
}
