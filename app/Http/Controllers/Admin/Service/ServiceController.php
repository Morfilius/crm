<?php

namespace App\Http\Controllers\Admin\Service;

use App\Entity\Employee\Employee;
use App\Entity\Service\Category;
use App\Entity\Service\Service;
use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Service\CreateRequest;
use App\Http\Requests\Admin\Service\UpdateRequest;
use App\UseCases\Admin\ServiceService;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * @var ServiceService
     */
    private $serviceService;

    public function __construct(ServiceService $serviceService)
    {
        $this->serviceService = $serviceService;
    }

    public function index($active_category_id = null)
    {
        $scopes = Category::whereType(Category::TYPE_SCOPE)->get();
        $active_category = Category::whereType(Category::TYPE_CATEGORY)->whereId($active_category_id)->first();
        return view('admin.service.index', compact('scopes', 'active_category_id', 'active_category'));
    }

    public function create($category_id)
    {
        return view('admin.service.create', compact('category_id'));
    }

    public function store(CreateRequest $request)
    {
        $this->serviceService->create($request);
        return redirect()->route('admin.service.index');
    }

    public function edit(Service $service)
    {
        return view('admin.service.edit', compact('service'));
    }

    public function update(UpdateRequest $updateRequest, Service $service)
    {
        $this->serviceService->update($updateRequest, $service);
        return redirect()->route('admin.service.index');
    }

    public function detach(Service $service, Employee $employee)
    {
        $this->serviceService->detach($service, $employee);
        return back();
    }

    public function destroy($id)
    {
        //
    }
}
