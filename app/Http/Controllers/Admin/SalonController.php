<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Salon;
use App\Filter\SalonFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Salon\CreateRequest;
use App\Http\Requests\Admin\Salon\UpdateRequest;
use App\Repositories\ServiceReadRepository;
use App\UseCases\Admin\SalonService;
use Illuminate\Http\Request;

class SalonController extends Controller
{
    /**
     * @var SalonService
     */
    private $salonService;
    /**
     * @var ServiceReadRepository
     */
    private $serviceReadRepository;

    public function __construct(SalonService $salonService, ServiceReadRepository $serviceReadRepository)
    {
        $this->salonService = $salonService;
        $this->serviceReadRepository = $serviceReadRepository;
    }

    public function index(Request $request)
    {
        $salons = (new SalonFilter())->filter($request);
        return view('admin.salon.index', compact('salons'));
    }

    public function create()
    {
        return view('admin.salon.create');
    }

    public function store(CreateRequest $request)
    {
        $this->salonService->create($request);
        return redirect()->route('admin.salon.index');
    }

    public function show(Salon $salon)
    {
        $services = $services = $this->serviceReadRepository->showServicesWithEmployeeList($salon->id);
        return view('admin.salon.show', compact('salon', 'services'));
    }

    public function edit(Salon $salon)
    {

        return view('admin.salon.edit', compact('salon'));
    }

    public function update(UpdateRequest $request, Salon $salon)
    {
        $this->salonService->update($request, $salon);
        return redirect()->route('admin.salon.index');
    }

    public function destroy($id)
    {
        //
    }
}
