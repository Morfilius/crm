<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Staff;
use App\Filter\StaffFilter;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    public function index(Request $request)
    {
        $staffs = (new StaffFilter())->filter($request);
        return view('admin.staff.index', compact('staffs'));
    }

    public function create()
    {
        return view('admin.staff.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:255'
        ]);

        $staff = Staff::make($request->only('name', 'description'));
        $staff->salon()->associate(Auth::user()->salon);
        $staff->save();

        return redirect()->route('admin.staff.index');
    }

    public function edit(Staff $staff)
    {
        return view('admin.staff.edit', compact('staff'));
    }

    public function update(Request $request, Staff $staff)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:255'
        ]);

        $staff->update($request->only('name', 'description'));
        return redirect()->route('admin.staff.index');
    }

    public function destroy(Staff $staff)
    {
        $staff->delete();
        return redirect()->route('admin.staff.index');
    }
}
