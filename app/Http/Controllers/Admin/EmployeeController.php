<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Employee\Employee;
use App\Filter\EmployeeFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Employee\UpdateRequest;
use App\UseCases\Admin\EmployeeService;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{

    /**
     * @var EmployeeService
     */
    private $employeeService;

    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    public function index(Request $request)
    {
        $employees = (new EmployeeFilter())->filter($request);
        return view('admin.employee.index', compact('employees'));
    }

    public function show(Employee $employee)
    {
        return view('admin.employee.show', ['employee' => $employee]);
    }

    public function edit(Employee $employee)
    {
        return view('admin.employee.edit', ['employee' => $employee]);
    }

    public function update(UpdateRequest $request, Employee $employee)
    {
        $this->employeeService->update($request, $employee);
        return redirect()->route('admin.employee.index');
    }

    public function block(Employee $employee)
    {
//        $employee->block();
        return redirect()->route('admin.employee.index');
    }
}
