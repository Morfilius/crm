<?php


namespace App\Http\Controllers\Admin;


use App\Entity\Employee\Employee;
use App\Entity\User;
use App\Filter\UserFilter;
use App\Http\Requests\Admin\Users\CreateRequest;
use App\Http\Requests\Admin\Users\UpdateRequest;
use App\UseCases\Admin\UserService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class UsersController extends Controller
{
    /**
     * @var userService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        $users = (new UserFilter())->filter($request);
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(CreateRequest $request)
    {
        $this->userService->create($request);
        return redirect()->route('admin.users.index');
    }

    public function edit(User $user)
    {
        return view('admin.users.edit', ['user' => $user]);
    }

    public function update(UpdateRequest $request, User $user)
    {
        $this->userService->update($request, $user);
        return redirect()->route('admin.users.index');
    }

    public function destroy(User $user)
    {
        $this->userService->delete($user);
        return redirect()->route('admin.users.index');
    }
}
