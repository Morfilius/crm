<?php

namespace App\Http\Controllers\Admin\Manager;

use App;
use App\Entity\Salon;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Salon\UpdateRequest;
use App\Repositories\ServiceReadRepository;
use App\UseCases\Admin\SalonService;
use Auth;

class SalonController extends Controller
{
    /**
     * @var SalonService
     */
    private $salonService;
    /**
     * @var ServiceReadRepository
     */
    private $serviceReadRepository;

    public function __construct(SalonService $salonService, ServiceReadRepository $serviceReadRepository)
    {
        $this->salonService = $salonService;
        $this->serviceReadRepository = $serviceReadRepository;
    }

    public function index()
    {
        $salon = Auth::user()->salon;
        $services = $this->serviceReadRepository->showServicesWithEmployeeList($salon->id ?? 0);
        return view('admin.salon.manager.index', compact('salon', 'services'));
    }

    public function edit()
    {
        $salon = Auth::user()->salon;
        return view('admin.salon.manager.edit', compact('salon'));
    }

    public function update(UpdateRequest $request, Salon $salon)
    {
        $this->salonService->update($request, $salon);
        return redirect()->route('admin.salon.manager.index');
    }
}
