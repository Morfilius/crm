<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Client;
use App\Filter\ClientFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Client\CreateRequest;
use App\Http\Requests\Admin\Client\UpdateRequest;
use App\UseCases\Admin\ClientService;
use Auth;
use Request;

class ClientController extends Controller
{
    /**
     * @var ClientService
     */
    private $clientService;

    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    public function index(Request $request)
    {
        $clients = (new ClientFilter())->filter($request, Auth::user()->salon->id);
        return view('admin.client.index', compact('clients'));
    }

    public function create()
    {
        return view('admin.client.create');
    }

    public function store(CreateRequest $request)
    {
        $this->clientService->create($request, Auth::user()->salon);
        return redirect()->route('admin.client.index');
    }

    public function show(Client $client)
    {
        return view('admin.client.show', compact('client'));
    }

    public function edit(Client $client)
    {
        return view('admin.client.edit', compact('client'));
    }


    public function update(UpdateRequest $request, Client $client)
    {
        $this->clientService->update($request, $client);
        return redirect()->route('admin.client.index');
    }

    public function destroy(Client $client)
    {
        $this->clientService->destroy($client);
        return back();
    }
}
