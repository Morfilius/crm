<?php

namespace App\Http\Resources\Service;

use App\Entity\Service\Category;
use App\Entity\Service\Service;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function toArray($request)
    {
        return $this->collection->map(function (Category $item) {
            return [
                'name' => $item->name,
                'services' => $item->services->map(function (Service $service) {
                    return [
                        'id' => $service->id,
                        'name' => $service->name
                    ];
                })
            ];
        });
    }
}
