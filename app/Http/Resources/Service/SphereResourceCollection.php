<?php

namespace App\Http\Resources\Service;

use App\Entity\Service\Category;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SphereResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function toArray($request)
    {
        return $this->collection->map(function (Category $item) {
            return [
                'id' => $item->id,
                'name' => $item->name
            ];
        });
    }
}
