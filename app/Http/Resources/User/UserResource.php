<?php

namespace App\Http\Resources\User;

use App\Entity\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 *  @property  User $resource
 *
*/

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request

     * @return array
     */
    public function toArray($request)
    {
        return [
            'photo' => $this->resource->getOriginalImage(),
            'first_name' => $this->resource->first_name,
            'last_name' => $this->resource->last_name,
            'email' => $this->resource->email,
            'city' => $this->resource->city
        ];
    }
}
