<?php

namespace App\Http\Resources\Salon;

use App\Entity\Salon;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SalonResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function toArray($request)
    {
        return $this->collection->map(function (Salon $item) {
            return [
                'id' => $item->id,
                'photo' => $item->getOriginalImage(),
                'title' => $item->title,
                'type' => $item->type,
                'address' => $item->address,
            ];
        });
    }
}
