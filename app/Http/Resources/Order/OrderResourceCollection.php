<?php

namespace App\Http\Resources\Order;

use App\Entity\Order\Order;
use App\Entity\Order\OrderService;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function toArray($request)
    {
        return $this->collection->map(function (Order $order) {
            return [
                'order_id' => $order->id,
                'status' => $order->status,
                'image' => $order->employee->user->getOriginalImage(),
                'title' => trim($order->services->reduce(function ($prev, OrderService $service) {
                    $prev .= ' ' . $service->service->name;
                    return $prev;
                }, '')),
                'price' => $order->services->reduce(function ($prev, OrderService $service) {
                    $prev += $service->service->price;
                    return $prev;
                }, 0),
                'salon_title' => $order->employee->salon->title,
                'address' => $order->employee->salon->address,
                'record_date_time' => $order->date->format('d.m.Y') . ' ' . \minutes_to_string($order->session_start),
                'master_fist_name' => $order->employee->user->first_name,
                'master_last_name' => $order->employee->user->last_name,
            ];
        });
    }
}
