<?php

namespace App\Http\Resources\Employee;

use App\Entity\Employee\EmployeeImages;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ExampleResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function toArray($request)
    {
        return $this->collection->map(function (EmployeeImages $image) {
            return [
                'example_id' => $image->id,
                'title' => $image->employee->user->fullName(),
                'person_icon' =>$image->employee->user->getOriginalImage(),
                'image' => $image->getOriginalImage(),
            ];
        });
    }
}
