<?php

namespace App\Http\Resources\Employee;

use App\Entity\User;
use Illuminate\Http\Resources\Json\ResourceCollection;

class EmployeeResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function toArray($request)
    {
        return $this->collection->map(function (User $item) {
            return [
                'id' => $item->employee->id,
                'photo' => $item->getOriginalImage(),
                'first_name' => $item->first_name,
                'last_name' => $item->last_name,
                'staff' => $item->staff->name,
                'address' => $item->salon->address,
            ];
        });
    }
}
