<?php

namespace App\Http\Resources\Employee;

use App\Entity\Employee\EmployeeImages;
use App\Entity\Service\Service;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * App\Entity\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $role
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $phone
 * @property string|null $photo
 * @property string|null $city
 * @property-read \App\Entity\Employee\Employee|null $employee
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Entity\Salon|null $salon
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User user()
 * @property int|null $staff_id
 * @property int|null $salon_id
 * @property-read \App\Entity\Staff|null $staff
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User current()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereSalonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereStaffId($value)
 */

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->employee->id,
            'photo' => $this->getOriginalImage(),
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'staff' => $this->staff->name,
            'address' => $this->salon->address,
            'description' => $this->employee->description,
            'images' => $this->employee->images->map(function (EmployeeImages $image) {
                return $image->getOriginalImage();
            }),
            'services' => $this->employee->services->map(function (Service $item) {
                return [
                    'name' => $item->name,
                    'price' => $item->name
                ];
            }),
        ];
    }
}
