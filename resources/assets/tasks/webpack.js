'use strict';

const webpack = require('webpack');
const path = require('path');
const notifier = require('node-notifier');
const gulplog = require('gulplog');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

module.exports = function(options,gulp) {
    return function(callback) {
        let options = {
            entry:   {
                main: './src/js/main'
            },
            output:  {
                path:     path.resolve('../../public/js'),
                publicPath: '/js/',
                filename: '[name].js',
                library: "app"
            },
            mode: (isDevelopment)?'development':'production',
            watch:   isDevelopment,
            devtool: isDevelopment ? 'eval' : false,
            optimization: {
                splitChunks: {
                    chunks: "all",
                    minChunks: 1,
                    cacheGroups: {
                        libs: {
                            name:'libs',
                            test: /[\\/]node_modules[\\/]/,
                        },
                    }
                }
            },
            plugins: [
                new webpack.ProvidePlugin({
                    $: 'jquery',
                    jQuery: 'jquery'
                })
            ],
            module: {
                rules: [
                    {
                        test: /\.m?js$/,
                        exclude: /(node_modules|bower_components)/,
                        use: {
                            loader: 'babel-loader',
                            options: {
                                presets: ['@babel/preset-env']
                            }
                        }
                    }
                ]
            }
        };

        webpack(options, function(err, stats) {
            if (!err) { // no hard error
                // try to get a soft error from stats
                err = stats.toJson().errors[0];
            }
            if (err) {
                notifier.notify({
                    title: 'Webpack',
                    message: err
                });
                gulplog.error(err);
            } else {
                gulplog.info(stats.toString({
                    colors: true
                }));
            }
            // task never errs in watch mode, it waits and recompiles
            if (!options.watch && err) {
                callback(err);
            } else {
                callback();
            }

        });
    };
};
