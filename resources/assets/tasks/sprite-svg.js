'use strict';

const $ = require('gulp-load-plugins')();
const combine = require('stream-combiner2').obj;
const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
const svgSprite   = require('gulp-svg-sprite');

module.exports = function(options,gulp) {
    return function() {
        return combine(
            gulp.src(options.src),
            $.svgmin({
                js2svg: {
                    pretty: true
                }
            }),
            $.cheerio({
                run: function ($) {
                    $('[fill]').removeAttr('fill');
                    $('[stroke]').removeAttr('stroke');
                    $('[style]').removeAttr('style');
                },
                parserOptions: {xmlMode: true}
            }),
            $.replace('&gt;', '>'),
            svgSprite({
                mode: {
                    symbol: {
                        dest: '.',
                        sprite: "sprite.svg",
                        render: {
                            sass: {
                                dest: 'sprite-svg.sass',
                                template: 'template.txt'
                            }
                        }
                    }
                }
            }),
            $.if('*.svg',gulp.dest(options.output),gulp.dest('./src/sprites/style'))
        ).on('error', $.notify.onError({title: 'sass'}));
    };
};