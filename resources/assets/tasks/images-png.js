'use strict';

const $ = require('gulp-load-plugins')();
const imageminPng = require('imagemin-pngquant');
const combine = require('stream-combiner2').obj;

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

module.exports = function(options,gulp) {
    return function() {
        return combine(
            gulp.src(options.src),
            $.newer(options.output),
            $.imagemin(imageminPng({
                quality: '70-90',
                speed: 1,
                floyd: 1
            })),
            gulp.dest(options.output)
        ).on('error', $.notify.onError({title: 'images-png'}));
    };
};