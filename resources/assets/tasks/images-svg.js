'use strict';

const $ = require('gulp-load-plugins')();
const combine = require('stream-combiner2').obj;

module.exports = function(options,gulp) {
    return function() {
        return combine(
            gulp.src(options.src),
            gulp.dest(options.output)
        ).on('error', $.notify.onError({title: 'images-svg'}));
    };
};
