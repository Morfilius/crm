'use strict';

const $ = require('gulp-load-plugins')();
const jpegoptim   = require('imagemin-jpegoptim');
const combine = require('stream-combiner2').obj;

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

module.exports = function(options,gulp) {
    return function() {
        return combine(
            gulp.src(options.src),
            $.newer(options.output),
            $.imagemin([jpegoptim({
                progressive:true,
                max: 85,
                stripAll: true
            })],{verbose:true}),
            gulp.dest(options.output)
        ).on('error', $.notify.onError({title: 'images-jpg'}));
    };
};