'use strict';

const $ = require('gulp-load-plugins')();
const combine = require('stream-combiner2').obj;

module.exports = function(options,gulp) {
    return function() {
        return gulp.src(options.src)
            .pipe($.concat('libs.min.css'))
            .pipe($.cssnano())
            .pipe(gulp.dest(options.output))
    };
};