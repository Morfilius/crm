'use strict';

const $ = require('gulp-load-plugins')();
const fileinclude = require('gulp-file-include');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

module.exports = function(options,gulp) {
    return function() {
       return gulp.src(options.src)
                .pipe(fileinclude({
                   prefix: '@@',
                   basepath: '@file'
                }))
                .pipe(gulp.dest(options.output))
    };
};