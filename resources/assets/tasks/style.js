'use strict';

const $ = require('gulp-load-plugins')();
const combine = require('stream-combiner2').obj;

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

module.exports = function(options,gulp) {
    return function() {
        return combine(
            gulp.src(options.src),
            $.replace('~', '../../node_modules/'),
            $.if(isDevelopment, $.sourcemaps.init()),
            $.sass(),
            $.autoprefixer({
                cascade: false
            }),
            gulp.dest(options.output)
        ).on('error', $.notify.onError({title: 'sass'}));
    };
};