'use strict';

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

module.exports = function(options,gulp) {
    return function() {
        return gulp.src(options.src)
            .pipe(gulp.dest(options.output))
    };
};