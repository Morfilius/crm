'use strict';

const $ = require('gulp-load-plugins')();
const imageminPng = require('imagemin-pngquant');
const spritesmith = require('gulp.spritesmith');
const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

module.exports = function(options,gulp) {
    return function(done) {
        let spriteData = gulp.src('src/sprites/*.png')
            .pipe($.imagemin(imageminPng({
                quality: '70-90',
                speed: 1,
                floyd: 1
            })))
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: 'sprite-png.sass',
                cssFormat: 'sass',
                padding: 1,
                imgPath: '../img/sprite.png'
            }));
        spriteData.img.pipe(gulp.dest(options.output));
        spriteData.css.pipe(gulp.dest('./src/sprites/style'));
        done()
    };
};
