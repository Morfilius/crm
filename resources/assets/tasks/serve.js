'use strict';

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

module.exports = function(browserSync,gulp) {
    return function() {
        browserSync.init({
            server: 'dist',
            notify: false,
            reloadDelay: 150
        });
    }
};
