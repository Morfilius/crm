import $ from 'jquery';
export default {
    init() {
        this.registerEvents();
    },
    registerEvents() {
        $('.new-image').on('click', this.fileBrowser)
        $('.upload-single-input').change(this.previewImage);
        $('.image-remove').on('click',this.imageRemove)
    },
    fileBrowser(e) {
        e.stopPropagation();
        if (e.target.tagName === 'INPUT') return;
        $(this).siblings('input').click();
    },
    previewImage() {
        var file = this.files[0];
        var reader = new FileReader();
        let $this = $(this);
        if (file) {
            reader.readAsDataURL(file);
        }
        reader.onloadend = function () {
            $this.parent().removeClass('image-select').find('.image-preview').attr('src', reader.result);
            $this.siblings('input').val('');
        };
    },
    imageRemove(e) {
        if($(this).attr('href') === '#') {
            e.preventDefault();
            $(this).parent().addClass('image-select').find('input').val('');
        }
    }
}