export default {
    init() {
        this.registerEvents();
    },
    registerEvents() {
        const $this = this;
        const container = $('.select-constructor').each(function () {
            $(this).on('click', '.select-constructor__control-btn', (e) => {
                e.preventDefault(); $this.addItem(e.target);
            });
            $(this).on('click', '.image-remove', (e) => {e.preventDefault(); $this.removeItem(e.target)})
        });

    },
    addItem(el) {
        const
            item = $(el),
            container = item.closest('.select-constructor'),
            selectItem = container.find('.select-constructor__select option:selected');

        if (!!selectItem.data('type')) {
            switch (selectItem.data('type')) {
                case ('person') : {
                    this.itemPerson(selectItem);break;
                }
                case ('metro') : {
                    this.itemMetro(selectItem);break;
                }
            }

        }
    },
    itemPerson(item) {
        const
            container = item.closest('.select-constructor'),
            name = container.data('name'),
            content = container.find('.person-list'),
            image = item.data('image'),
            value = item.val(),
            text = item.text();

        const html = `<div class="person-item select-constructor__item">
                        <div class="person-item__img-wrap">
                            <img src="${image}" alt="" class="image border-round">
                            <a href="#" class="image-remove person-item__remove"></a>
                        </div>
                        <input type="hidden" name="${name}[]" value="${value}">
                        <span class="person-item__text">${text.replace(' ','<br>')}</span>
                    </div>`;

        content.append(html)
    },
    itemMetro(item) {
        const container = item.closest('.select-constructor'),
        content = container.find('.string-constructor-list'),
        name = container.data('name'),
        value = item.val(),
        text = item.text();

        const html = `<span class="string-constructor-item select-constructor__item">${text}
                            <a href="#" class="image-remove string-constructor-item__remove"></a>
                             <input type="hidden" name="${name}[]" value="${value}">
                        </span>`;
        content.append(html)
    },
    removeItem(item) {
        item.closest('.select-constructor__item').remove();
    }
}