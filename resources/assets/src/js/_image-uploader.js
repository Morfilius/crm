import $ from 'jquery';
export default {
    init() {
        this.registerEvents();
    },
    registerEvents() {
        $('.image-uploader__new-image').on('click', this.fileBrowser)
        $('.upload-input').change(this.previewImage);
        $('.image-uploader').on('click', '.image-remove   ',this.imageRemove)
    },
    fileBrowser(e) {
        e.stopPropagation();
        if (e.target.tagName === 'INPUT') return;
        $(this).find('input').click();
    },
    previewImage() {
        var file = this.files[0];
        var reader = new FileReader();
        let $this = $(this);
        if (file) {
            reader.readAsDataURL(file);
        }
        reader.onloadend = function () {

            $this.closest('.image-uploader').find('.image-uploader__item:last-child').before(`<div class="image-uploader__item">
<img src="${reader.result}" alt="" class="image-uploader__image">
<a href="#" class="image-remove"></a></div>`);
            $this.closest('.image-uploader__item').prev('.image-uploader__item').append($this.clone().addClass('dn').attr('name','image[]'));
        };
    },
    imageRemove(e) {
        if($(this).attr('href') === '#') {
            e.preventDefault();
            $(this).parent().remove()
        }
    }
}