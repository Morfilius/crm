import $ from 'jquery';
import './zabuto_calendar.min';
import 'slick-carousel';
import 'dragscroll';
import '@chenfengyuan/datepicker';
import 'select2';
import autosize from 'autosize';
import raty from 'raty-js';
import number from './_number';
import imageUploader from './_image-uploader';
import singleImageUploader from './_single-image-uploader.js';
import selectConstructor from './_select-constructor.js';
import map from './_map.js';
import Inputmask from "inputmask";
import 'bootstrap/js/dist/collapse';
import 'bootstrap/js/dist/modal';
import PerfectScrollbar from 'perfect-scrollbar';

calendarSize();

if (document.querySelector('.main')) {
    const ps = new PerfectScrollbar('.main');
}
$(document).ready(function () {

    //menu
    !function(){
        $('.collapse-menu-btn').on('click', function () {
            $('body').toggleClass('collapse-menu');
        })
    }();

    //calendar - order
    !function(){
        $('.calendars').slick({
            slidesToShow: 3,
        });

        $('.order-calendar-js').each(function () {
            const
                date = JSON.parse(this.dataset.date),
                links = JSON.parse(this.dataset.links);
            let checked = [];
            if (this.dataset.checked) {
                checked = JSON.parse(this.dataset.checked);
            }

            $(this).zabuto_calendar({
                language: "ru",
                year: date.year,
                month: date.month,
                show_previous: true,
                show_next: true,
                action: function () {
                    if ($(this).hasClass('event')) {
                        const data = $(this).attr('id').match(/[-\d]+$/)[0];
                        location.replace(links[data]);
                    }
                },
                data: checked
            });
        })

    }();

    //calendar - order - employee
    !function(){

        $('.calendar-orders-employee-js').each(function () {
            const link = this.dataset.link;
            $(this).zabuto_calendar({
                language: "ru",
                // year: date.year,
                // month: date.month,
                show_previous: true,
                show_next: true,
                action: function () {
                    const
                        row = $(this).closest('tr'),
                        firstChild = row.find('td.dow-clickable'),
                        lastChild = row.find('td.dow-clickable'),
                        from = firstChild.eq(0).attr('id').match(/[-\d]+$/)[0],
                        to = lastChild.eq(lastChild.length - 1).attr('id').match(/[-\d]+$/)[0];

                    location.replace(link.replace('_from', from).replace('_to', to));
                },
            });
        })

    }();

    //calendar - salon
    !function(){

        $('.calendar-service-js').each(function () {
            let checked = [];
            if (this.dataset.checked) {
                checked = JSON.parse(this.dataset.checked);
            }

            $(this).zabuto_calendar({
                language: "ru",
                show_previous: true,
                show_next: true,
                action: function () {
                    const
                        container = $(this).closest('.calendar'),
                        inputName = container.data('name'),
                        data = $(this).attr('id').match(/[-\d]+$/)[0];
                    $(`[data-id=${data}]`).remove();

                    if (! $(this).hasClass('event')) {
                        container.append(`<input type="hidden" data-id="${data}" name="${inputName}[]" value="${data}">`);
                        $(this).addClass('event')
                    }else {
                        $(this).removeClass('event')
                    }

                },
                 data: checked
            });
        })

    }();

    //datepicker
    !function () {
        $.fn.datepicker.languages['ru-RU'] = {
            format: 'dd.mm.yyyy',
            days: [ 'Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
            daysShort: ['Вс', 'Пон', 'Вт', 'Ср', 'Чет', 'Пят', 'Сб'],
            daysMin: ['Вс', 'Пон', 'Вт', 'Ср', 'Чет', 'Пят', 'Сб'],
            months: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            weekStart: 1,
            startView: 0,
            yearFirst: true,
            yearSuffix: 'г'
        };
        $('.date').datepicker({
            language: 'ru-RU',
            autoHide: true
        });
    }();

    //select2
    !function(){
        $('.select').select2();
        $('.select').select2();
    }();

    //employee slider
    !function(){
        $('.order-employee-slider-js').slick({
            slidesToShow: 5,
        });
    }();

    //rating
    !function(){
        $('.rating').raty({
            starHalf:    '/img/star-half.png',
            starOff:     '/img/star-off.png',
            readOnly:    true,
            starOn:      '/img/star-on.png',
            hints:       ['очень плохо', 'плохо', 'нормально', 'хорошо', 'отлично'],
            noRatedMsg:  'Без рейтинга'
        });
    }();

    //accordion
    !function(){
        $('.collapse').on('show.bs.collapse', function () {
            $(this).siblings('button').addClass('show-collapse')
        })
        $('.collapse').on('hide.bs.collapse', function () {
            $(this).siblings('button').removeClass('show-collapse')
        })
    }();

    //phone
    !function(){
        Inputmask("+7 (999) 999-99-99").mask(document.querySelectorAll('.phone'));
    }();

    //map
    !function(){
        $('#mapModal').on('shown.bs.modal', function (e) {
            if (! document.querySelector('ymaps')){
                map.init();
            }
        })
    }();

    //trash
    !function () {
        $('.trash-js').on('click', function (e) {
            e.preventDefault();
            $(this).siblings('.modal').modal('show')
        })
        $('.trash-modal-ok-js').on('click', function (e) {
            e.preventDefault();
            $(this).closest('.modal').modal('hide');
            $(this).closest('form').submit();
        })
        $('.trash-modal-cancel-js').on('click', function (e) {
            e.preventDefault();
            $(this).closest('.modal').modal('hide');
        })

    }();

    //services category add
    !function () {
        $('.accordion-menu__add').click(function (e) {
            e.preventDefault();
            const
                csrf = $('meta[name="csrf-token"]').attr('content'),
                url = $(this).attr('href');
            $('#new-category').remove();
            $(this).parent().append(`<form action="${url}" method="post" id="new-category" class="new-category">
                                        <input name="_token" type="hidden" value="${csrf}">
                                        <input type="text" name="name" class="new-category__item">
                                    </form>`);
            $('#new-category input').focus();
        });
    }();

    //delete btn
    !function () {
        $('.ajax-form button').click(function (e) {
            e.preventDefault();
            const
                csrf = $('meta[name="csrf-token"]').attr('content'),
                url = $(this).closest('.ajax-form').data('action'),
                method = $(this).closest('.ajax-form').data('method');
            $('#ajax-form').remove();
            $('body').append(`<form action="${url}" method="post" id="ajax-form" class="dn">
                                        <input name="_token" type="hidden" value="${csrf}">
                                        <input name="_method" type="hidden" value="${method}">
                                    </form>`);
            $('#ajax-form').submit();
        });
    }();

    //create orders
    !function () {
        if (!document.querySelector('#orders')) return;
        const
            table = $('#orders'),
            body = table.find('tbody'),
            service = table.data('service');


        let serviceHtml;
        service.forEach(function (item) {
            serviceHtml+= `<option value="${item.id}">${item.name}</option>`;
        });

        const row = `<tr class="table-body">
                            <td class="table-body__item"><select class="select" name="service[name][]">${serviceHtml}</select></td>
                            <td class="table-body__item tac"> - </td>
                            <td class="table-body__item tac">
                            <label class="number"><input type="number" value="1" name="service[qty][]" class="input-border tac maw60"></label></td>
                            <td class="table-body__item tac"><input type="text" name="service[price][]" class="input-border maw80 tac"></td>
                            <td class="table-body__item tac"><input type="text" name="service[discount][]" class="input-border maw80 tac" ></td>
                            <td class="table-body__item tac">
                                -
                            </td>
                            <td class="table-body__item table-body__item--control tac">
                                <div class="control-links">
                                    <a href="#" class="control-links__item row-delete-js" style="background-image:url('/img/trash.svg');"></a>
                                </div>
                            </td>
                        </tr>`;



        $('#order-create').on('click', function (e) {
            e.preventDefault();
            body.append(row);
            $(".select").select2();
            number.init();
        })
    }();

    !function () {
        $('table').on('click', '.row-delete-js', function (e) {
            e.preventDefault();
            $(this).closest('tr').remove();
        })
    }();

    //simple code
    !function(){
        autosize(document.querySelectorAll('textarea'));
        calendarEvents();
        number.init();
        imageUploader.init();
        singleImageUploader.init();
        selectConstructor.init();
    }();


});

function calendarEvents() {
    $('.calendar-event').each(function () {
        const
            $this = $(this),
            body = $this.find('.calendar-event__body'),
            height = $this.closest('tr').height(),
            from = this.dataset.from ? JSON.parse(this.dataset.from) : 0,
            to = this.dataset.to ? JSON.parse(this.dataset.to) : 0;

       const hours = to.hour - from.hour;
       if (hours > 0) {
           body.height(hours * height - 24)
       }
    })
}

function calendarSize() {
    const
        calendarOrders = document.querySelector('.calendar-orders'),
        cols = document.querySelectorAll('.calendar-employee__employee-col'),
        hoursCols = document.querySelector('.calendar-employee__hours-col');

    if(!calendarOrders) return false;

    let width = calendarOrders.clientWidth,
        tableWidth = 0,
        hoursColWidth = 92,
        employeeColWidth = (width - hoursColWidth) * 0.25;

    if (!cols || !hoursCols) return  false

    hoursCols.style.width = hoursColWidth + 'px';
    tableWidth += hoursColWidth;
    for (var i = 0 ; i < cols.length; ++i ) {
        cols[i].style.width = employeeColWidth + 'px';
        tableWidth += Math.floor(employeeColWidth);
    }
    document.querySelector('.calendar-employee').style.width = tableWidth + 'px';
}