import $ from 'jquery';
export default {
    template: '<label class="number" id="inserted"><span class="number__plus">+</span><input type="number"><span class="number__minus">-</span></label>',
    init: function (){
        this.draw();
    },
    draw()
    {
        const $this = this;
        $('[type=number]').not('.number-added').each(function () {
            const parent = $(this).parent()
            parent.append($this.template)
            $('#inserted').removeAttr('id').find('input').after(this).remove();
            $this.addEventListeners(this);
            $(this).addClass('number-added');
        })
    },
    addEventListeners(number)
    {
        $(number).siblings('.number__plus').on('click', function () {
            $(number).val(parseFloat($(number).val()) + 1)
        })
        $(number).siblings('.number__minus').on('click', function () {
            if($(number).val() > 0) {
                $(number).val(parseFloat($(number).val()) - 1)
            }
        })
    }
}