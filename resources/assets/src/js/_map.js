import $ from 'jquery';
export default {
    init() {

        ymaps.ready(init);

        let geoObject;

        function init() {
            var myPlacemark,
                myMap = new ymaps.Map('map', {
                    center: [55.753994, 37.622093],
                    zoom: 9,
                    controls: ['zoomControl']
                });

            // Слушаем клик на карте.
            myMap.events.add('click', function (e) {
                var coords = e.get('coords');
                makePlacemark(coords);
            });

            function makePlacemark(coords) {
                myMap.geoObjects.removeAll()
                myPlacemark = createPlacemark(coords);
                myMap.geoObjects.add(myPlacemark);
                // Слушаем событие окончания перетаскивания на метке.
                myPlacemark.events.add('dragend', function () {
                    getAddress(myPlacemark.geometry.getCoordinates());
                });
                getAddress(coords);
            }

            // Создание метки.
            function createPlacemark(coords) {
                return new ymaps.Placemark(coords, {
                    iconCaption: 'поиск...'
                }, {
                    preset: 'islands#violetDotIconWithCaption',
                    draggable: true
                });
            }

            // Определяем адрес по координатам (обратное геокодирование).
            function getAddress(coords) {
                myPlacemark.properties.set('iconCaption', 'поиск...');
                ymaps.geocode(coords).then(function (res) {
                    geoObject = res.geoObjects.get(0);

                    myPlacemark.properties
                        .set({
                            // Формируем строку с данными об объекте.
                            iconCaption: [
                                // Название населенного пункта или вышестоящее административно-территориальное образование.
                                //firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                                // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                                geoObject.getThoroughfare() || geoObject.getPremise(),
                                geoObject.getPremiseNumber()
                            ].filter(Boolean).join(', '),
                            // В качестве контента балуна задаем строку с адресом объекта.
                            balloonContent: geoObject.getAddressLine()
                        });
                });
            }

            // Создадим элемент управления поиск
            var searchControl = new ymaps.control.SearchControl({
                options: {
                    noPlacemark: true
                }
            });

            // Добавим поиск на карту
            myMap.controls.add(searchControl);

            // Нужное нам событие (выбор результата поиска)
            searchControl.events.add('resultselect', function(e) {
                var index = e.get('index');
                searchControl.getResult(index).then(function(res) {
                    const coords = res.geometry.getCoordinates();
                    makePlacemark(coords);
                    /* Можем ставить метку */


                });
            })


            $('#save-addr').on('click', function () {
                if (!geoObject) return;

                const
                    street = geoObject.getThoroughfare() || geoObject.getPremise(),
                    house = geoObject.getPremiseNumber();

                if (!house || !street) return;

                const address = street + ', ' + house;
                const [latitude, longitude] = geoObject.geometry.getCoordinates();

                $('#address').text(address);
                $('#latitude').val(latitude);
                $('#longitude').val(longitude);
                $('#address_input').val(address);
                $('#mapModal').modal('hide')
            })

        }

    }
}