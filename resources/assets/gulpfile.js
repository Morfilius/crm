const gulp        = require('gulp');
const browserSync = require('browser-sync').create();
const reload      = browserSync.reload;
const $ = require('gulp-load-plugins')();

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

function lazyRequireTask(taskName, path, options) {
    options = options || {};
    options.taskName = taskName;
    gulp.task(taskName, function(callback) {
        let task = require('./tasks/'+path).call(this, options,gulp);

        return task(callback);
    });
}

lazyRequireTask('serve','serve', browserSync);
lazyRequireTask('style','style',{src: 'src/sass/libs.sass',output:'src/libs/'});
lazyRequireTask('style:main','style-main',{src: 'src/sass/style.sass',output:'../../public/css',bs: browserSync});
lazyRequireTask('html','html',{src: 'src/html/*.html',output:'dist/'});//копивание html файлов
lazyRequireTask('fonts','fonts',{src: 'src/fonts/**/*',output:'dist/fonts/'});
lazyRequireTask('clean','clean');//удаление папки dist
lazyRequireTask('images:jpg','images-jpg',{src: 'src/img/**/*.{jpg,png}',output:'../../public/img'});
lazyRequireTask('images:png','images-png',{src: 'src/img/**/*.png',output:'../../public/img'});
lazyRequireTask('images:svg','images-svg',{src: 'src/img/**/*.svg',output:'../../public/img'});
lazyRequireTask('sprite:png','sprite-png',{src: 'src/sprites/**/*.png',output:'dist/img'});
lazyRequireTask('webpack','webpack');
lazyRequireTask('csslibs','libscss',{src: [
        'src/libs/libs.css',
        'node_modules/perfect-scrollbar/css/perfect-scrollbar.css',
   ],output:'../../public/css/'});



gulp.task('images',gulp.parallel('images:jpg','images:png', 'images:svg'));

gulp.task('build', gulp.series('clean', gulp.parallel('html','fonts',gulp.series('style','style:main','csslibs'),'images','sprite:png','webpack')));

gulp.task('default',
    gulp.series(
        'build',
        gulp.parallel(
            'serve',
            function() {
                gulp.watch('src/sass/**/*.{sass,scss}', gulp.series('style:main'));
                gulp.watch('src/sass/**/(libs|bootstrap-vars).{sass,scss}', gulp.series('style','csslibs')).on("change", reload);
                gulp.watch('src/libs/**/*.css', gulp.series('csslibs')).on("change", reload);
                gulp.watch('src/sass/**/fonts.{sass,scss}', gulp.series('style','fonts')).on("change", reload);
                gulp.watch('src/html/**/*.html', gulp.series('html')).on("change", reload);
                gulp.watch('src/img/**/*', gulp.series('images')).on("change", reload);
                gulp.watch('src/sprites/**/*.{svg,png}', gulp.series('sprite:png')).on("change", reload);
                gulp.watch('src/js/**/*.js').on("change", reload);
            }
        )
    )
);
