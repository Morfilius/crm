<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Проект</title>
    <meta name="viewport" content="width=1500px">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/libs.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<div class="wrapper">
    <aside class="sidebar">
        <div class="collapse-menu-btn">
            <span class="collapse-menu-btn__inner"></span>
        </div>
        <nav class="menu-wrap">
            <ul class="menu">
                <!--li class="menu__item active">
                    <a href="#" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/folder.svg') }})"></span>
                        <span class="menu__text">Заказы</span>
                    </a>
                </li>
                <li class="menu__item">
                    <a href="#" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/review.svg') }})"></span>
                        <span class="menu__text">Отзывы</span>
                    </a>
                </li>
                <li class="menu__item">
                    <a href="#" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/persons.svg') }})"></span>
                        <span class="menu__text">Клиенты</span>
                    </a>
                </li>
                <li class="menu__item">
                    <a href="#" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/bell.svg') }})"></span>
                        <span class="menu__text">Публикации</span>
                    </a>
                </li>
                <li class="menu__item">
                    <a href="#" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/house.svg') }})"></span>
                        <span class="menu__text">Профиль салона</span>
                    </a>
                </li>
                <li class="menu__item">
                    <a href="#" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/settings.svg') }})"></span>
                        <span class="menu__text">Управление услугами</span>
                    </a>
                </li-->
                <!--li class="menu__item ">
                    <a href="#" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/bell.svg') }})"></span>
                        <span class="menu__text">Публикации</span>
                    </a>
                </li-->
                <li class="menu__item  {{ Request::routeIs('admin.order*') ? 'active' : '' }}">
                    <a href="{{ route('admin.order.list.calendar') }}" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/folder.svg') }})"></span>
                        <span class="menu__text">Заказы</span>
                    </a>
                </li>
                <li class="menu__item {{ Request::routeIs('admin.client*') ? 'active' : '' }}">
                    <a href="{{ route('admin.client.index') }}" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/persons.svg') }})"></span>
                        <span class="menu__text">Клиенты</span>
                    </a>
                </li>
                <li class="menu__item {{ Request::routeIs('admin.salon.manager*') ? 'active' : '' }}">
                    <a href="{{ route('admin.salon.manager.index') }}" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/house.svg') }})"></span>
                        <span class="menu__text">Профиль салона</span>
                    </a>
                </li>
{{--                <li class="menu__item">--}}
{{--                    <a href="{{ route('admin.salon.index') }}" class="menu__link">--}}
{{--                        <span class="menu__icon" style="background-image: url({{ asset('img/house.svg') }})"></span>--}}
{{--                        <span class="menu__text">Салоны (админ)</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
                <li class="menu__item {{ Request::routeIs('admin.service*') ? 'active' : '' }}">
                    <a href="{{ route('admin.service.index') }}" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/settings.svg') }})"></span>
                        <span class="menu__text">Управление услугами</span>
                    </a>
                </li>
                <li class="menu__item {{ Request::routeIs('admin.employee*') ? 'active' : '' }}">
                    <a href="{{ route('admin.employee.index') }}" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/master.svg') }})"></span>
                        <span class="menu__text">Сотрудники</span>
                    </a>
                </li>
                <li class="menu__item {{ Request::routeIs('admin.staff*') ? 'active' : '' }}">
                    <a href="{{ route('admin.staff.index') }}" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/staff.svg') }})"></span>
                        <span class="menu__text">Должности</span>
                    </a>
                </li>
                <li class="menu__item {{ Request::routeIs('admin.users*') ? 'active' : '' }}">
                    <a href="{{ route('admin.users.index') }}" class="menu__link">
                        <span class="menu__icon" style="background-image: url({{ asset('img/employee.svg') }})"></span>
                        <span class="menu__text">Пользователи</span>
                    </a>
                </li>
            </ul>
        </nav>
    </aside>
    <div class="content">
        @yield('content')
    </div>
</div>
<script src="{{ asset('js/libs.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
