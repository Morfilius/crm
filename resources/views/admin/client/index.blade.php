@extends('layouts.main')
@section('content')
    <header class="header">
        <h1 class="page-title">Клиенты</h1>
        <div class="top-buttons">
            <a href="{{ route('admin.client.create') }}" class="button button--black">Добавить</a>
        </div>
    </header>
    <main class="main">
        <div class="orders-list">
            <form action="/" class="order-top">
                <div class="order-top__inner">
                    <div class="input-row">
                        <div class="date-created">
                            <span class="date-created__title">Дата создания c </span>
                            <label class="input-arrow">
                                <input type="text" placeholder="дд.мм.гггг" class="date-created__input date">
                            </label>
                            по
                            <div class="input-arrow">
                                <input type="text" placeholder="дд.мм.гггг" class="date-created__input date">
                            </div>
                        </div>
                    </div>
                    <div class="input-row">
                        <div class="mr90">
                            <select name="employee" data-placeholder="Выберите сотрудника" class="select w320" id="">
                                <option></option>
                                <option value="1">Иванов Виктор Иванович</option>
                                <option value="2">Сидоров Иван Николаевич</option>
                                <option value="3">Петров Петр Петрович</option>
                            </select>
                        </div>
                        <div class="mr90">
                            <select name="employee" data-placeholder="Выберите пользователя" class="select w320" id="">
                                <option></option>
                                <option value="1">Пользователь 1</option>
                                <option value="2">Пользователь 2</option>
                                <option value="3">Пользователь 3</option>
                            </select>
                        </div>
                        <div>
                            <select name="employee" data-placeholder="Выберите услугу" class="select w320" id="">
                                <option></option>
                                <option value="1">Услуга 1</option>
                                <option value="2">Услуга 2</option>
                                <option value="3">Услуга 3</option>
                                <option value="4">Услуга 4</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-row">
                        <input type="text" placeholder="Введите имя или телефон клиента" class="input">
                    </div>
                    <div class="input-row">
                        <div class="mr50">
                            <select name="employee" data-placeholder="Выберите время визита" class="select w250">
                                <option></option>
                                <option value="1">09:00</option>
                                <option value="2">10:00</option>
                                <option value="2">11:00</option>
                                <option value="2">12:00</option>
                                <option value="2">13:00</option>
                            </select>
                        </div>
                        <div class="mr50">
                            <select name="employee" data-placeholder="Выберите источник" class="select w250">
                                <option></option>
                                <option value="1">Мобильное приложение</option>
                                <option value="1">Веб сайт</option>
                            </select>
                        </div>
                        <div class="mr50">
                            <select name="employee" data-placeholder="Статус клиента" class="select  w250">
                                <option></option>
                                <option value="1">Клиент пришел</option>
                                <option value="1">Клиент не пришел</option>
                            </select>
                        </div>
                        <button class="button button--black mla">Применить</button>
                    </div>
                </div>

            </form>
            @if(! $clients->isEmpty())
                <table class="table">
                    <tr class="table-header">
                        <th class="table-header__item">Имя клиента</th>
                        <th class="table-header__item">Email</th>
                        <th class="table-header__item">Номер телефона</th>
                        <th class="table-header__item">Всего оплачено, руб.</th>
                        <th class="table-header__item">Скидка, %</th>
                        <th class="table-header__item">Статус клиента</th>
                        <th class="table-header__item">Источник</th>
                        <th class="table-header__item table-header__item--control"></th>
                    </tr>
                    @foreach($clients as $client)
                    <tr class="table-body">
                        <td class="table-body__item">{{ $client->user->fullName() }}</td>
                        <td class="table-body__item tac">{{ $client->user->email }}</td>
                        <td class="table-body__item tac">{{ $client->user->phone }}</td>
                        <td class="table-body__item tac">-</td>
                        <td class="table-body__item tac">{{ $client->discount }}</td>
                        <td class="table-body__item tac">
                            {{ App\Entity\Client::getStatus($client->status) }}
                        </td>
                        <td class="table-body__item tac">
                            {{ App\Entity\Client::getSource($client->source) }}
                        </td>
                        <td class="table-body__item table-body__item--control tac">
                            <div class="control-links">
                                <a href="{{ route('admin.client.show', $client) }}" class="control-links__item" style="background-image:url({{ asset('img/edit.svg') }});"></a>
                                <form class="trash-form" action="{{ route('admin.client.destroy', $client) }}" method="POST" >
                                    @csrf
                                    @method('DELETE')
                                    <button class="control-links__item trash-js" data-toggle="modal" data-target="#delete" style="background-image:url( {{ asset('img/trash.svg') }});"></button>
                                    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered maw400" role="document">
                                            <div class="modal-content">
                                                <button type="button" class="map-container__close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <div class="modal-body ">
                                                    <div class="modal-inner tac">
                                                        Вы действительно хотите удалить данную запись?
                                                    </div>
                                                    <div class="modal-trash-control">
                                                        <button class="button button--border-black trash-modal-ok-js">Да</button>
                                                        <button class="button button--black trash-modal-cancel-js">Отмена</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
            @endif
        </div>
    </main>
@endsection
