@extends('layouts.main')
@section('content')
    <form method=POST enctype='multipart/form-data' action="{{ route('admin.client.store') }}">
        @csrf
        <header class="header">
            <div class="back-wrap">
                <a href="{{ route('admin.client.index') }}" class="back-link"></a>
                <h1 class="page-title">Карточка клиента</h1>
            </div>
            <div class="top-buttons">
                <button class="button button--red">Сохранить</button>
            </div>
        </header>
        <main class="main">
            <div class="publish-single">
                <div class="preview-sect">
                    <div class="preview">
                        <div class="image-preview-wrap image-preview-wrap--big image-select">
                            <img src="" alt="" class="image-preview">
                            <div class="new-image"></div>
                            <input type="file" name="photo" class="upload-single-input">
                            <a href="#" class="image-remove"></a>
                        </div>
                        <div class="preview-info-with-titl pl-4">
                            <div class="user-info">
                                <label class="user-info__row feedback-wrap feedback-wrap--auto df">
                                    <span class="bold">Имя:</span> &nbsp;
                                    <input type="text" name="first_name" placeholder="Введите имя" value="{{ old('first_name') }}" class="input">
                                    <span class="invalid-feedback">{{ $errors->first('first_name') }}</span>
                                </label><br>
                                <label class="user-info__row feedback-wrap feedback-wrap--auto df"><span class="bold">Фамилия:</span> &nbsp;
                                    <input type="text" name="last_name" placeholder="Введите фамилию" value="{{ old('last_name') }}" class="input">
                                    <span class="invalid-feedback">{{ $errors->first('last_name') }}</span>
                                </label><br>
                                <label class="user-info__row feedback-wrap feedback-wrap--auto df"><span class="bold">Email:</span> &nbsp;
                                    <input type="email" name="email" placeholder="Введите Email" value="{{ old('email') }}" class="input">
                                    <span class="invalid-feedback">{{ $errors->first('email') }}</span>
                                </label><br>
                                <label class="user-info__row feedback-wrap feedback-wrap--auto df"><span class="bold">Пароль:</span> &nbsp;
                                    <input type="password" name="password" placeholder="**********" value="{{ old('password') }}" class="input">
                                    <span class="invalid-feedback">{{ $errors->first('password') }}</span>
                                </label><br>
                                <label class="user-info__row feedback-wrap feedback-wrap--auto df"><span class="bold">Телефон:</span> &nbsp;
                                    <input type="text" name="phone" placeholder="Введите телефон" value="{{ old('phone') }}" class="input phone">
                                    <span class="invalid-feedback">{{ $errors->first('phone') }}</span>
                                </label><br>
                                <label class="user-info__row feedback-wrap feedback-wrap--auto df"><span class="bold">Скидка,%:</span> &nbsp;
                                    <input type="text" name="discount" placeholder="Введите скидку" value="{{ old('discount') }}" class="input">
                                    <span class="invalid-feedback">{{ $errors->first('discount') }}</span>
                                </label><br>
                                <label class="user-info__row feedback-wrap feedback-wrap--auto mb-3"><span class="bold">Статус:</span> &nbsp;
                                    <select name="status" data-placeholder="--" class="select select--no-border w90">
                                        <option></option>
                                        <option value="low" selected>Низкий</option>
                                        <option value="high">Высокий</option>
                                    </select>
                                    <span class="invalid-feedback">{{ $errors->first('status') }}</span>
                                </label><br>
                                <label class="user-info__row feedback-wrap feedback-wrap--auto"><span class="bold">Источник:</span> &nbsp;
                                    <select name="source" data-placeholder="--" class="select w150 select--no-border">
                                        <option></option>
                                        <option value="mobile">Мобильное приложение</option>
                                        <option value="admin" selected>Админ. панель</option>
                                    </select>
                                    <span class="invalid-feedback">{{ $errors->first('source') }}</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </form>
@endsection
