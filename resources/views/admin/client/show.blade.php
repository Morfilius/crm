@extends('layouts.main')
@section('content')
    <header class="header">
        <div class="back-wrap">
            <a href="{{ route('admin.client.index') }}" class="back-link"></a>
            <h1 class="page-title">Карточка публикации</h1>
        </div>
        <div class="top-buttons">
            <a href="{{ route('admin.client.destroy', $client) }}" class="button button--border-black">Удалить</a>
        </div>
    </header>
    <main class="main">
        <div class="publish-single">
            <div class="preview-sect">
                <div class="preview">
                    <div class="image-preview-wrap image-preview-wrap--big ">
                        <img src="{{ $client->user->getThumb('preview') }}" alt="" class="image-preview">
                    </div>
                    <div class="preview-info-with-title">
                        <div class="user-info">
                            <span class="preview-info__title">{{ $client->user->fullName() }}</span>
                            <div class="user-info">
                                <span class="user-info__row"><span class="bold">Email: </span>{{ $client->user->email }}</span>
                                <span class="user-info__row"><span class="bold">Телефон: </span>{{ $client->user->phone }}</span>
                                <span class="user-info__row"><span class="bold">Скидка,%: </span>{{ $client->user->discount }}</span>
                                <span class="user-info__row"><span class="bold">Статус: </span>{{ App\Entity\Client::getStatus($client->status) }}</span>
                                <span class="user-info__row"><span class="bold">Источник: </span>{{ App\Entity\Client::getSource($client->source) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="preview-btn-wrap">
                    <a href="{{ route('admin.client.edit', $client) }}" class="button button--red">Редактировать</a>
                </div>
            </div>
        </div>
    </main>
@endsection
