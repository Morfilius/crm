@extends('layouts.main')
@section('content')
<header class="header">
    <div class="back-wrap">
        <h1 class="page-title">Сотрудники</h1>
    </div>
</header>
<main class="main">
    <div class="publish-list">
        <form action="/" class="top-control">
            <div class="filter-buttons-list">
                <a href="{{ url()->current() }}" class="filter-button @if(!request('filter')) active @endif">Все</a>
                <a href="?filter={{ \App\Entity\Employee\Employee::MODERATE }}" class="filter-button @if(filter_active('filter', \App\Entity\Employee\Employee::MODERATE)) active @endif">На модерации</a>
                <a href="?filter={{ \App\Entity\Employee\Employee::ACTIVE }}" class="filter-button @if(filter_active('filter', \App\Entity\Employee\Employee::ACTIVE)) active @endif">Активные</a>
                <a href="?filter={{ \App\Entity\Employee\Employee::BLOCKED }}" class="filter-button @if(filter_active('filter', \App\Entity\Employee\Employee::BLOCKED)) active @endif">Заблокированные</a>
            </div>
        </form>
        <div class="list">
            <div class="list__content employee-list">
                @foreach($employees as $employee)
                    <a href="{{ route('admin.employee.show', $employee) }}" class="list-item employee-item">
                        <div class="list-item__img list-item__img--big">
                            <img src="{{ $employee->user->getThumb('grid') }}" class="image" alt="">
                        </div>
                        <div class="list-item__content">
                            <div class="list-item__row">
                                <span class="list-item__title">{{ $employee->user->fullName() }}</span>
                            </div>
                            <div class="list-item__row">
                                <div>
                                    <span class="list-item__row-text">{{ $employee->user->staff->name }}</span><br>
                                    <div class="rating" data-score="{{ $employee->rating }}"></div>
                                </div>
                            </div>
                            <div class="list-item__row">
                                <span class="list-item__row-text">Адрес: {{ $employee->address }}</span>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
            <div class="list__bottom">
                <nav class="pagination-wrap">
                    {{ $employees->links() }}
                </nav>
            </div>
        </div>
    </div>
</main>
@endsection
