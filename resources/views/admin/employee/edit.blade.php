@extends('layouts.main')
@section('content')
    <form action="{{ route('admin.employee.update', $employee) }}" method="POST" class="scroll-content" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <header class="header">
            <div class="back-wrap">
                <a href="{{ route('admin.employee.show', $employee) }}" class="back-link"></a>
                <h1 class="page-title">Карточка сотрудника</h1>
            </div>
            <div class="top-buttons">
                <button class="button button--red">Сохранить</button>
            </div>
        </header>
        <main class="main">
                <div class="publish-single">
                <div class="preview-sect">
                    <div class="preview">
                        <div class="image-preview-wrap image-preview-wrap--big">
                            <img src="{{ $employee->user->getThumb('preview') }}" alt="" class="image-preview">
                        </div>
                        <div class="preview-info-with-title">
                            <span class="preview-info__title">{{ $employee->user->fullName() }}</span><span class="rating ml22" data-score="{{ $employee->rating }}"></span>
                            <table class="preview-info">
                                <tr class="preview-info__row">
                                    <td class="preview-info__col pr32">{{ $employee->user->staff->name }}</td>
                                    <td class="preview-info__col">
                                        <label class="feedback-wrap">
                                            <input type="text" name="work_time" value="{{ old('work_time', $employee->work_time) }}" class="font-gray" placeholder="Введите время работы">
                                            <span class="invalid-feedback">{{ $errors->first('work_time') }}</span>
                                        </label>
                                    </td>
                                </tr>
                                <tr class="preview-info__row">
                                    <td class="preview-info__col pr32">
                                        <label class="feedback-wrap">
                                            <input type="text" name="address" value="{{ old('address', $employee->address) }}" class="font-gray" placeholder="Введите адрес">
                                            <span class="invalid-feedback">{{ $errors->first('address') }}</span>
                                        </label>
                                    </td>
                                    <td class="preview-info__col">
                                        <label class="feedback-wrap">
                                            <input type="text" name="price" value="{{ old('price', $employee->price) }}" class="font-gray" placeholder="Введите цену">
                                            <span class="invalid-feedback">{{ $errors->first('price') }}</span>
                                        </label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">О сотруднике</span>
                    </div>
                    <div class="content-sect__inner">
                        <label class="feedback-wrap">
                            <textarea type="text" name="description" class="textarea" placeholder="Введите информацию о мастере" rows="4">{{ old('description', $employee->description)}}</textarea>
                            <span class="invalid-feedback">{{ $errors->first('description') }}</span>
                        </label>
                    </div>
                </div>
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">Работы сотрудника</span>
                    </div>
                    <div class="content-sect__inner">
                        <div class="image-uploader">
                            @foreach($employee->images as $image)
                                <div class="image-uploader__item">
                                    <img src="{{ $image->getThumb('list') }}" alt="" class="image-uploader__image">
                                    <input type="hidden" name="image_ids[]" value="{{ (int)$image->id }}">
                                    <a href="#" class="image-remove"></a>
                                </div>
                            @endforeach
                            <div class="image-uploader__item">
                                <div class="image-uploader__new-image">
                                    <div class="image-uploader__info">
                                        <span class="image-uploader__icon" style="background-image: url({{ asset('img/plus.svg') }});"></span><br>
                                        <span class="image-uploader__info-text">Добавить</span>
                                    </div>
                                    <input class="dn upload-input" type="file">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(! $employee->services->isEmpty())
                        <div class="content-sect">
                            <div class="content-sect__title-wrap">
                                <span class="content-sect__title">Позиции</span>
                            </div>
                            <table class="table">
                                <tr class="table-header">
                                    <th class="table-header__item">Название услуги</th>
                                    <th class="table-header__item">Минимальная цена, руб.</th>
                                    <th class="table-header__item table-header__item--control"></th>
                                </tr>
                                @foreach($employee->services as $service)
                                    <tr class="table-body">
                                        <td class="table-body__item tac">{{ $service->name }}</td>
                                        <td class="table-body__item tac">{{ $service->price }}</td>
                                        <td class="table-body__item table-body__item--control tac">
                                    <div class="control-links">
                                        <a href="{{ route('admin.service.edit', $service) }}" class="control-links__item" style="background-image:url({{ asset('img/edit.svg') }});"></a>
                                        <div class="trash-form ajax-form" data-action="{{ route('admin.service.detach.user', [$service, $employee]) }}" data-method="POST" >
                                            <button class="control-links__item trash-js" data-toggle="modal" data-target="#delete" style="background-image:url( {{ asset('img/trash.svg') }});"></button>
                                            <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered maw400" role="document">
                                                    <div class="modal-content">
                                                        <button type="button" class="map-container__close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <div class="modal-body ">
                                                            <div class="modal-inner tac">
                                                                Вы действительно хотите удалить данную запись?
                                                            </div>
                                                            <div class="modal-trash-control">
                                                                <button class="button button--border-black trash-modal-ok-js">Да</button>
                                                                <button class="button button--black trash-modal-cancel-js">Отмена</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    @endif
            </div>
        </main>
    </form>

@endsection
