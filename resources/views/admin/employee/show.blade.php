@extends('layouts.main')
@section('content')
    <header class="header">
        <div class="back-wrap">
            <a href="{{ route('admin.employee.index') }}" class="back-link"></a>
            <h1 class="page-title">Карточка сотрудника</h1>
        </div>
        <div class="top-buttons">
            <button class="button button--border-black">Заблокировать</button>
        </div>
    </header>
    <main class="main">
            <div class="publish-single">
            <div class="preview-sect">
                <div class="preview">
                    <div class="image-preview-wrap image-preview-wrap--big">
                        <img src="{{ $employee->user->getThumb('preview') }}" alt="" class="image-preview">
                    </div>
                    <div class="preview-info-with-title">
                        <span class="preview-info__title">{{ $employee->user->fullName() }}</span><span class="rating ml22" data-score="{{ $employee->rating }}"></span>
                        <table class="preview-info">
                            <tr class="preview-info__row">
                                <td class="preview-info__col pr32">{{ $employee->user->staff->name }}</td>
                                <td class="preview-info__col">@if($employee->work_time) Время работы: {{ $employee->work_time }} @endif</td>
                            </tr>
                            <tr class="preview-info__row">
                                <td class="preview-info__col pr32">@if($employee->address) Адрес: {{ $employee->address }} @endif</td>
                                <td class="preview-info__col">@if($employee->price) Стоимость услуг: {{ $employee->price }} руб. @endif</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="preview-btn-wrap">
                    <a href="{{ route('admin.employee.edit', $employee) }}" class="button button--red">Редактировать</a>
                </div>
            </div>
            @if($employee->description)
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">О сотруднике</span>
                    </div>
                    <div class="content-sect__inner">
                        <p class="text">{{ $employee->description }}</p>
                    </div>
                </div>
            @endif
            @if(! $employee->images->isEmpty())
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">Работы сотрудника</span>
                    </div>
                    <div class="content-sect__inner">
                        <div class="image-uploader">
                            @foreach($employee->images as $image)
                                <div class="image-uploader__item">
                                    <img src="{{ $image->getThumb('list') }}" alt="" class="image-uploader__image">
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            @if(! $employee->services->isEmpty())
                    <div class="content-sect">
                        <div class="content-sect__title-wrap">
                            <span class="content-sect__title">Позиции</span>
                        </div>
                        <table class="table">
                            <tr class="table-header">
                                <th class="table-header__item">Название услуги</th>
                                <th class="table-header__item">Минимальная цена, руб.</th>
                                <th class="table-header__item table-header__item--control"></th>
                            </tr>
                            @foreach($employee->services as $service)
                            <tr class="table-body">
                                <td class="table-body__item tac">{{ $service->name }}</td>
                                <td class="table-body__item tac">{{ $service->price }}</td>
                                <td class="table-body__item table-body__item--control tac">
                                    <div class="control-links">
                                        <a href="{{ route('admin.service.edit', $service) }}" class="control-links__item" style="background-image:url({{ asset('img/edit.svg') }});"></a>
                                        <form class="trash-form" action="{{ route('admin.service.detach.user', [$service, $employee]) }}" method="POST" >
                                            @csrf
                                            @method('DELETE')
                                            <button class="control-links__item trash-js" data-toggle="modal" data-target="#delete" style="background-image:url( {{ asset('img/trash.svg') }});"></button>
                                            <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered maw400" role="document">
                                                    <div class="modal-content">
                                                        <button type="button" class="map-container__close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <div class="modal-body ">
                                                            <div class="modal-inner tac">
                                                                Вы действительно хотите удалить данную запись?
                                                            </div>
                                                            <div class="modal-trash-control">
                                                                <button class="button button--border-black trash-modal-ok-js">Да</button>
                                                                <button class="button button--black trash-modal-cancel-js">Отмена</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
            @endif

        </div>
</main>
@endsection
