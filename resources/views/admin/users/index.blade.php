@extends('layouts.main')
@section('content')
    <header class="header" xmlns="http://www.w3.org/1999/html">
        <div class="back-wrap">
            <h1 class="page-title">Пользователи</h1>
        </div>
        <div class="top-buttons">
            <a href="{{ route('admin.users.create') }}" class="button button--black">Добавить</a>
        </div>
    </header>
    <main class="main">
        <div class="users-list">
            <div class="top-control">
                <form action="?" method="GET" class="search-form">
                    <label class="search-form__label">
                        <input type="text" class="search-form__input" name="search" value="{{ request('search') }}" placeholder="Введите должность">
                    </label>
                    <button class="search-form__submit">Найти</button>
                </form>
            </div>
            <div class="list">
                <div class="list__content">
                    <div class="row">
                        @foreach ($users as $user)
                            <div class="col-6">
                                    <div class="list-item">
                                        <div class="list-item__img list-item__img--big">
                                            <img src="{{ $user->getThumb('grid') }}" class="image" alt="">
                                        </div>
                                        <div class="list-item__content">
                                            <div class="list-item__row mb10">
                                                <span class="list-item__title">{{ $user->fullName() }}</span>
                                            </div>
                                            <div class="list-item__row">
                                                <span class="list-item__row-text"><span class="bold">Должность: </span> <br>{{ $user->staff ? $user->staff->name : '' }}</span>
                                            </div>
                                        </div>
                                        <div class="list-item__control">
                                    <div class="control-links">
                                        <a href="{{ route('admin.users.edit', $user) }}" class="control-links__item" style="background-image:url({{ asset('img/edit.svg') }});"></a>
                                        @if($user->id !== Auth::user()->id)
                                        <form class="trash-form" action="{{ route('admin.users.destroy', $user) }}" method="POST" >
                                            @csrf
                                            @method('DELETE')
                                            <button class="control-links__item trash-js" data-toggle="modal" data-target="#delete" style="background-image:url( {{ asset('img/trash.svg') }});"></button>
                                            <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered maw400" role="document">
                                                    <div class="modal-content">
                                                        <button type="button" class="map-container__close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <div class="modal-body ">
                                                            <div class="modal-inner tac">
                                                                Вы действительно хотите удалить данную запись?
                                                            </div>
                                                            <div class="modal-trash-control">
                                                                <button class="button button--border-black trash-modal-ok-js">Да</button>
                                                                <button class="button button--black trash-modal-cancel-js">Отмена</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        @endif
                                    </div>
                                        </div>
                                    </div>
                                </div>
                        @endforeach
                    </div>
                </div>

                <div class="list__bottom">
                    <nav class="pagination-wrap">
                        {{ $users->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </main>
@endsection
