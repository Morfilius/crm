@extends('layouts.main')
@section('content')
    <form method=POST enctype='multipart/form-data' action="{{ route('admin.users.store') }}">
        @csrf
        <header class="header">
            <div class="back-wrap">
                <a href="{{ route('admin.users.index') }}" class="back-link"></a>
                <h1 class="page-title">Карточка пользователя</h1>
            </div>
            <div class="top-buttons">
                <button class="button button--red">Сохранить</button>
            </div>
        </header>
        <main class="main">
            <div class="user-single">
                <div class="preview aifs">
                    <div class="image-preview-wrap image-preview-wrap--big image-select">
                        <img src="" alt="" class="image-preview">
                        <div class="new-image"></div>
                        <input type="file" name="photo" class="upload-single-input">
                        <a href="#" class="image-remove"></a>
                    </div>
                    <div class="user-single-form">
                        <div class="row mb30">
                            <div class="col-4">
                                <label class="two-spike-input">
                                    <span class="two-spike-input__title">Имя</span><br>
                                    <input type="text" name="first_name" class="two-spike-input__input input" value="{{ old('first_name') }}" placeholder="Введите имя пользователя">
                                    <span class="invalid-feedback">{{ $errors->first('first_name') }}</span>
                                </label>
                            </div>
                            <div class="col-4">
                                <label class="two-spike-input">
                                    <span class="two-spike-input__title">Фамилия</span><br>
                                    <input type="text" name="last_name" class="two-spike-input__input input" value="{{ old('last_name')  }}" placeholder="Введите фамилию пользователя">
                                    <span class="invalid-feedback">{{ $errors->first('last_name') }}</span>
                                </label>
                            </div>
                            <div class="col-4">
                                <label class="two-spike-input">
                                    <span class="two-spike-input__title">Телефон</span><br>
                                    <input type="text" name="phone" class="two-spike-input__input input phone"  value="{{ old('phone')  }}" placeholder="+7 (___) ___-__-__">
                                    <span class="invalid-feedback">{{ $errors->first('phone') }}</span>
                                </label>
                            </div>
                        </div>
                        <div class="row mb30">
                            <div class="col-4">
                                <label class="two-spike-input">
                                    <span class="two-spike-input__title">Должность</span><br>
                                    <select type="text" name="staff" class="two-spike-input__input select" data-placeholder="Выберите должность">
                                        <option></option>
                                        @foreach (App\Entity\Staff::getList() as $staff)
                                            <option value="{{ $staff->name }}" @if (old('role') == $staff->name) {{ 'selected' }} @endif>{{ $staff->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="invalid-feedback">{{ $errors->first('role') }}</span>
                                </label>
                            </div>
                            <div class="col-4">
                                <label class="two-spike-input">
                                    <span class="two-spike-input__title">Email</span><br>
                                    <input type="text" name="email" class="two-spike-input__input input"  value="{{ old('email')  }}" placeholder="Введите Email">
                                    <span class="invalid-feedback">{{ $errors->first('email') }}</span>
                                </label>
                            </div>
                            <div class="col-4">
                                <label class="two-spike-input">
                                    <span class="two-spike-input__title">Пароль</span><br>
                                    <input type="password" name="password" placeholder="***********"  value="{{ old('password')  }}" class="two-spike-input__input input">
                                    <span class="invalid-feedback">{{ $errors->first('password') }}</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </form>
@endsection
