@extends('layouts.main')
@section('content')
    <header class="header">
        <h1 class="page-title">Заказы</h1>
        <a href="{{ route('admin.order.create') }}" class="button button--black" style="margin-left: 20px; margin-right: auto">Добавить заказ</a>
        <div class="top-menu">
            <a href="{{ route('admin.order.list.calendar') }}" class="top-menu__item">Календарь</a>
            <span class="top-menu__item active">Список</span>
            <a href="{{ route('admin.order.list.employees') }}" class="top-menu__item">По сотрудникам</a>
        </div>
    </header>
    <main class="main">
        <div class="orders-list">
            <form action="{{ url()->current() }}" class="order-top">
                <div class="order-top__inner">
                    <form action="?" method="GET">
                        <div class="input-row">
                            <div class="date-created">
                                <span class="date-created__title">Дата создания c </span>
                                <label class="input-arrow">
                                    <input type="text" placeholder="дд.мм.гггг" class="date-created__input date">
                                </label>
                                по
                                <div class="input-arrow">
                                    <input type="text" placeholder="дд.мм.гггг" class="date-created__input date">
                                </div>
                            </div>
                        </div>
                        <div class="input-row">
                            <div class="mr90">
                                <select name="employee" data-placeholder="Выберите сотрудника" class="select w320" id="">
                                    <option></option>
                                    <option value="0">Любой</option>
                                    @foreach($salon->employees as $employee)
                                        <option value="{{ $employee->id }}" @if(request('employee') == $employee->id) selected @endif>{{ $employee->user->fullName() }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mr90">
                                <select name="client" data-placeholder="Выберите пользователя" class="select w320" id="">
                                    <option></option>
                                    <option value="0">Любой</option>
                                    @foreach($salon->clients as $client)
                                        <option value="{{ $client->id }}" @if(request('client') == $client->id) selected @endif>{{ $client->user->fullName() }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div>
                                <select name="service" data-placeholder="Выберите услугу" class="select w320" id="">
                                    <option></option>
                                    <option value="0">Любой</option>
                                    @foreach($salon->services as $service)
                                        <option value="{{ $service->id }}" @if(request('service') == $service->id) selected @endif>{{ $service->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="input-row">
                            <input type="text" name="phone" value="{{ request('phone') }}" placeholder="Введите имя или телефон клиента" class="input">
                        </div>
                        <div class="input-row">
                            <div class="mr50">
                                <select name="time" data-placeholder="Выберите время визита" class="select w250">
                                    <option></option>
                                    <option value="0">Любой</option>
                                    @foreach(\App\Entity\Service\Service::sessionStartList() as $session_start)
                                        <option @if(time_to_integer($session_start) == request('time')) selected @endif value="{{ time_to_integer($session_start) }}">{{ $session_start }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mr50">
                                <select name="source" data-placeholder="Выберите источник" class="select w250">
                                    <option></option>
                                    <option value="0">Любой</option>
                                    @foreach(App\Entity\Order\Order::sourcesList() as $key => $source)
                                        <option value="{{ $key }}" @if(request('source') == $key) selected @endif>{{ $source }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mr50">
                                <select name="status" data-placeholder="Выберите статус" class="select  w250">
                                    <option></option>
                                    <option value="0">Любой</option>
                                    @foreach(App\Entity\Order\Order::statusesList() as $key => $status)
                                        <option value="{{ $key }}" @if(request('status') == $key) selected @endif>{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="button button--black mla">Применить</button>
                        </div>
                    </form>
                </div>
            </form>
            @if($orders->isNotEmpty())
            <table class="table">
                <tr class="table-header">
                    <th class="table-header__item">Мастер</th>
                    <th class="table-header__item">Услуги</th>
                    <th class="table-header__item">Клиент</th>
                    <th class="table-header__item">Время визита</th>
                    <th class="table-header__item">Создано</th>
                    <th class="table-header__item">Статус визита</th>
                    <th class="table-header__item">Источник</th>
                    <th class="table-header__item table-header__item--control"></th>
                </tr>
                @foreach($orders as $order)
                <tr class="table-body">
                    <td class="table-body__item">
                            <span class="order-list-img-wrap">
                                <img src="{{ $order->employee->user->getThumb('preview') }}" class="order-list-img" alt="">
                            </span>
                        {{ $order->employee->user->fullName() }}
                    </td>
                    <td class="table-body__item">
                        @if($order->services->isNotEmpty())
                            {{ implode(', ', $order->services->map(function ($service) { return $service->service->name; })->toArray()) }}
                        @endif
                    </td>
                    <td class="table-body__item tac">{{ $order->client->user->fullName() }}<br>{{ $order->client->user->phone }}</td>
                    <td class="table-body__item tac">{{ $order->date->locale('ru')->shortDayName }}, 19-00</td>
                    <td class="table-body__item tac">{{ $order->created_at->translatedFormat('d F Y в m:i') }}</td>
                    <td class="table-body__item tac">{{ App\Entity\Order\Order::getStatus($order->status) }} </td>
                    <td class="table-body__item tac">{{ App\Entity\Order\Order::getSource($order->source) }}</td>
                    <td class="table-body__item table-body__item--control tac">
                        <div class="control-links">
                            <a href="{{ route('admin.order.edit', $order) }}" class="control-links__item" style="background-image:url({{ asset('img/edit.svg') }});"></a>
                            <form class="trash-form" action="{{ route('admin.order.destroy', $order)  }}" method="POST" >
                                @csrf
                                @method('DELETE')
                                <button class="control-links__item trash-js" data-toggle="modal" data-target="#delete" style="background-image:url( {{ asset('img/trash.svg') }});"></button>
                                <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered maw400" role="document">
                                        <div class="modal-content">
                                            <button type="button" class="map-container__close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <div class="modal-body ">
                                                <div class="modal-inner tac">
                                                    Вы действительно хотите удалить данную запись?
                                                </div>
                                                <div class="modal-trash-control">
                                                    <button class="button button--border-black trash-modal-ok-js">Да</button>
                                                    <button class="button button--black trash-modal-cancel-js">Отмена</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
            </table>
            @endif
        </div>
    </main>
@endsection
