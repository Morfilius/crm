@extends('layouts.main')
@section('content')
    <header class="header">
        <h1 class="page-title">Заказы</h1>
        <a href="{{ route('admin.order.create') }}" class="button button--black" style="margin-left: 20px; margin-right: auto">Добавить заказ</a>
        <div class="top-menu">
            <span class="top-menu__item active">Календарь</span>
            <a href="{{ route('admin.order.list.list') }}" class="top-menu__item">Список</a>
            <a href="{{ route('admin.order.list.employees') }}" class="top-menu__item">По сотрудникам</a>
        </div>
    </header>
    <main class="main">
        <div class="calendar-orders">
            <div class="calendars-wrap">
                <div class="calendars">
                    @foreach($calendars as $calendar)
                        <div>
                            <?php
                                $checked = collect($calendar['orders'])->map(function ($item) { return ['date' => $item->date->format('Y-m-d')]; })->toJson();
                                $links = collect($calendar['orders'])->reduce(function ($result, $item) {
                                    $result[$item->date->format('Y-m-d')] = url_to(['date' => $item->date->format('Y-m-d')]);
                                    return $result;
                                }, []);
                            ?>
                            <div class="calendar order-calendar-js" data-links='<?= json_encode($links) ?>' data-checked='<?= $checked ?>' data-date='{"year":{{$calendar['year']}},"month":{{$calendar['month']}}}'></div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="order-calendar-employee dragscroll">
                <table class="calendar-employee">
                    <tr class="calendar-employee__header">
                        <th class="calendar-employee__th calendar-employee__hours-col"></th>
                        @foreach($salon->employees as $employee)
                        <th class="calendar-employee__th calendar-employee__employee-col">
                            <div class="calendar-employee__th-inner">
                                <div class="employee-table-preview">
                                    <img src="{{ $employee->user->getThumb('preview') }}" alt="" class="employee-table-preview__img"><br>
                                    <span class="employee-table-preview__title">{{ $employee->user->fullName() }}</span>
                                </div>
                            </div>
                        </th>
                        @endforeach
                    </tr>
                    @for($i = 7; $i < 24; $i++)
                        <?php $ordersByEmployeeId = get_elem_by_time($ordersByTimeAndEmployee, $i.':00') ?>
                    <tr class="calendar-employee__row">
                        <td class="calendar-employee__time">{{ $i }}:00</td>
                        @foreach($salon->employees as $employee)
                        <td class="calendar-employee__td">
                            @if(isset($ordersByEmployeeId[$employee->id]))
                                <?php
                                    $order = $ordersByEmployeeId[$employee->id];
                                    $start = time_to_part($order->session_start);
                                    $end = time_to_part($order->session_end);
                                ?>
                                <div class="calendar-event" data-from='{"hour":{{ $start['hour'] }}, "min":{{ (int)$start['min'] }}}' data-to='{"hour":{{ $end['hour'] }}, "min":{{ (int)$end['min'] }}}'>
                                    <header class="calendar-event__header">{{ $i }}:00  Новый сеанс</header>
                                    <p class="calendar-event__body"></p>
                                </div>
                            @endif
                        </td>
                        @endforeach
                    </tr>
                    @endfor
                </table>
            </div>
        </div>
    </main>
@endsection
