<?php
use App\Entity\Service\Service;
?>
@extends('layouts.main')
@section('content')
    <form method=POST enctype='multipart/form-data' class="scroll-content" action="{{ route('admin.order.update', $order) }}">
        @csrf
        @method('PUT')
        <header class="header">
            <div class="back-wrap">
                <a href="{{ route('admin.order.list.list') }}" class="back-link"></a>
                <h1 class="page-title">Карточка заказа</h1>
            </div>
            <div class="top-buttons">
                <button class="button button--red">Cохранить</button>
            </div>
        </header>
        <main class="main">
            <div class="order-single">
                <div class="preview-sect">
                    <div class="preview">
                        <table class="common-info">
                            <tr class="common-info__row">
                                <td class="common-info__col pr32"><span class="bold fz16">Клиент</span></td>
                                <td class="common-info__col">
                                    <select class="select w200 select--no-border" name="client" data-placeholder="Выберите клиента" >
                                        <option></option>
                                        @foreach($salon->clients as $client)
                                            <option value="{{ $client->id }}" @if(old('client', $order->client->id) == $client->id) selected @endif>{{ $client->user->fullName() }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr class="common-info__row">
                                <td class="common-info__col pr32"><span class="bold fz16">Сотрудник</span></td>
                                <td class="common-info__col">
                                    <select class="select w200 select--no-border" name="employee" data-placeholder="Выберите сотрудника" >
                                        <option></option>
                                        @foreach($salon->employees as $employee)
                                            <option value="{{ $employee->id }}" @if(old('employee', $order->employee->id) == $employee->id) selected @endif>{{ $employee->user->fullName() }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr class="common-info__row">
                                <td class="common-info__col pr32"><span class="bold fz16">Важность</span></td>
                                <td class="common-info__col">
                                    <select class="select w200 select--no-border" name="priority" data-placeholder="Выберите важность" >
                                        <option></option>
                                        @foreach(App\Entity\Order\Order::priorityList() as $key => $priority)
                                            <option value="{{ $key }}" @if(old('priority', $order->priority) == $key) selected @endif>{{ $priority }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr class="common-info__row">
                                <td class="common-info__col pr32"><span class="bold fz16">Источник</span></td>
                                <td class="common-info__col">
                                    <select class="select w200 select--no-border" name="source" data-placeholder="Выберите источник" >
                                        <option></option>
                                        @foreach(App\Entity\Order\Order::sourcesList() as $key => $source)
                                            <option value="{{ $key }}" @if(old('source', $order->source) == $key) selected @endif>{{ $source }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">Запись</span>
                    </div>
                    <div class="content-sect__inner">
                        <table class="common-info">
                            <tr class="common-info__row">
                                <td class="common-info__col"><span class="bold fz16">Дата заказа</span></td>
                                <td class="common-info__col">
                                    <label class="input-arrow">
                                        <input type="text" value="{{ old('date', $order->date->format('d.m.Y')) }}" name="date" style="margin-left: 0" placeholder="дд.мм.гггг" class="date-created__input date">
                                    </label>
                                </td>
                            </tr>
                            <tr class="common-info__row">
                                <td class="common-info__col"><span class="bold fz16">Время начала заказа</span></td>
                                <td class="common-info__col">
                                    <span class="select2-border">
                                        <select name="session_start" data-placeholder="--" class="select w90">
                                            <option></option>
                                            @foreach(\App\Entity\Service\Service::sessionStartList() as $session_start)
                                                <option @if(time_to_integer($session_start) == old('session_start', $order->session_start)) selected @endif value="{{ time_to_integer($session_start) }}">{{ $session_start }}</option>
                                            @endforeach
                                        </select>
                                    </span>
                                </td>
                            </tr>
                            <tr class="common-info__row">
                                <td class="common-info__col"><span class="bold fz16">Время окончания заказа</span></td>
                                <td class="common-info__col">
                                    <span class="select2-border">
                                        <select name="session_end" data-placeholder="--" class="select w90">
                                            <option></option>
                                            @foreach(\App\Entity\Service\Service::sessionEndList() as $session_end)
                                                <option @if(time_to_integer($session_end) == old('session_end', $order->session_end)) selected @endif value="{{ time_to_integer($session_end) }}">{{ $session_end }}</option>
                                            @endforeach
                                        </select>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">Статус заказа</span>
                    </div>
                    <div class="content-sect__inner">
                        <div class="checkbox-row">
                            @foreach(App\Entity\Order\Order::statusesList() as $key => $status)
                            <label class="checkbox">
                                <input type="radio" name="status" @if($key == old('status', $order->status)) checked @endif value="{{ $key }}">
                                <span class="checkbox-mark"></span>{{ $status }}
                            </label>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">Услуги</span>
                    </div>
                    <table class="table" data-service='<?= $salon->services->map(function (Service $service) {
                        return [
                            'id' => $service->id,
                            'name' => $service->name,
                        ];
                    })->toJson() ?>' id="orders">
                        <tr class="table-header">
                            <th class="table-header__item">Позиции в заказе</th>
                            <th class="table-header__item">Мастер</th>
                            <th class="table-header__item">Количество</th>
                            <th class="table-header__item">Цена, руб.</th>
                            <th class="table-header__item">Скидка, %</th>
                            <th class="table-header__item">Итого, руб.</th>
                            <th class="table-header__item table-header__item--control"></th>
                        </tr>
                        @foreach($order->services as $orderService)
                        <tr class="table-body">
                            <td class="table-body__item">
                                <select class="select" name="service[name][]" data-select2-id="70" tabindex="-1" aria-hidden="true">
                                    @foreach($salon->services as $service)
                                    <option value="{{ $service->id }}" @if( $orderService->service->id == $service->id) selected @endif> {{ $service->name }} </option>
                                    @endforeach
                                </select>
                            <td class="table-body__item tac">{{ $order->employee->user->fullName() }}</td>
                            <td class="table-body__item tac">
                                <label class="number"><label class="number"><input type="number" value="{{ $orderService->qty }}" name="service[qty][]" class="input-border tac maw60"></label></label></td>
                            <td class="table-body__item tac"><input type="text" name="service[price][]" class="input-border maw80 tac" value="{{ $orderService->price }}"></td>
                            <td class="table-body__item tac"><input type="text" name="service[discount][]" class="input-border maw80 tac" value="{{ $orderService->discount }}"></td>
                            <td class="table-body__item tac">
                                {{ $orderService->getCost() }}
                            </td>
                            <td class="table-body__item table-body__item--control tac">
                                <div class="control-links">
                                    <a href="#" class="control-links__item row-delete-js" style="background-image:url({{ asset('img/trash.svg') }});"></a>
                                </div>
                            </td>

                        </tr>
                        @endforeach
                    </table>
                    <div class="order-create" id="order-create">
                        <button class="button button--black">Добавить услугу</button>
                    </div>
                </div>
            </div>
        </main>
    </form>
@endsection
