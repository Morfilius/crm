<?php
use Illuminate\Support\Carbon;
?>
@extends('layouts.main')
@section('content')
    <header class="header">
        <h1 class="page-title">Заказы</h1>
        <a href="{{ route('admin.order.create') }}" class="button button--black" style="margin-left: 20px; margin-right: auto">Добавить заказ</a>
        <div class="top-menu">
            <a href="{{ route('admin.order.list.calendar') }}" class="top-menu__item">Календарь</a>
            <a href="{{ route('admin.order.list.list') }}" class="top-menu__item">Список</a>
            <span class="top-menu__item active">По сотрудникам</span>
        </div>
    </header>
    <main class="main">
        <div class="order-employee">
            <div class="order-top">
                <div class="order-top__inner">
                    <div class="order-employee-top">
                        <div class="order-employee-calendar">
                            <div class="calendar calendar-orders-employee-js" data-link='<?= url_to(['from' => '_from', 'to' => '_to'])?>'></div>
                        </div>
                        <div class="order-employee-slider-wrap">
                            <span class="order-employee-slider__title">Сотрудники</span>
                            <script>
                                //order-employee-slider-js - если 5 и более элементов
                                //order-employee--no-slider - если 5 и более элементов
                            </script>
                            <div class="order-employee-slider order-employee-slider-js">
                                @foreach($salon->employees->chunk(3) as $employeeRow)
                                <div>
                                    <div class="order-employee-item">
                                        <ul class="order-employee-slider-list">
                                            @foreach($employeeRow as $employee)
                                            <li class="order-employee-slider-list__item">
                                                <a href="{{ url_to(['employee' => $employee->id]) }}" class="order-employee-slider-list__link">
                                                         <span class="order-list-img-wrap">
                                                                <img src="{{ $employee->user->getThumb('preview') }}" class="order-list-img" alt="">
                                                        </span>
                                                    <span class="order-employee-slider-list__item-text">{{ $employee->user->fullName() }}</span>
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(request('from') && request('to'))
            <table class="table">
                <?php
                    $from = new Carbon(request('from'));
                    $to = new Carbon(request('to'));
                    $diff = $from->diffInDays($to) + 1
                ?>
                <tr class="table-header">
                    <th class="table-header__item w100">Время</th>
                    @for ($i = 0; $i < $diff; ++$i)
                        <?php $date = $from->copy()->add($i, 'day')?>
                        <th class="table-header__item">{{ $date->locale('ru')->dayName }}<br>{{ $date->translatedFormat('j F') }}</th>
                    @endfor
                </tr>
                @for($i = 7; $i < 24; $i++)
                <tr class="calendar-employee__row">
                    <td class="calendar-employee__time">{{ $i }}:00</td>
{{--                    <td class="calendar-employee__td">--}}
{{--                        <div class="calendar-event" data-from='{"hour":8, "min":0}' data-to='{"hour":10, "min":0}'>--}}
{{--                            <header class="calendar-event__header">8:00  Новый сеанс</header>--}}
{{--                            <p class="calendar-event__body">Заказ на 3 часа: окраска и стрижка женская</p>--}}
{{--                        </div>--}}
{{--                    </td>--}}
                    @for ($v = 0; $v < $diff; ++$v)
                        <?php $dateRow = $from->copy()->add($v, 'day')?>
                            <td class="calendar-employee__td"></td>
                    @endfor
                </tr>
                @endfor
            </table>
            @endif
        </div>
    </main>
@endsection
