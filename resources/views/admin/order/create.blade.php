<?php
use App\Entity\Service\Service;
?>
@extends('layouts.main')
@section('content')
    <form method=POST enctype='multipart/form-data' class="scroll-content" action="{{ route('admin.order.store') }}">
        @csrf
        <header class="header">
            <div class="back-wrap">
                <a href="{{ route('admin.order.list.calendar') }}" class="back-link"></a>
                <h1 class="page-title">Карточка заказа</h1>
            </div>
            <div class="top-buttons">
                <button class="button button--red">Cохранить</button>
            </div>
        </header>
        <main class="main">
            <div class="order-single">
                <div class="preview-sect">
                    <div class="preview">
                        <table class="common-info">
                            <tr class="common-info__row">
                                <td class="common-info__col pr32"><span class="bold fz16">Клиент</span></td>
                                <td class="common-info__col">
                                    <label class="feedback-wrap">
                                        <select class="select w200 select--no-border" name="client" data-placeholder="Выберите клиента" >
                                            <option></option>
                                            @foreach($salon->clients as $client)
                                                <option value="{{ $client->id }}" @if(old('client') == $client->id) selected @endif>{{ $client->user->fullName() }}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback">{{ $errors->first('client') }}</span>
                                    </label>
                                </td>
                            </tr>
                            <tr class="common-info__row">
                                <td class="common-info__col pr32"><span class="bold fz16">Сотрудник</span></td>
                                <td class="common-info__col">
                                    <label class="feedback-wrap">
                                        <select class="select w200 select--no-border" name="employee" data-placeholder="Выберите сотрудника" >
                                            <option></option>
                                            @foreach($salon->employees as $employee)
                                            <option value="{{ $employee->id }}" @if(old('employee') == $employee->id) selected @endif>{{ $employee->user->fullName() }}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback">{{ $errors->first('employee') }}</span>
                                    </label>
                                </td>
                            </tr>
                            <tr class="common-info__row">
                                <td class="common-info__col pr32"><span class="bold fz16">Важность</span></td>
                                <td class="common-info__col">
                                    <label class="feedback-wrap">
                                        <select class="select w200 select--no-border" name="priority" data-placeholder="Выберите важность" >
                                            <option></option>
                                            @foreach(App\Entity\Order\Order::priorityList() as $key => $priority)
                                                <option value="{{ $key }}" @if(old('priority') == $key) selected @endif>{{ $priority }}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback">{{ $errors->first('priority') }}</span>
                                    </label>
                                </td>
                            </tr>
                            <tr class="common-info__row">
                                <td class="common-info__col pr32"><span class="bold fz16">Источник</span></td>
                                <td class="common-info__col">
                                    <label class="feedback-wrap">
                                        <select class="select w200 select--no-border" name="source" data-placeholder="Выберите источник" >
                                            <option></option>
                                            @foreach(App\Entity\Order\Order::sourcesList() as $key => $source)
                                                <option value="{{ $key }}" @if(old('source') == $key) selected @endif>{{ $source }}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback">{{ $errors->first('source') }}</span>
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">Запись</span>
                    </div>
                    <div class="content-sect__inner">
                        <table class="common-info">
                            <tr class="common-info__row">
                                <td class="common-info__col"><span class="bold fz16">Дата заказа</span></td>
                                <td class="common-info__col">
                                    <label class="input-arrow feedback-wrap">
                                        <input type="text" value="{{ old('date') }}" name="date" style="margin-left: 0" placeholder="дд.мм.гггг" class="date-created__input date">
                                        <span class="invalid-feedback nw">{{ $errors->first('date') }}</span>
                                    </label>
                                </td>
                            </tr>
                            <tr class="common-info__row">
                                <td class="common-info__col"><span class="bold fz16">Время начала заказа</span></td>
                                <td class="common-info__col">
                                    <span class="select2-border">
                                        <label class="feedback-wrap">
                                            <select name="session_start" data-placeholder="--" class="select w90">
                                                <option></option>
                                                    @foreach(\App\Entity\Service\Service::sessionStartList() as $session_start)
                                                    <option @if(time_to_integer($session_start) == old('session_start')) selected @endif value="{{ time_to_integer($session_start) }}">{{ $session_start }}</option>
                                                @endforeach
                                            </select>
                                            <span class="invalid-feedback nw">{{ $errors->first('session_start') }}</span>
                                         </label>
                                    </span>
                                </td>
                            </tr>
                            <tr class="common-info__row">
                                <td class="common-info__col"><span class="bold fz16">Время окончания заказа</span></td>
                                <td class="common-info__col">
                                    <span class="select2-border">
                                        <label class="feedback-wrap">
                                            <select name="session_end" data-placeholder="--" class="select w90">
                                                <option></option>
                                                @foreach(\App\Entity\Service\Service::sessionEndList() as $session_end)
                                                    <option @if(time_to_integer($session_end) == old('session_end')) selected @endif value="{{ time_to_integer($session_end) }}">{{ $session_end }}</option>
                                                @endforeach
                                            </select>
                                            <span class="invalid-feedback nw">{{ $errors->first('session_end') }}</span>
                                        </label>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">Статус заказа</span>
                    </div>
                    <div class="content-sect__inner">
                        <div class="checkbox-row">
                            <span class="feedback-wrap df">
                            @foreach(App\Entity\Order\Order::statusesList() as $key => $status)
                                <label class="checkbox">
                                    <input type="radio" name="status"  @if($key == old('status')) checked @endif value="{{ $key }}">
                                    <span class="checkbox-mark"></span>{{ $status }}
                                </label>
                            @endforeach
                                <span class="invalid-feedback">{{ $errors->first('status') }}</span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">Услуги</span>
                    </div>
                    <table class="table" data-service='<?= $salon->services->map(function (Service $service) {
                        return [
                            'id' => $service->id,
                            'name' => $service->name,
                        ];
                    })->toJson() ?>' id="orders">
                        <tr class="table-header">
                            <th class="table-header__item">Позиции в заказе</th>
                            <th class="table-header__item">Мастер</th>
                            <th class="table-header__item">Количество</th>
                            <th class="table-header__item">Цена, руб.</th>
                            <th class="table-header__item">Скидка, %</th>
                            <th class="table-header__item">Итого, руб.</th>
                            <th class="table-header__item table-header__item--control"></th>
                        </tr>
                    </table>
                    <div class="order-create" id="order-create">
                        <button class="button button--black">Добавить услугу</button>
                    </div>
                </div>
            </div>
        </main>
    </form>
@endsection
