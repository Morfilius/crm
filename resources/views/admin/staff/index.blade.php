@extends('layouts.main')
@section('content')
    <header class="header">
        <div class="back-wrap">
            <h1 class="page-title">Должности</h1>
        </div>
        <div class="top-buttons">
            <a href="{{ route('admin.staff.create') }}" class="button button--black">Добавить</a>
        </div>
    </header>
    <main class="main">
        <div class="publish-list">
            <div class="top-control">
                <form action="/" class="search-form">
                    <label class="search-form__label">
                        <input type="text" class="search-form__input" placeholder="Введите должность">
                    </label>
                    <button class="search-form__submit">Найти</button>
                </form>
            </div>
            <div class="list">
                <div class="list__content">
                    @foreach($staffs as $staff)
                        <div class="list-item">
                            <div class="list-item__content staff-content">
                                <div>
                                    <span class="list-item__row-text"><span class="bold">Название: </span>{{ $staff->name }}</span>
                                    <span class="list-item__row-text ml100"><span class="bold">Описание: </span>{{ $staff->description }}</span>
                                </div>
                            </div>
                            <div class="list-item__control">
                                <span class="control-links">
                                    <a href="{{ route('admin.staff.edit', $staff) }}" class="control-links__item" style="background-image:url({{ asset('img/edit.svg') }});"></a>
                                    <form class="trash-form" action="{{ route('admin.staff.destroy', $staff) }}" method="POST" >
                                        @csrf
                                        @method('DELETE')
                                            <button class="control-links__item trash-js" data-toggle="modal" data-target="#delete" style="background-image:url( {{ asset('img/trash.svg') }});"></button>
                                            <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered maw400" role="document">
                                                    <div class="modal-content">
                                                        <button type="button" class="map-container__close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <div class="modal-body ">
                                                            <div class="modal-inner tac">
                                                                Вы действительно хотите удалить данную запись?
                                                            </div>
                                                            <div class="modal-trash-control">
                                                                <button class="button button--border-black trash-modal-ok-js">Да</button>
                                                                <button class="button button--black trash-modal-cancel-js">Отмена</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                </span>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="list__bottom">
                    <nav class="pagination-wrap">
                        {{ $staffs->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </main>
@endsection
