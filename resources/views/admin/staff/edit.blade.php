@extends('layouts.main')
@section('content')
    <form action="{{ route('admin.staff.update', $staff) }}" method="POST" class="scroll-content" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <header class="header">
            <div class="back-wrap">
                <a href="{{ route('admin.staff.index') }}" class="back-link"></a>
                <h1 class="page-title">Карточка должности</h1>
            </div>
            <div class="top-buttons">
                <button class="button button--red">Сохранить</button>
            </div>
        </header>
        <main class="main">
            <div class="user-single">
                <div class="row">
                    <div class="col-3">
                        <label class="two-spike-input">
                            <span class="two-spike-input__title">Название</span><br>
                            <input type="text" name="name" class="two-spike-input__input input" value="{{ old('name', $staff->name) }}" placeholder="Введите название должности">
                            <span class="invalid-feedback">{{ $errors->first('name') }}</span>
                        </label>
                    </div>
                    <div class="col-3">
                        <label class="two-spike-input">
                            <span class="two-spike-input__title">Описание</span><br>
                            <input type="text" name="description" class="two-spike-input__input input" value="{{ old('description', $staff->description) }}" placeholder="Введите описание должности">
                            <span class="invalid-feedback">{{ $errors->first('description') }}</span>
                        </label>
                    </div>
                </div>
            </div>
        </main>
    </form>
@endsection
