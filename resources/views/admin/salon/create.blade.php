@extends('layouts.main')
@section('content')
<script src="https://api-maps.yandex.ru/2.1/?apikey=1888d523-0c7c-4180-8bb7-7489afc9c71d&lang=ru_RU" type="text/javascript"></script>
<form action="{{ route('admin.salon.store') }}" method="POST" class="scroll-content" enctype="multipart/form-data">
    @csrf
    <header class="header">
        <div class="back-wrap">
            <a href="{{ route('admin.salon.index') }}" class="back-link"></a>
            <h1 class="page-title">Карточка салона</h1>
        </div>
        <div class="top-buttons">
            <button class="button button--red">Cохранить</button>
        </div>
    </header>
    <main class="main">
        <div class="services-single">
            <div class="preview-sect">
                <div class="preview">
                    <div class="image-preview-wrap image-preview-wrap--big image-select">
                        <img src="" alt="" class="image-preview">
                        <div class="new-image"></div>
                        <input type="file" name="photo" class="upload-single-input">
                        <a href="#" class="image-remove"></a>
                    </div>
                    <div class="preview-info-with-title">
                        <span class="preview-info__title w320"><input type="text" class="input" name="title"  value="{{ old('title') }}" placeholder="Название"></span>
                        <table class="preview-info">
                            <tr class="preview-info__row">
                                <td class="preview-info__col pr32">
                                    <input type="text" class="input" name="type"  value="{{ old('type') }}" placeholder="Введите тип объекта">
                                </td>
                                <td class="preview-info__col"><input type="text" name="work_time"  value="{{ old('work_time') }}"  class="input" style="max-width: 200px" placeholder="Введите время работы"></td>
                            </tr>
                            <tr class="preview-info__row">
                                <td class="preview-info__col pr32">
                                    <div class="mt7">
                                        Адрес:
                                        <a href="#" data-toggle="modal" data-target="#mapModal" id="address" class="address">Выберите адрес</a>
                                        <input type="hidden" name="address"  id="address_input"  value="">
                                        <input type="hidden" name="latitude"  value="{{ old('latitude') }}" id="latitude">
                                        <input type="hidden" name="longitude"  value="{{ old('longitude') }}" id="longitude">
                                    </div>
                                </td>
                                <td class="preview-info__col">
                                    <div class="select-constructor">
                                        <div class="select-constructor__control">
                                            <span class="select-constructor__title">Станции метро:</span>
                                            <select name="employee" data-placeholder="Выберите станцию" class="w190 select-constructor__select select select--no-border">
                                                <option></option>
                                                <option value="1" data-type="metro">Автозаводская</option>
                                                <option value="1" data-type="metro">Дубровка</option>
                                                <option value="1" data-type="metro">Кожуховская</option>
                                            </select>
                                            <button class="button button--black select-constructor__control-btn">Добавить</button>
                                        </div>
                                        <div class="select-constructor__content">
                                            <div class="string-constructor-list">

                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="content-sect">
                <div class="content-sect__title-wrap">
                    <span class="content-sect__title">О салоне</span>
                </div>
                <div class="content-sect__inner">
                    <textarea class="textarea" name="description">{{ old('description') }}</textarea>
                </div>
            </div>
            <div class="content-sect">
                <div class="content-sect__title-wrap">
                    <span class="content-sect__title">Реквизиты</span>
                </div>
                <div class="content-sect__inner">
                    <div class="row mb30">
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">Тип</span><br>
                                <input type="text" name="company_type"  value="{{ old('company_type') }}" class="two-spike-input__input input">
                            </label>
                        </div>
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">Название юридического лица</span><br>
                                <input type="text" name="company_name"  value="{{ old('company_name') }}" class="two-spike-input__input input">
                            </label>
                        </div>
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">Юридический адрес</span><br>
                                <input type="text" name="company_address"  value="{{ old('company_address') }}" class="two-spike-input__input input">
                            </label>
                        </div>
                    </div>
                    <div class="row mb30">
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">ИНН</span><br>
                                <input type="text" name="inn"  value="{{ old('inn') }}" class="two-spike-input__input input">
                            </label>
                        </div>
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">КПП</span><br>
                                <input type="text" name="kpp"  value="{{ old('kpp') }}" class="two-spike-input__input input">
                            </label>
                        </div>
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">БИК</span><br>
                                <input type="text" name="bik"  value="{{ old('bik') }}" class="two-spike-input__input input">
                            </label>
                        </div>
                    </div>
                    <div class="row mb30">
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">Название банка</span><br>
                                <input type="text" name="bank_name"  value="{{ old('bank_name') }}" class="two-spike-input__input input">
                            </label>
                        </div>
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">Кореспондентский счет</span><br>
                                <input type="text" name="correspondent_account"  value="{{ old('correspondent_account') }}" class="two-spike-input__input input">
                            </label>
                        </div>
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">Расчетный счет</span><br>
                                <input type="text" name="checking_account"  value="{{ old('checking_account') }}" class="two-spike-input__input input">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-sect">
                <div class="content-sect__title-wrap">
                    <span class="content-sect__title">Сотрудники</span>
                </div>
                <div class="content-sect__inner">
                    <div class="select-constructor" data-name="employee_ids">
                        <div class="select-constructor__control">
                            <select  data-placeholder="Выберите мастера" class="w190 select-constructor__select select select--no-border">
                                <option></option>
                                @foreach(App\Entity\Employee\Employee::getList() as $employee)
                                    <option value="{{ $employee->id }}" data-type="person" data-image="{{ $employee->user->getThumb('preview') }}">{{ $employee->user->fullName() }}</option>
                                @endforeach
                            </select>
                            <button class="button button--black select-constructor__control-btn">Добавить</button>
                        </div>
                        <div class="select-constructor__content">
                            <div class="person-list">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</form>
<div class="modal fade" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered map-container" role="document">
        <div class="modal-content">
            <button type="button" class="map-container__close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body ">
                <div class="map-container__save"><button id="save-addr" class="button button--red">Сохранить адрес</button></div>
                <div id="map" style="width: 100%; height: 560px"></div>
            </div>
        </div>
    </div>
</div>
@endsection
