@extends('layouts.main')
@section('content')
<header class="header">
    <div class="back-wrap">
        <h1 class="page-title">Салоны</h1>
    </div>
    <div class="top-buttons">
        <a href="{{ route('admin.salon.create') }}" class="button button--black">Добавить</a>
    </div>
</header>
<main class="main">
    <div class="publish-list">
        <form action="/" class="top-control">
            <div class="filter-buttons-list">
                <a href="#" class="filter-button active">Все</a>
                <a href="#" class="filter-button">Новые</a>
                <a href="#" class="filter-button">Ожидают подтверждения</a>
                <a href="#" class="filter-button">Активные</a>
                <a href="#" class="filter-button">Заблокированные</a>
            </div>
        </form>
        <div class="list">
            <div class="list__content">
                <div class="row">
                    @foreach($salons as $salon)
                        <div class="col-6">
                            <a href="{{ route('admin.salon.show', $salon) }}" class="list-item">
                                <div class="list-item__img list-item__img--big">
                                    <img src="{{ $salon->getThumb('grid') }}" class="image" alt="">
                                </div>
                                <div class="list-item__content">
                                    <div class="list-item__row">
                                        <span class="list-item__title">{{ $salon->title }}</span>
                                    </div>
                                    <div class="list-item__row">
                                        <span class="list-item__row-text">{{ $salon->type }}</span>
                                    </div>
                                    <div class="list-item__row">
                                        <span class="rating" data-score="{{ $salon->rating }}"></span>
                                    </div>
                                    <div class="list-item__row">
                                        <span class="list-item__row-text"><span class="bold">Адрес:</span> {{ $salon->address }}</span>
                                    </div>
                                    <div class="list-item__row">
                                        <span class="list-item__row-text"><span class="bold">Время работы: </span>{{ $salon->work_time }}</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="list__bottom">
                <nav class="pagination-wrap">
                    {{ $salons->links() }}
                </nav>
            </div>
        </div>
    </div>
</main>
@endsection
