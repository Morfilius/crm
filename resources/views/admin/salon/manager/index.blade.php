@extends('layouts.main')
@section('content')
<header class="header">
    <div class="back-wrap">
        <h1 class="page-title">Карточка салона</h1>
    </div>
    <div class="top-buttons">
        <a href="{{ route('admin.salon.manager.edit', $salon) }}" class="button button--red">Редактировать</a>
    </div>
</header>
<main class="main">
    <div class="services-single">
        <div class="preview-sect">
            <div class="preview">
                <div class="image-preview-wrap image-preview-wrap--big">
                    <img src="{{ $salon->getThumb('preview') }}" alt="" class="image-preview">
                    <div class="new-image"></div>
                </div>
                <div class="preview-info-with-title">
                    <span class="preview-info__title">{{ $salon->title }}</span> <span class="rating ml22" data-score="{{ $salon->rating }}"></span>
                    <table class="preview-info">
                        <tr class="preview-info__row">
                            <td class="preview-info__col pr32">
                                {{ $salon->type }}
                            </td>
                            <td class="preview-info__col">
                                {{ $salon->work_time }}
                            </td>
                        </tr>
                        <tr class="preview-info__row">
                            <td class="preview-info__col pr32">
                                Адрес: {{ $salon->address }}
                            </td>
                            <td class="preview-info__col">Станции метро:
                                <span class="metro-list">
                                            <span class="metro-item">
                                                Автозаводская
                                                <span class="metro-item__mark"  style="background-color: #3BAD39"></span>
                                                <span class="metro-item__mark"  style="background-color: #D71515"></span>
                                            </span>
                                            <span class="metro-item">
                                                Дубровка
                                                <span class="metro-item__mark"  style="background-color: #519ADE"></span>
                                            </span>
                                            <span class="metro-item">
                                                Кожуховская
                                                <span class="metro-item__mark"  style="background-color: #EFA7A7"></span>
                                            </span>
                                        </span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="content-sect">
            <div class="content-sect__title-wrap">
                <span class="content-sect__title">О салоне</span>
            </div>
            <div class="content-sect__inner">
                <p class="text">{{ $salon->description }}</p>
            </div>
        </div>
        @if(! $salon->employees->isEmpty())
            <div class="content-sect">
                <div class="content-sect__title-wrap">
                    <span class="content-sect__title">Сотрудники</span>
                </div>
                <div class="content-sect__inner">
                    <div class="person-list">
                        @foreach($salon->employees as $employee)
                        <div class="person-item">
                            <div class="person-item__img-wrap">
                                <img src="{{ $employee->user->getThumb('preview') }}" alt="" class="image border-round">
                            </div>
                            <span class="person-item__text">{!! str_replace(' ', '<br>', $employee->user->fullName()) !!}</span>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        @if(! $services->isEmpty())
            <div class="content-sect">
                <div class="content-sect__title-wrap">
                    <span class="content-sect__title">Позиции</span>
                </div>
                <table class="table">
                    <tr class="table-header">
                        <th class="table-header__item">Название услуги</th>
                        <th class="table-header__item">Сотрудник</th>
                        <th class="table-header__item">Минимальная цена, руб.</th>
                        <th class="table-header__item table-header__item--control"></th>
                    </tr>
                    @foreach($services as $service)
                        <tr class="table-body">
                            <td class="table-body__item tac">{{ $service['service']->name }}</td>
                            <td class="table-body__item tac">{{ $service['employee']->user->fullName() }}</td>
                            <td class="table-body__item tac">{{ $service['service']->price }}</td>
                            <td class="table-body__item table-body__item--control tac">
                                <div class="control-links">
                                    <a href="{{ route('admin.service.edit', $service) }}" class="control-links__item" style="background-image:url({{ asset('img/edit.svg') }});"></a>
                                    <form class="trash-form" action="{{ route('admin.service.detach.user', [$service['service'], $service['employee']]) }}" method="POST" >
                                        @csrf
                                        @method('DELETE')
                                        <button class="control-links__item trash-js" data-toggle="modal" data-target="#delete" style="background-image:url( {{ asset('img/trash.svg') }});"></button>
                                        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered maw400" role="document">
                                                <div class="modal-content">
                                                    <button type="button" class="map-container__close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <div class="modal-body ">
                                                        <div class="modal-inner tac">
                                                            Вы действительно хотите удалить данную запись?
                                                        </div>
                                                        <div class="modal-trash-control">
                                                            <button class="button button--border-black trash-modal-ok-js">Да</button>
                                                            <button class="button button--black trash-modal-cancel-js">Отмена</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        @endif
    </div>
</main>
@endsection
