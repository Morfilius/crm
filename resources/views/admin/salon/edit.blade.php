@extends('layouts.main')
@section('content')
<script src="https://api-maps.yandex.ru/2.1/?apikey=1888d523-0c7c-4180-8bb7-7489afc9c71d&lang=ru_RU" type="text/javascript"></script>
<form action="{{ route('admin.salon.update', $salon) }}" method="POST" class="scroll-content" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <header class="header">
        <div class="back-wrap">
            <a href="{{ route('admin.salon.index') }}" class="back-link"></a>
            <h1 class="page-title">Карточка салона</h1>
        </div>
        <div class="top-buttons">
            <button class="button button--red">Cохранить</button>
        </div>
    </header>
    <main class="main">
        <div class="services-single">
            <div class="preview-sect">
                <div class="preview">
                    <div class="image-preview-wrap image-preview-wrap--big @if(! $salon->hasImage()) image-select @endif">
                        <img src="{{ $salon->getThumb('preview') }}" alt="" class="image-preview">
                        <div class="new-image"></div>
                        <input type="file" name="photo" class="upload-single-input">
                        <a href="#" class="image-remove"></a>
                    </div>
                    <div class="preview-info-with-title">
                        <span class="preview-info__title w320">
                            <label class="feedback-wrap">
                                <input type="text" class="input" name="title"  value="{{ old('title', $salon->title) }}" placeholder="Название">
                                <span class="invalid-feedback">{{ $errors->first('title') }}</span>
                            </label>
                        </span>
                        <table class="preview-info">
                            <tr class="preview-info__row">
                                <td class="preview-info__col pr32">
                                    <label class="feedback-wrap">
                                        <input type="text" class="input" name="type"  value="{{ old('type', $salon->type) }}" placeholder="Введите тип объекта">
                                        <span class="invalid-feedback">{{ $errors->first('type') }}</span>
                                    </label>
                                </td>
                                <td class="preview-info__col">
                                    <label class="feedback-wrap">
                                        <input type="text" name="work_time"  value="{{ old('work_time', $salon->work_time) }}"  class="input" style="max-width: 200px" placeholder="Введите время работы">
                                        <span class="invalid-feedback">{{ $errors->first('work_time') }}</span>
                                    </label>
                                </td>
                            </tr>
                            <tr class="preview-info__row">
                                <td class="preview-info__col pr32">
                                    <div class="mt7">
                                        Адрес:
                                        <label class="feedback-wrap" style="width: auto">
                                            <a href="#" data-toggle="modal" data-target="#mapModal" id="address" class="address">@if(old('address', $salon->address)) {{ old('address', $salon->address) }} @else Выберите адрес @endif</a>
                                            <input type="hidden" name="address" id="address_input" value="{{ old('address', $salon->address) }}">
                                            <input type="hidden" name="latitude"  value="{{ old('latitude', $salon->latitude) }}" id="latitude">
                                            <input type="hidden" name="longitude"  value="{{ old('longitude', $salon->longitude) }}" id="longitude">
                                            <span class="invalid-feedback">{{ $errors->first('address') }}</span>
                                        </label>
                                    </div>
                                </td>
                                <td class="preview-info__col">
                                    <div class="select-constructor">
                                        <div class="select-constructor__control">
                                            <span class="select-constructor__title">Станции метро:</span>
                                            <select name="employee" data-placeholder="Выберите станцию" class="w190 select-constructor__select select select--no-border">
                                                <option></option>
                                                <option value="1" data-type="metro">Автозаводская</option>
                                                <option value="1" data-type="metro">Дубровка</option>
                                                <option value="1" data-type="metro">Кожуховская</option>
                                            </select>
                                            <button class="button button--black select-constructor__control-btn">Добавить</button>
                                        </div>
                                        <div class="select-constructor__content">
                                            <div class="string-constructor-list">

                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="content-sect">
                <div class="content-sect__title-wrap">
                    <span class="content-sect__title">О салоне</span>
                </div>
                <div class="content-sect__inner">
                    <label class="feedback-wrap">
                        <textarea class="textarea" name="description">{{ old('description', $salon->description) }}</textarea>
                        <span class="invalid-feedback">{{ $errors->first('description') }}</span>
                    </label>
                </div>
            </div>
            <div class="content-sect">
                <div class="content-sect__title-wrap">
                    <span class="content-sect__title">Реквизиты</span>
                </div>
                <div class="content-sect__inner">
                    <div class="row mb30">
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">Тип</span><br>
                                <input type="text" name="company_type"  value="{{ old('company_type', $salon->company_type) }}" class="two-spike-input__input input">
                                <span class="invalid-feedback">{{ $errors->first('company_type') }}</span>
                            </label>
                        </div>
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">Название юридического лица</span><br>
                                <input type="text" name="company_name"  value="{{ old('company_name', $salon->company_name) }}" class="two-spike-input__input input">
                                <span class="invalid-feedback">{{ $errors->first('company_name') }}</span>
                            </label>
                        </div>
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">Юридический адрес</span><br>
                                <input type="text" name="company_address"  value="{{ old('company_address', $salon->company_address) }}" class="two-spike-input__input input">
                                <span class="invalid-feedback">{{ $errors->first('company_address') }}</span>
                            </label>
                        </div>
                    </div>
                    <div class="row mb30">
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">ИНН</span><br>
                                <input type="text" name="inn"  value="{{ old('inn', $salon->inn) }}" class="two-spike-input__input input">
                                <span class="invalid-feedback">{{ $errors->first('inn') }}</span>
                            </label>
                        </div>
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">КПП</span><br>
                                <input type="text" name="kpp"  value="{{ old('kpp', $salon->kpp) }}" class="two-spike-input__input input">
                                <span class="invalid-feedback">{{ $errors->first('kpp') }}</span>
                            </label>
                        </div>
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">БИК</span><br>
                                <input type="text" name="bik"  value="{{ old('bik', $salon->bik) }}" class="two-spike-input__input input">
                                <span class="invalid-feedback">{{ $errors->first('bik') }}</span>
                            </label>
                        </div>
                    </div>
                    <div class="row mb30">
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">Название банка</span><br>
                                <input type="text" name="bank_name"  value="{{ old('bank_name', $salon->bank_name) }}" class="two-spike-input__input input">
                                <span class="invalid-feedback">{{ $errors->first('bank_name') }}</span>
                            </label>
                        </div>
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">Кореспондентский счет</span><br>
                                <input type="text" name="correspondent_account"  value="{{ old('correspondent_account', $salon->correspondent_account) }}" class="two-spike-input__input input">
                                <span class="invalid-feedback">{{ $errors->first('correspondent_account') }}</span>
                            </label>
                        </div>
                        <div class="col-3">
                            <label class="two-spike-input">
                                <span class="two-spike-input__title">Расчетный счет</span><br>
                                <input type="text" name="checking_account"  value="{{ old('checking_account', $salon->checking_account) }}" class="two-spike-input__input input">
                                <span class="invalid-feedback">{{ $errors->first('checking_account') }}</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-sect">
                <div class="content-sect__title-wrap">
                    <span class="content-sect__title">Сотрудники</span>
                </div>
                <div class="content-sect__inner">
                    <div class="select-constructor" data-name="employee_ids">
                        <div class="select-constructor__control">
                            <select name="employee" data-placeholder="Выберите сотрудника" class="w190 select-constructor__select select select--no-border">
                                <option></option>
                                @foreach(App\Entity\Employee\Employee::getList() as $employee)
                                    <option value="{{ $employee->id }}" data-type="person" data-image="{{ $employee->user->getThumb('preview') }}">{{ $employee->user->fullName() }}</option>
                                @endforeach
                            </select>
                            <button class="button button--black select-constructor__control-btn">Добавить</button>
                        </div>
                        <div class="select-constructor__content">
                            <div class="person-list">
                                @foreach($salon->employees as $employee)
                                    <div class="person-item select-constructor__item">
                                        <div class="person-item__img-wrap">
                                            <img src="{{ $employee->user->getThumb('preview') }}" alt="" class="image border-round">
                                            <a href="#" class="image-remove person-item__remove"></a>
                                        </div>
                                        <span class="person-item__text">{!! str_replace(' ', '<br>', $employee->user->fullName()) !!}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</form>
<div class="modal fade" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered map-container" role="document">
        <div class="modal-content">
            <button type="button" class="map-container__close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body ">
                <div class="map-container__save"><button id="save-addr" class="button button--red">Сохранить адрес</button></div>
                <div id="map" style="width: 100%; height: 560px"></div>
            </div>
        </div>
    </div>
</div>
@endsection
