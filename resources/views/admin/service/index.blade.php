@extends('layouts.main')
@section('content')
    <header class="header">
        <div class="back-wrap">
            <h1 class="page-title">Услуги</h1>
        </div>
        @if($active_category_id)
            <div class="top-buttons">
                <a href="{{ route('admin.service.create', $active_category_id) }}" class="button button--black">Добавить услугу</a>
            </div>
        @endif
    </header>
    <main class="main">
        <div class="services">
            <div class="services__menu">
                <ul class="accordion-menu">
                    <li class="accordion-menu__item">
                       <a href="{{ route('admin.service.scope.store') }}" class="accordion-menu__add accordion-menu__add--scope">Добавить сферу</a>
                    </li>
                    @foreach($scopes as $scope)
                    <li class="accordion-menu__item">
                        <button class="accordion-menu__btn" data-toggle="collapse" data-target="#collapse{{ $scope->id }}" aria-expanded="true" aria-controls="collapseOne">{{ $scope->name }}</button>
                        <div class="collapse @if($scope->hasChildCategory($active_category_id)) show @endif" id="collapse{{ $scope->id }}">
                            <a href="{{ route('admin.service.category.store', $scope->id) }}" class="accordion-menu__add">Добавить категорию</a>
                            @if($categories = $scope->categories())
                            <div class="accordion-menu__content">
                                <ul class="accordion-submenu" >
                                    @foreach($categories as $category)
                                    <li class="accordion-submenu__item @if($category->id == $active_category_id) active @endif">
                                        <a href="{{ route('admin.service.index', $category->id) }}" class="accordion-submenu__link">{{ $category->name }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="services__table">
                @if(isset($active_category->services) && ! $active_category->services->isEmpty())
                <table class="table services-table">
                    <tr class="table-header">
                        <th class="table-header__item">Название услуги</th>
                        <th class="table-header__item w180">Онлайн-запись</th>
                        <th class="table-header__item w180">Начало поиска</th>
                        <th class="table-header__item w180">Конец поиска</th>
                        <th class="table-header__item w320">Минимальная<br> цена, руб.</th>
                    </tr>
                    @foreach($active_category->services as $service)
                    <tr class="table-body">
                        <td class="table-body__item">{{ $service->name }}</td>
                        <td class="table-body__item tac">
                            <label class="checkbox jcc">
                                <input type="checkbox" disabled @if($service->online) checked @endif value="">
                                <span class="checkbox-mark checkbox-mark--gray checkbox-mark--check"></span>
                            </label>
                        </td>
                        <td class="table-body__item tac">{{ $service->session_start }}</td>
                        <td class="table-body__item tac">{{ $service->session_end }}</td>
                        <td class="table-body__item">
                            <span class="df jcsb">
                                {{ $service->price }}
                                <span class="control-links">
                                    <a href="{{ route('admin.service.edit', $service) }}" class="control-links__item" style="background-image:url({{ asset('img/edit.svg') }});"></a>
                                    <a href="#" class="control-links__item" style="background-image:url({{ asset('img/trash.svg') }});"></a>
                                </span>
                            </span>
                        </td>
                    </tr>
                    @endforeach
                </table>
                @endif
            </div>
        </div>
    </main>
@endsection
