@extends('layouts.main')
@section('content')
    <form action="{{ route('admin.service.update', $service) }}" method="POST" class="scroll-content" enctype="multipart/form-data">
        @csrf
        <header class="header">
            <div class="back-wrap">
                <a href="{{ route('admin.service.index') }}" class="back-link"></a>
                <h1 class="page-title">Карточка услуги</h1>
            </div>
            <div class="top-buttons">
                <button class="button button--red">Сохранить</button>
            </div>
        </header>
        <main class="main">
            <div class="services-single">
                <div class="content-sect">
                    <div class="content-sect__inner">
                        <table class="preview-info no-margin">
                            <tr class="preview-info__row">
                                <td class="preview-info__col pr32"><span class="bold">Название услуги</span></td>
                                <td class="preview-info__col">
                                    <label class="feedback-wrap">
                                        <input type="text" class="input" name="name" value="{{ old('name', $service->name) }}">
                                        <span class="invalid-feedback">{{ $errors->first('name') }}</span>
                                    </label>
                                </td>
                            </tr>
                            <tr class="preview-info__row">
                                <td class="preview-info__col pr32"><span class="bold">Минимальная цена, руб.</span></td>
                                <td class="preview-info__col">
                                    <label class="feedback-wrap">
                                        <input type="text" name="price" class="input-border maw80 tac" value="{{ old('price', $service->price) }}">
                                        <span class="invalid-feedback">{{ $errors->first('price') }}</span>
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">Описание</span>
                    </div>
                    <div class="content-sect__inner">
                        <label class="feedback-wrap">
                            <textarea name="description" class="textarea" rows="2">{{ old('description', $service->description) }}</textarea>
                            <span class="invalid-feedback">{{ $errors->first('description') }}</span>
                        </label>
                    </div>
                </div>
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">Салоны</span>
                    </div>
                    <div class="content-sect__inner">
                        <div class="select-constructor" data-name="salon_ids">
                            <div class="select-constructor__control">
                                <label>
                                    <select name="employee" data-placeholder="Выберите салон" class="w190 select-constructor__select select select--no-border">
                                        <option></option>
                                        @foreach(App\Entity\Salon::getList() as $salon)
                                            <option value="{{ $salon->id }}" data-type="metro">{{ $salon->title }}</option>
                                        @endforeach
                                    </select>
                                </label>
                                <button class="button button--black select-constructor__control-btn" id="test">Добавить</button>
                            </div>
                            <div class="select-constructor__content">
                                <div class="string-constructor-list">

                                    @foreach($service->salons as $salonItem)
                                        <span class="string-constructor-item select-constructor__item">{{ $salonItem->title }}
                                            <a href="#" class="image-remove string-constructor-item__remove"></a>
                                        </span>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">Сотрудники</span>
                    </div>
                    <div class="content-sect__inner">
                        <div class="select-constructor" data-name="employee_ids">
                            <div class="select-constructor__control">
                                <label>
                                    <select  data-placeholder="Выберите сотрудника" class="w190 select-constructor__select select select--no-border">
                                        <option></option>
                                        @foreach(App\Entity\Employee\Employee::getList() as $employee)
                                            <option value="{{ $employee->id }}" data-type="person" data-image="{{ $employee->user->getThumb('preview') }}">{{ $employee->user->fullName() }}</option>
                                        @endforeach
                                    </select>
                                </label>
                                <button class="button button--black select-constructor__control-btn">Добавить</button>
                            </div>
                            <div class="select-constructor__content">
                                <div class="person-list">
                                    @foreach($service->employees as $employeeItem)
                                        <div class="person-item select-constructor__item">
                                            <div class="person-item__img-wrap">
                                                <img src="{{ $employeeItem->user->getThumb('preview') }}" alt="" class="image border-round">
                                                <a href="#" class="image-remove person-item__remove"></a>
                                            </div>
                                            <span class="person-item__text">{!! str_replace(' ', '<br>', $employeeItem->user->fullName()) !!}</span>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">Онлайн-запись</span>
                    </div>
                    <div class="content-sect__inner">
                        <table class="common-info">
                            <tr class="common-info__row">
                                <td class="common-info__col"><span class="bold fz16">Онлайн-запись</span></td>
                                <td class="common-info__col">
                                    <label class="checkbox feedback-wrap">
                                        <input type="checkbox" name="online" @if(old('online', $service->online)) checked @endif value="1">
                                        <span class="checkbox-mark checkbox-mark--check"></span><span class="fz16">Разрешить онлайн-запись</span>
                                        <span class="invalid-feedback">{{ $errors->first('online') }}</span>
                                    </label>
                                </td>
                            </tr>
                            <tr class="common-info__row">
                                <td class="common-info__col"><span class="bold fz16">Время начала поиска сеансов</span></td>
                                <td class="common-info__col">
                                    <span class="select2-border">
                                        <label class="feedback-wrap">
                                            <select name="session_start" data-placeholder="--" class="select w90">
                                                <option></option>
                                                @foreach(\App\Entity\Service\Service::sessionStartList() as $session_start)
                                                    <option @if($session_start == old('session_start', $service->session_start)) selected @endif value="{{ $session_start }}">{{ $session_start }}</option>
                                                @endforeach
                                            </select>
                                            <span class="invalid-feedback">{{ $errors->first('session_start') }}</span>
                                        </label>
                                    </span>
                                </td>
                            </tr>
                            <tr class="common-info__row">
                                <td class="common-info__col"><span class="bold fz16">Время окончания поиска сеансов</span></td>
                                <td class="common-info__col">
                                    <span class="select2-border">
                                        <label class="feedback-wrap">
                                            <select name="session_end" data-placeholder="--" class="select w90">
                                                <option></option>
                                                @foreach(\App\Entity\Service\Service::sessionEndList() as $session_end)
                                                    <option @if($session_end == old('session_end', $service->session_end)) selected @endif value="{{ $session_end }}">{{ $session_end }}</option>
                                                @endforeach
                                            </select>
                                            <span class="invalid-feedback">{{ $errors->first('session_end') }}</span>
                                        </label>
                                    </span>
                                </td>
                            </tr>
                            <tr class="common-info__row">
                                <td class="common-info__col"><span class="bold fz16">Шаг вывода сеансов, мин.</span></td>
                                <td class="common-info__col">
                                    <span class="select2-border">
                                        <label class="feedback-wrap">
                                            <select  name="session_step" data-placeholder="--" class="select w90">
                                                <option></option>
                                                 @foreach(\App\Entity\Service\Service::sessionStepList() as $session_step)
                                                    <option @if($session_step == old('session_step', $service->session_step)) selected @endif value="{{ $session_step }}">{{ $session_step }}</option>
                                                @endforeach
                                            </select>
                                            <span class="invalid-feedback">{{ $errors->first('session_step') }}</span>
                                        </label>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="content-sect">
                    <div class="content-sect__title-wrap">
                        <span class="content-sect__title">Даты записи</span>
                    </div>
                    <div class="content-sect__inner">
                        <label class="checkbox">
                            <input type="checkbox" name="status" checked value="1">
                            <span class="checkbox-mark checkbox-mark--check"></span><span class="fz16">Установить даты без онлайн-записи</span>
                        </label>
                        <div class="calendar-recording">
                            <?php
                                $checked = $service->records->map(function ($item) {
                                    return [
                                        'date' => $item->date->format('Y-m-d'),
                                    ];
                                })->toJson()
                            ?>
                            <div class="calendar calendar-service-js calendar-recording__item" data-checked='<?= $checked ?>' data-name="record"></div>
                            <div class="calendar calendar-service-js calendar-recording__item" data-checked='<?= $checked ?>' data-name="record"></div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </form>
@endsection
