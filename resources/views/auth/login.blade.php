@extends('layouts.login')
@section('content')
<div class="login">
    <div class="login__left"><img class="login__logo" src="{{ asset('img/logo.svg') }}" alt=""></div>
    <div class="login__right">
        <form  method="POST" action="{{ route('login') }}" class="login-form">
            @csrf
            <span class="login-form__title">Вход</span>
            <label class="login-form__row">
                <span class="login-form__text">Email</span><br>
                <input type="email" placeholder="Введите Email" name="email" value="{{ old('email') }}" class="login-form__input">
            </label>
            <label class="login-form__row">
                <span class="login-form__text">Пароль</span><br>
                <input type="password" placeholder="Введите пароль" name="password"  value="{{ old('password') }}" class="login-form__input">
            </label>
            @if($errors->first('email') || $errors->first('password'))
                <span class="login-error">Неправильные логин или пароль</span>
            @endif
            <button class="login-form__button button button--red">Войти</button>
        </form>
    </div>
</div>
@endsection
